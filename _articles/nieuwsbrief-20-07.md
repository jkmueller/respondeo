---
title: Bericht van onze zustervereniging Rosenstock-Huessy Gesellschaft
category: nieuwsbrief
created: 2020-07
order: 1
---
Via Agnes Lieverse-Opdam kwam het bericht dat vrijdagavond 24 juli 2020 Eckart Wilkens is overleden. Hij was al enige tijd ernstig ziek.

***Eckart Wilkens*** is lange tijd voorzitter geweest van de Duitse [Rosenstock-Huessy Gesellschaft](https://www.rosenstock-huessy.com). Hij heeft een belangrijke rol gespeeld in de [verbreiding van inzicht](https://www.eckartwilkens.org), met name in de biografische context en de plaats van het werk van Rosenstock-Huessy in de geschiedenis en cultuur van Duitsland en Europa. Hij heeft ook altijd nauw contact gehouden met de Nederlanders die bezig waren met het werk van Rosenstock-Huessy, met name rond het [Rosenstock-Huessy huis](https://www.erhg.net/rosenstock-huessy-huis-nl/).
Hij las en vertaalde uit het Nederlands naar het Duits, meestal gedichten voor de [Stimmstein](https://www.rosenstock-huessy.com/stimmstein/) (periodiek dat uitgegeven werd door de Gesellschaft).

Op onze [web-site](http://www.respondeo.nl/)  zal binnenkort hierover meer te lezen zijn.
