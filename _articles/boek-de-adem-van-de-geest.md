---
title: "Eugen Rosenstock-Huessy: De adem van de Geest"
category: boek
order: 1
---
Vertaling van Der Atem des Geistes (1951,1991) van Eugen Rosenstock-Huessy.
Vertaald, bewerkt en toegelicht door Wim van der Schee
Te bestellen bij [Wim van der Schee](mailto:wpvanderschee@planet.nl).

![De adem van de Geest 1]({{ 'assets/images/De-adem-van-de-Geest.jpg' | relative_url }}){:.img-right.img-large}
Dit boek beschrijft de aard van de (tussen)menselijke betrekkingen op basis van hoe die zich historisch hebben ontwikkeld. Het is tevens een pleidooi voor een relationele visie op het menszijn. In deze visie gaat het om een ontwikkeling waarin ons natuurlijk en ons geestelijk bestaan met elkaar verweven zijn. Tegenwoordig dreigt het natuurlijk perspectief onze geest  op te slokken. Als bijvoorbeeld in de samenleving het overheersend motto is dat een ieder ‘zijn eigen broek’ op moet kunnen houden, dan verwijst dat meer naar ieders eigen kracht (ongeacht hoe het zit met die kracht) dan naar onderlinge solidariteit.

Een mens heeft een lichaam en een geest. Die geest is eeuwenlang beschut door het christelijk geloof, maar die tijd lijkt voorbij. Los van die beschutting is de humaniteit van ons menszijn in het geding. In de persoonlijke levenssfeer speelt die als vanouds een rol, zoals tussen ouders en kinderen en tussen vrienden, die elkaar vertrouwen en waarderen. Maar in de maatschappij spelen maatstaven van humaniteit een te kleine rol. Het gevolg is onderling wantrouwen, onderlinge concurrentie, een scheve verdeling van welvaart en welzijn, polarisatie etc. De ‘adem van de geest’ waait er nauwelijks.

![De adem van de Geest 2]({{ 'assets/images/De-adem-van-de-Geest2.jpg' | relative_url }}){:.img-left.img-large}
Gaat het over de geest, dan gaat het over onze bezieling, over wat zinvol is, over hoe wij elkaar nodig hebben als geestverwanten. Dit boek gaat over de werking van de geest als het ontvangen en het zelf bijdragen aan een werkelijk medemenselijk, humaan bestaan. Wat weten we ervan? Waar blijft de wetenschap op de agogische terreinen van opvoeding, onderwijs en samenwerking? Rosenstock-Huessy gaat terug naar de kern van wat er in de omgang tussen mensen gebeurt en schoont ons inzicht. Centraal staat dat wij contextueel en toekomstgericht moeten denken. Wij, wereldburgers op die ene kwetsbare planeet, moeten beter nadenken over wat nodig is om tot een humane samenleving te komen, in onze persoonlijke verhoudingen én als betrokkenen bij de grote vraagstukken van deze tijd.

De vertaler, Wim, is beschikbaar voor een inleiding of lezing + gesprek over dit boek.
