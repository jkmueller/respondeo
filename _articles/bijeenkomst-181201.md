---
title: Bijeenkomst 1 december 2018
thema: "Lazarus – de miljoenen jaren en de schepping door het woord"
category: bijeenkomst
created: 2018-12-01
order: 1
---
### {{ page.thema }}

Lazarus werd door Jezus uit de dood gered. Maar waarom? Rosenstock-Huessy heeft opgemerkt dat hij eigenlijk met het verhaal van de opwekking van Lazarus niks kon.

Stemmen die iets te zeggen hebben moeten weer tot leven geroepen worden, maar waarom Lazarus? Bas Leenman wijst op een overeenkomst tussen Lazarus en Johannes: ze zijn de enige twee personen waarvan in de bijbel gezegd wordt dat Jezus hen liefhad. Waar de levens van alle andere apostelen naar een dramatisch hoogtepunt toegaan is Johannes gewoon heel oud geworden. Ook Lazarus zal “gewoon” zijn plaats in de wereld weer hebben ingenomen. Wat hebben “gewone” levens, het komen en gaan van miljarden mensen door miljoenen jaren, toe te voegen aan de heilsgeschiedenis?

Wel een relevante vraag voor mensen die gevangen zitten in de dagelijkse cyclus van productie en consumptie – mensen als wij.

Op 1 december lezen en bespreken we als Respondeo met belangstellenden de tekst van Bas Leenman in zijn boek “Als God stukloopt” pagina’s 210-223.

![Irene Twello]({{ 'assets/images/irene-twello.jpg' | relative_url }}){:.img-right.img-small}

Het programma:

| 10.30 |  Ontvangst en koffie
| 11.00 |  Opening, samen lezen
| 12.30 |  Lunch
| 13:30 |  Open gesprek onder leiding van Otto Kroesen
| 15.00 |  Koffie en thee
| 15.30 |  Sluiting

De tekst is op te vragen bij Egbert Schroten. Maar beter bestel je het hele boek bij Skandalon (website of via de boekhandel).

Voor de kosten van lunch en zaalhuur wordt een vrijwillige bijdrage gevraagd (richtbedrag 15 euro). Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo.

[als PDF]({{ 'assets/downloads/2018_respondeo_uitnodiging_najaarsbijeenkomst.pdf' | relative_url }})
