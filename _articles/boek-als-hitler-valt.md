---
title: "Marlouk Alders: Als Hitler valt"
category: boek
order: 1
---
![Als Hitler valt]({{ 'assets/images/Als-Hitler-valt-2.jpg' | relative_url }}){:.img-left.img-large}
In deze tijd van grote spanningen, mensen die elkaar niet verstaan, verkeerd uitvallende dadendrang, is dit een hoogst actueel onderwerp. Onze hele maatschappij is in verzet. Alles moet anders, maar hoe? We praten niet met elkaar. We vechten elkaar de tent uit, soms letterlijk. Ook binnen het Duitse verzet waren er tegenstellingen, van links tot rechts, van radicaal tot gematigd.

**TOCH WERKTE EEN GROOT DEEL VAN HET DUITSE VERZET SAMEN, IN DE BEFAAMDE KREISAUER KREIS. HOE KREGEN ZE DAT VOOR ELKAAR IN HET TOTAAL VERSCHEURDE DUITSLAND?**

Het antwoord wordt gegeven in dit boekje van Marlouk Alders. De meeste leden van de Kreisauer Kreis hadden al jaren eerder als vrijwilliger met elkaar samengewerkt, socialisten met conservatieven, christenen met atheïsten, mensen van adel met boeren, arbeiders en studenten. Samenwerken is het antwoord, zo heeft Marlouk Alders zelf ervaren. Door samen te werken leer je samen te praten, zo bewijst het spannende verhaal van de Kreisauer Kreis. Daar ligt de weg naar een nieuwe toekomst voor ons allemaal.

[Bestellen in ons Boekwinkeltje](https://respondeo.boekwinkeltjes.nl/)
