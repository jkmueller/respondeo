---
title: "Interim 8 – oktober 2016"
thema: "Als God stukloopt"
category: interim
created: 2016-10
order: 1
---

### {{ page.thema }}

#### Artikelen rond het boek van Bas Leenman

* Inleiding – Wilmy Verhage
* De metamorfose van het woord van God in de taal van de mens – Otto Kroesen
* De bijbel- en maatschappijvisie van Bas Leenman: uitdaging en reserves – Rinse Reeling Brouwer
* Een lans voor synagoge en kerk – Hans van der Heiden
* Aan de andere kant van de heuvel – Jan de Vlieger
* Bij de introductie van ‘Als God stukloopt’ – Jan Kroesen
* Tijdsdenken versus ruimtelijk denken – Wilmy Verhage
* De taal is wijzer dan haar spreker III, oorspronkelijke tekst
* Achterpagina

[Interim 8-2016]({{ 'assets/downloads/interim_8_2016.pdf' | relative_url }})
