---
title: "Symposium en boekpresentatie 12 november 2022"
thema: "‘De menselijke onderneming’"
category: bijeenkomst
created: 2022-10-15
published: 2022-10-15
order: 1
---
### {{ page.thema }}
![De menselijke onderneming]({{ 'assets/images/kroesen-de-menselijke-onderneming.jpeg' | relative_url }}){:.img-right.img-large}
Crisis. Het is een woord dat vandaag de dag vaak wordt gebezigd. Als je terugkijkt de geschiedenis in, zie je dat bij een crisis juist nieuwe en grote inspiraties een uitweg bieden. Mensen krijgen een nieuw geloof en zelfverstaan, wat leidt tot een nieuwe inirichting van economie en maatschappij.

In **De menselijke onderneming** beschrijft Otto Kroesen dit 'grote verhaal van geloof en economie', van de stammensamenleving tot nu.
<!--more-->

----
### Programma symposium en boekpresentatie

| 12:30 | Ontvangst, met koffie, thee en broodjes
| 13:30 | start symposium
|       | **Is Europa al af?**
|       | ***Father Avin Kunnekkadan***
|       | *Lid van het Theologisch Elftal van dagblad Trouw. Migrantenpater RK-parochie De Goede Herder in Schiedam en provinciaal overste van de congregatie Societas Verbi Divini*
|       | **Het kruis der werkelijkheid in het MKB**
|       | ***Johan Reijenga***
|       | *Hoofddocent en onderzoeker bij Kenniscentrum Business Innovation van Hogeschool Rotterdam. Hij volgde studies economie en theologie, en adviseert bedrijven en organisaties op het gebied van strategie, leiderschap en innovatie*
|| Pauze
|       | **De inspiratie van Skandalon**
|       | ***Jan de Vlieger***
|       | *Directeur en uitgever van Skandalon. Voormalig predikant en directeur van De Drukkerij*
|       | **Een groter verhaal voor een kleinere mens**
|       | ***Otto Kroesen***
|       | *Auteur van De menselijke onderneming*
|       | **Discussie met de sprekers en de zaal**
|       | o.l.v. ***Jan Kroesen***
|       | *Dagvoorzitter, voorzitter Rosenstock-Huessy vereniging Respondeo*
| 16:00 | Hapjes en drankjes
| 17:00 | Einde


**Aanmelden**\
Aanmelden liefst vóór maandag 7 november bij ottokroesen@gmail.com.

**Locatie**\
Synagoge, Delft, Koornmarkt 12, 2611 EE.\
Dat is 10 minuten lopen van het station. Parkeren kan achter de synagoge of ondergronds in de buurt.

**Korting**\
Het boek is bij de boekpresentatie te verkrijgen voor € 27,50 i.p.v. € 32,99. U hoeft dan niet vooraf te bestellen. Wilt u het boek graag kopen, maar bent u niet in de gelegenheid te komen, bestel dan voor € 29,99 het boek [op](https://www.skandalon.nl/onderneming)

Het programma als [PDF]({{ 'assets/downloads/kroesen_boekpresentatie_en_symposium_de_menselijke_onderneming.pdf' | relative_url }}).

----
### Over het boek

Vond er in de geschiedenis een crisis plaats, dan kon alleen een nieuwe en grote inspiratie een uitweg bieden. In De menselijke onderneming volgt Otto Kroesen het spoor van dergelijke inspiraties. Ze gaven mensen een nieuw geloof en zelfverstaan en mondden uit in een nieuwe inrichting van de economie en de maatschappij.

Het grote verhaal begint bij de stammensamenleving, met hechte en ondernemende groepen. Hierna komen hiërarchische rijksculturen op, zoals Egypte en China, maar ook Israël en Griekenland hebben ons gevormd. Na de oudheid volgt onder meer de kerk van de martelaren, de islam en Europa; allemaal samenlevingen met een gemeenschappelijk verhaal.

Onze moderne ondernemingen en de globaal geworden wereldeconomie hebben geen groot verhaal meer. Mensen lijken alleen maar hun eigen (fragmentarische) levensverhaal te hebben. We worden geleefd, zijn ontworteld en thuisloos, verbrokkeld door drukke bedrijvigheid en vlugge sensaties. Kroesen kijkt niet alleen terug, maar duidt ook de uitdagingen van onze huidige tijd. Welk groot verhaal raapt ons bij elkaar?

Een korte beschrijving van de inhoud is [hier]({{ 'assets/downloads/kroesen_De_Menselijke_Onderneming_toc.pdf' | relative_url }}) te downloaden en een inhoudsopgave [hier]({{ 'assets/downloads/kroesen_De_Menselijke_Onderneming_kort.pdf' | relative_url }}).
