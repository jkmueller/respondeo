---
title: Nieuwsbrief april 2021
category: nieuwsbrief
created: 2021-04
order: 1
---
Bij deze de nieuwsbrief van april 2021, \
grotendeels te gebruiken als voorbereiding op:

**Zaterdag 8 mei 2021 voorjaarsbijeenkomst** *vereniging Respondeo*

via Zoom: https://tudelft.zoom.us/j/91580515620
Deze link kun je zonder bezwaar doorsturen aan andere belangstellenden.

***Gesprek naar aanleiding van het boek “Mijn ontelbare identiteiten” van Sinan Çankaya***

Agenda:         

| 13:45 |     bijeenkomst toegankelijk
||       (zeker als je niet veel “zoomt” is het aan te raden tijd te nemen om geluid, beeld en zitplaats uit te proberen en in te stellen
| 14:00 |     welkom, kort rondje: wie is er?
||.             eerste deel
| 15:00 |     pauze
| 15:15 |    tweede deel
| 16:15 |    afronding, tijd voor mededelingen
| 16:30 |    einde

Deze bijeenkomst is geen plaats om hele doorwrochte betogen of discussies te voeren. Lees het boek, of grasduin in deze nieuwsbrief en vraag jezelf af: Aan welke stukken of uitspraken van Rosenstock doet je dit denken?

In een bestuursvergadering werd de brief aan/over de Anne Frankstichting genoemd, van Bas Leenman. Dit staat in “als God stukloopt”, uitgegeven bij Skandalon.

[nieuwsbrief 21-40]({{ 'assets/downloads/2021_april_Digitale_brief_Respondeo.pdf' | relative_url }})
