---
title: Nieuwsbrief oktober 2020
category: nieuwsbrief
created: 2020-10
order: 1
---
Beste leden van Respondeo,

bij deze de tweede nieuwsbrief, door het bestuur verzorgd. Er is vooral aandacht voor de betekenis van Eckart Wilkens, zijn vertalen en voorleven van de inzichten van Rosenstock-Huessy. Eckart, voormalig bestuurslid van de duitse Rosenstock-Huessy Gesellschaft, is deze zomer overleden.

Verder lezen kan op de uitgebreide "post" [Volwasseneneducatie van Eckart Wilkens](http://www.rosenstock-huessy.nl/wp/volwasseneneducatie-van-eckart-wilkens/) van Otto op onze website. Geschikt voor groot en kleiner beeldscherm.

Verder hebben we de onderdelen bij elkaar gezet die normaalgesproken bij de algemene ledenvergadering gepresenteerd en besproken worden. Het is wat behelpen maar we zitten niet stil. We willen ons zo verantwoorden en gelegenheid geven tot inspraak. Wij hopen dat we in 2021 wel weer een fysieke bijeenkomst kunnen houden.

[nieuwsbrief 20-10]({{ 'assets/downloads/2020_oktober_digitale_brief_Respondeo.pdf' | relative_url }})
