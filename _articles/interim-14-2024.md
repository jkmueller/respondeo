---
title: "Interim 14 – april 2024"
thema: "Oorlog in Europa"
category: interim
created: 2024-04
order: 1
published: 2024-04-26
---

### {{ page.thema }}

#### De kloof tussen Oost en West - pogingen tot inzicht en overbrugging

INHOUDSOPGAVE
* Inleiding - Jan Kroesen
* Imago Dei opnieuw verbeeld met het oog op extreme dehumanisatie - Prof. Katja Tolstaja
* Conflict tussen Oost en West Liberalisering zonder Civil Society - Otto Kroesen
* Met Girard voor de hoognodige trialoog - Wiel Eggen.
* Het schisma tussn Oost en West - Wilmy Verhage
* Dat zijn geen vijanden, maar mensen - Marlouk Alders
* Achterpagina


[Interim 14 2024]({{ 'assets/downloads/Interim_14_2024.pdf' | relative_url }})

#### Inleiding

Geachte lezer, lezeres, lieve vrienden.

Oekraïne is het slachtoffer geworden van tegengestelde opvattingen van menselijk samenleven en de ontsporing van de communicatie daartussen. Hoe komen wij verder? Dat vraagt erkenning van de verschillen maar ook van het zoeken naar een weg vooruit, naar heling. Door verbindende taal (Rosenstock), nederige spiritualiteit (Tolstoja), geweldloze verzoening (Girard).

Dat maakt nieuwsgierig naar de geschiedenis van die verschillende opvattingen en de invloed die ze dagdagelijks ongeweten hebben op de verhoudingen in Europa, de oordelen en de vooroordelen die we binnen Europa ten aanzien van elkaar koesteren. Deze vraag was de
aanleiding tot een gezamenlijke bijeenkomst van de Girardkring met de vereniging Respondeo, welke plaatsvond op 30 september 2023 op uitnodiging van de Girardkring.

In de aanloop naar de gezamenlijke studiedag met de Girardkring werd in Respondeokring gesproken over het thema “In de ban van het schisma”. Doel was om meer zicht te krijgen op de achtergronden van de oorlog tussen Rusland en Oekraïne, maar ook op de tegenstellingen tussen Oost en West binnen Europa. In het gesprek hierover tijdens een Respondeo bijeenkomst op 4 mei 2023, werd zichtbaar dat er destijds rond 1054 ronduit haat heerste tussen de beide hemisferen van Europa. De vraag ligt voor de hand of dat heden ten dage nog steeds het geval is.

De Franken onder Karel de Grote rivaliseerden destijds duidelijk met het Oostelijk gelegen Byzantium en het omgekeerde was eveneens het geval. Rosenstock-Huessy maakt duidelijk dat de keuzen die destijds werden gemaakt in de aanloop naar de definitieve scheiding met grote waarschijnlijkheid ook door ons zouden zijn gemaakt. De aanleiding tot het grote schisma zou je kunnen zien als een kleine dwaze operette, maar was wel het resultaat van verschillen en animositeit die tot 1054 al diepe voren in Europa hadden getrokken. Daarmee was en is het nog steeds een serieuze aangelegenheid die het tot op de dag van vandaag nog steeds is. Niet alleen voor de Kerken die gescheiden wegen gaan, maar ook politiek en economisch. Zoals dat gaat wanneer een probleem onopgelost blijft bestaan, keren ze terug op een ingewikkelder en complexere vorm en de oorlog Rusland – Oekraïne kan in dat licht bezien worden.

In Duitsland spreekt men wel van een gescheiden ‘werkings-geschiedenis’, waarmee wordt uitgedrukt dat de geschiedenis van zo’n proces verschillende ‘mindsets’, verschillende manieren van geloven, denken en handelen oplevert. In Oost en West zijn in de loop van de erop volgende eeuwen op die manier verschillende praktijken ontstaan. Otto Kroesen in zijn artikelen gewijd aan deze botsing tussen Oost en West verwoordt het als volgt:

* “De oosters orthodoxe Kerk heeft altijd aan de westerse kerk verwereldlijking verweten. De kerk hield zich bezig met de regeling van de maatschappij en sterker nog: zowel in het geloof als in de maatschappij ging het om de regels, dogma’s en wetten. De kerk is er om Gods liefde voor erbarmelijke mensen te verkondigen. In de heiligen komt die liefde tot uiting. Zondaren worden door die liefde gered. Zo hoort het.\
Vanuit het westen kwam omgekeerd de kritiek dat de oosters orthodoxe Kerk wel eens wat kritischer naar de overheid mag zijn. De oosters  orthodoxe kerk stelde zich altijd passief op tegenover de politieke macht. Veelal was daarbij het argument doorslaggevend dat elke zondaar de toegang tot God moest kunnen blijven vinden”, aldus Otto in zijn artikelen over de botsing tussen Oost en West.[^1]

Het zijn grove lijnen, de kritiek op het Oosten dat vooral passief ondergaat wat machthebbers over hen uitstorten en de kritiek op de regelzucht waarmee overheden in het Westen hun burgers behandelen en de blinde liefdeloosheid die daarin te vaak schuilgaat.
Natuurlijk zijn dit niet de enige gebeurtenissen die aanleiding geven tot de verschillen tussen Oost en West. Ter relativering hiervan kunnen genoemd worden de eeuwenlange overheersing door de Tartaren in Rusland en delen van Oost-Europa[^2], de Russische Revolutie en de Stalinistische terreur en het gehele Sovjettijdperk. Al deze ontwikkelingen van doodslag en geweld doen al vanaf het eind van de middeleeuwen hun invloed gelden. Ze hebben diepe sporen nagelaten in de levenshouding van Oost-Europa en de Russen in het bijzonder. Wanneer Europa inderdaad een waardengemeenschap wil zijn, dan zal zij in deze geschiedenissen en niet alleen in de eigen westerse ontwikkeling, mogelijkheden moeten vinden om aan de imperatief van een Verenigd Europa gestalte te kunnen geven.

[^1]: Zie: https://www.respondeo.nl/ok-botsing-oost-west-2/
[^2]: De invloed van die overheersing is wellicht groter geweest dan tot nu toe wordt aangenomen. Zie: https://historiek.net/mongoolse-beze?ng-rusland/147594/

Daarmee komen we aan bij de vraag of er ‘Geestelijke heling van een verdeeld Europa’ mogelijk is. De vraag laat zich ook omkeren: Is een verenigd Europa blijvend mogelijk zonder een dergelijke geestelijke heling, niet in de laatste plaats met het oog op de holocaust geschiedenis in een christelijk Europa?

Met deze toekomstgerichte vraag houdt Katja Tolstaja, de huidige theoloog des vaderlands[^3], zich bezig. Als keynote speaker van de gezamenlijke bijeenkomst van Respondeo en de Girardkring op 30 september 2023 in de Dominicuskerk in Amsterdam, richt zij zich op de ontmenselijkende processen waaraan Rusland en de Oost-Europese landen zijn blootgesteld tijdens het tijdperk van het Sovjetregime. Ontmenselijking is dan ook een centrale kwestie in haar inleiding[^4] waarin ze laat zien wat in zowel Oost als in West-Europa heeft plaatsgevonden aan geweld, massamoord en ontkenning en beroving van al wat menselijk is aan de mensen. Vooral ook, hoe wij daar terecht kunnen komen. En welke gevolgen dat heeft, niet alleen voor individuen, maar voor hele samenlevingen. Katja Tolstaja wil een theologie ontwerpen, waarin aan deze noden tegemoet wordt gekomen en die ons in staat stelt de gevolgen ervan te boven te komen. En eigenlijk kan dat niet anders wanneer ik haar goed begrijp, dan door de vraag naar de mens centraal te stellen. Kun je wel spreken over een kern van het mens- zijn, bestaat de goede wilde wel (Rousseau), is de mens wel van goddelijke oorsprong, is de mens wel beelddrager Gods? Ze oriënteert zich daarbij op het beeld van de mens zoals dat realiter is geleefd in de concentratiekampen en in de Goelagarchipel te confronteren met wat in allerlei theologie is gezegd en geschreven over dat beeld van de mens. Beschrijvingen van het leven in concentratie en Goelag kampen, van mensen als Primo Levi en Varlam Sjamalov, die aan de randen van de afgrondelijkheid van het menselijke bestaan hebben geleefd, blijken hier grote invloed te hebben. Het Imago Dei, de mens als beeld van God, of profaner uitgedrukt, de mens die ‘in wezen’ goed is, die eigenlijk wel deugt, kan dat worden volgehouden of is het tijd om dat beeld te verlaten? Moeten we daarover niet anders gaan denken? Ze geeft een diepborende analyse, met gevolgen voor wetenschappen als theologie en antropologie.

[^3]: Katja Tolstaja was in 2023 de Theoloog des Vaderlands en bekleedt aan de VU de leerstoel “Theologie en Religie in Post-trauma?sche samenlevingen.

[^4]: De inleiding die is afgedrukt in dit blad is de oratie die mevrouw Tolstaja hield bij haar inauguratie als hoogleraar aan de VU. In de bijdrage die ze uitsprak volgde ze, zoals ze ook aangaf, deze oratie. Vandaar dat ervoor gekozen is om deze te vertalen.

De tweede inleiding, van de hand van Otto Kroesen, gaat in op de botsing van niet alleen van de ‘mindsets’ van Oost en West, maar ook op de geschiedenis ervan. Hoe zijn deze referentiekaders historisch gegroeid. Juist in de beschrijving van die historie is hij gericht op de vraag hoe we een Europese toekomst kunnen bouwen met behulp van deze verschillen, die we geërfd hebben van het verleden. Die intentie veronderstelt een breuk met de westerse zelfgenoegzaamheid.

In zijn bijdrage laat Otto Kroesen dan ook kritiek horen op de Westerse houding, met z’n vaak veel te korte tijdsperspectief. Instituties die in een lange geschiedenis zijn verworven en bevochten, worden voor vanzelfsprekend aangenomen. Die instituties zullen wel voor ons burgers blijven zorgen: We hebben het immers toch goed geregeld. Ondertussen lijkt het erop dat deze verworvenheden aan erosie onderhevig zijn.

Het bestaan van instituties heeft echter inspiratie nodig, die onderhouden dient te worden. Hij stelt daarom de vraag; wat kunnen we daarin - of misschien beter geformuleerd, wat hebben we van het Oosten, van de Oosterse Orthodoxie te leren?

De derde inleider Wiel Eggen, breidt ons perspectief verder uit in de breedte. We hebben in Europa te maken met tal van verschillende geestelijke stromingen. Er is niet alleen een verhaal tussen Oost en West, maar ook tussen Christendom en Islam. Het gesprek dat we moeten voeren is niet bipolair, maar multipolair. Hij beperkt zich echter tot een trialoog tussen Oost en West en Islam. Het zijn alle stromingen die zich beroepen op de Bijbel. Via een analyse volgens de mimetische benadering van Girard toont hij aan dat het noodzaak is dat de religies van Europa een gesprek met elkaar moeten aangaan. Ze zijn allemaal voortgekomen uit het Evangelisch gedachtengoed en hebben gemeenschappelijke wortels, die aanleiding waren tot verschillende opvattingen over de mens. Het westen was meer gericht op een verondersteld rechtszaak tussen God en mensen, het Oosten legde meer nadruk op de mens als het beeld van God en de onderlinge harmonie. De mens moet daarin beeld zijn/worden van Gods eenheid van personen. Dat wordt ook in de Islam (h)erkend. De barmhartigheid van de volstrekt ene God houdt oog, ook voor datgene wat buitengesloten wordt. In haar ontkenning van de Triniteit door de Islam noopt de barmhartigheid waaraan zij zich onderwerpt, tot zorg voor het uitgesloten standpunt. Dat geeft voldoende basis voor gesprek tussen deze stromingen die in heel de geschiedenis op gespannen voet met elkaar hebben gestaan. Een gesprek dat gericht moet zijn op vrede en zorg voor het verdrukte en de verdrukten.

In aansluiting op de drie inleidingen voegt Wilmy Verhage een reelectieve bijdrage toe. Ze trekt daarin een aantal lijnen door de geschiedenis van de tijd van Rome tot het heden. Daarin vat zij een aantal elementen samen uit de bijeenkomst van Respondeo (zie boven).

Een laatste bijdrage is van de hand van Marlouk Alders, lid van Respondeo. Als direct betrokkene bij het internationale ontmoetingscentrum in Kreisau, schrijft ze over de totstandkoming ervan. Dit centrum is gevestigd op het landgoed Kreisau, waar tijdens WOII de Kreisauer Kreis (een Duitse verzetsgroep) bijeen kwam. Een aantal geleerde mensen sprak daar met elkaar over de vraag hoe Duitsland er na WOII uit zou moeten zien (Neuordnung).
Marlouk Alders laat in haar bijdrage zien wat er op het gebied van 'her-menselijking' al tot stand wordt gebracht in het ontmoetingscentrum Kreisau, aan de hand van haar verhaal over Ewa Unger die de verschrikkingen van de oorlog heeft meegemaakt en die ze daar heeft leren kennen. Daarvan doet ze hier verslag.

Kreisau is opgericht na de val van de muur door mensen, die zich door Rosenstock-Huessy hebben laten inspireren.

Al deze gezichtspunten kunnen natuurlijk worden aangevuld. Niets is volledig uitgewerkt het blijft bij grote lijnen, maar het is wel een aanzet tot verder denken over de verhoudingen binnen Europa en de vraag naar een werelddeel waarin men vreedzaam kan samenleven.
Wij wensen de lezer van dit nummer van Interim dan ook veel leesplezier en stof tot nadenken.

Het ga u goed,\
Jan Kroesen/Wilmy Verhage\
Redactie van Interim.
