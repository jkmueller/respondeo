---
title: "Interim 7 – februari 2016"
thema: "Debet en Credit – Reflecties rond de financieel economische crisis"
category: interim
created: 2016-02
order: 1
---

### {{ page.thema }}


* Inleiding – Wilmy Verhage
* Over Taal, Kapitaal en Toekomst – Henk van Olst
* Sterren van getallen – Otto Kroesen
* De Oikos en de Nomos – Gert van Maanen
* Kapitaal, armoede, samenleving en geloof – Gert van Maanen
* Microkrediet, schuld of zuurstof? – Gert van Maanen
* Geloven, hopen en liefhebben zijn de motoren van de economie – Jan Kroesen
* De schijn-exactheid van het economische denken – Wilmy Verhage
* Recensies:
  * Liefdewerk tot in de eeuwigheid – Marlouk Alders
  * Themanummer over Rosenstock-Huessy – Wilmy Verhage
  * Geld en goed, Lessen voor welwillende kapitalisten – Wilmy Verhage
* Rectificatie ‘De taal is wijzer dan haar spreker’
* Van de bestuurstafel – Greet Oosterbroek-Baars

[Interim 7-2016]({{ 'assets/downloads/interim_7_2016.pdf' | relative_url }})
