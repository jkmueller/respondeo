---
title: Bijeenkomst 25 mei 2019
thema: "Bestaat er een Christelijk Tijdperk?"
category: bijeenkomst
created: 2019-05-25
order: 1
---
### {{ page.thema }}

De vraag naar een Christelijk tijdperk wordt meestal verstaan als de vraag naar een tijd waarin de Christelijke kerk(en) de belangrijkste macht in staat of maatschappij was. Dat is echter niet hoe Rosenstock-Huessy die vraag verstaat. In zijn geschiedenisstudies concludeert hij dat er in oude tijden vier sociale vormen waren die elkaar uitsloten en actief bevochten: de nomadische stammen, astrologische rijken, Hellas en Israel. Deze patstelling ziet hij door Jezus overwonnen omdat deze vier vormen levensaspecten werden van mensen in navolging van hem. Ook vandaag zijn er nog familie clans, workaholics, Netflix junkies en utopisten. Maar er is altijd een andere levensgestalte mogelijk.

Rosenstock-Huessy laat zien welke krachten het oorsprongsdocument van het Christelijke tijdperk vorm gaven. Die krachten zijn nog steeds aan het werk.
![Irene Twello]({{ 'assets/images/irene-twello.jpg' | relative_url }}){:.img-right.img-small}

We lezen en bespreken de slothoofdstukken van De Vrucht Der Lippen, bladzijde 110-126.

Het programma:

| 10.30 | Ontvangst en koffie
| 11.00 | Opening, samen lezen uit De Vrucht der Lippen
| 12.30 | Lunch
| 13:30 | Lezen en open gesprek
|| Koffie en thee
| 14.30 | Ledenvergadering
|| Welkom en vaststelling agenda
|| Stukken over 2018: Notulen vorige ALV, jaarverslag, verslag penningmeester Bestuurszaken
|| Mededelingen en activiteiten van leden
| 15.30 | Sluiting

* Voor de kosten van lunch en zaalhuur wordt een vrijwillige bijdrage gevraagd (richtbedrag 15 euro). Graag vooraf opgeven in verband met de lunch bij
* Op verzoek stuur ik de tekst in pdf op. Maar beter bestel het hele boekje (€ 5,-- plus eventueel verzendkosten). Ik zal ook een aantal nieuwe boekjes meebrengen.

Secretariaat Respondeo:\
Egbert Schroten, Veeningen 24, 7924 PJ Veeningen, tel. 0528-202041. \
e-mail: vereniging.respondeo@gmail.com

[als PDF]({{ 'assets/downloads/2019_uitnodiging_voorjaarsbijeenkomst.pdf' | relative_url }})
