---
title: "Interim 11 – juni 2018"
thema: Vredesprocessen
category: interim
created: 2018-06
order: 1
---

### {{ page.thema }}

INHOUDSOPGAVE
* Inleiding – Jan Kroesen
* Een gezond wij-gevoel – Egbert Schroten
* Zijn wij niet allen vreemdelingen voor elkaar – Marlouk Alders
* Girard over de oorsprong van onze cultuur – Hans Weigand
* Wording van de sociale gestalten volgens Eugen Rosenstock-Huessy – Jürgen Müller
* De Eindtijd – Otto Kroesen
* Girard en de Eindtijd – Philip van Wijk
* Enige Kanttekeningen bij de beteugeling van geweld – Wilmy Verhage
* Uit het bestuur – Egbert Schroten
* In Memoriam Feico Houweling – Otto Kroesen
* Achterpagina


[Interim 11-2018]({{ 'assets/downloads/interim_11_2018.pdf' | relative_url }})
