---
title: "Interim 3 – december 2013"
thema: De Taal van de Ziel
category: interim
created: 2013-12
order: 1
---

### {{ page.thema }}

* Introductie – Wilmy Verhage
* Spreken met hart en ziel – Henk van Olst
* The Language of the Soul: Suffering, Shame, Courage – Tom Duncanson
* Please Call Me by My True Names – Janneke van Damme
* Expositie “Respondeo etsi Mutabor” Eugen Rosenstock-Huessy (188-1973) – In gesprek met vrienden – Thomas Dreessen
* Een herinnering aan Douwkje Mulder – Agnes Lieverse
* Een herinnering aan Frans ten Kate – Feico Houweling
* Uitnodiging tot deelname aan een vertaalproject
* Voorjaarsbijeenkomst/ALV op 22 maart
* Lezing over de Kreisauer Kreis op 30 maart  
* Conferentie in Canada 19 tot 22 juni
* De Taal van de Ziel 28 juni 2014
* Uit het bestuur
* Vereniging Respondeo  
* Over Interim


[Interim 3-2013]({{ 'assets/downloads/interim_3_2013.pdf' | relative_url }})
