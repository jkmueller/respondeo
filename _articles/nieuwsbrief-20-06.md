---
title: Reactie discussie over Charles Taylor en Rosenstock-Huessy
category: nieuwsbrief
created: 2020-05
order: 1
---
Het is al enige tijd geleden dat de eerste digitale nieuwsbrief is rondgestuurd. Er is welgeteld één reactie op gekomen, van Wim van der Schee. Bedoeld om direct onder de oorspronkelijke tekst (de laatste bladzijde van die genoemde nieuwsbrief) te zetten :
Ik geef het hieronder onbewerkt weer, het kan prima als oproep gelezen worden om ook te reageren. Daar is tijd voor, zie verderop:

Een paar vragen:
  1.      Wat is nu meer precies en concreet het punt waarover we van gedachten wisselen en is het een punt dat ook urgentie heeft voor ieder van jullie zelf? Wat dan? Otto komt het dichtst bij zo’n punt – lijkt me – met zijn vraag over ‘strong values’ of  ‘gebodsethiek’? Otto, wat maakt het uit voor jou?
  2.      Waar blijft Bas. Zijn stuk of de kerk de maatschappij een weg kan wijzen telde toch ook mee? Klopt het met onze ervaring dat wij ‘zielen op de snelweg’ zijn geworden? En is dat erg of hoopvol of eerder een observatie?

Opmerkingen:
  1.      Het lijkt me in de geest van Rosenstock dat we ons beperken tot wat echt nodig is, omdat het op onze lippen brandt of onze liefde heeft. Ik houd van Taylor, omdat hij pleit voor een veel breder kennisbegrip dan tegenwoordig erkenning vindt. Dat is érg nodig, omdat het heersend kennisbegrip zich vereenzelvigt met ‘greep nemen’, met het instrumenteren van onze menselijkheid, met als zoethouder dat het ons comfort oplevert (voor wie en voor zo lang het betaalbaar is).

  2.      Strong values of gebodsethiek? Ik denk dat het een kwestie van ‘milieu’ is. De professor zegt het zus en de protestantse gemeente zegt het zo. Waar het op aan komt is dat we in het concrete geval allemaal beseffen wat goed of kwaad is om te doen.

  3.      ‘Zielen op de snelweg’. Is dat niet ongeveer hetzelfde als te zeggen dat wij onze eenheid zullen vinden in het besef allemaal gelijkwaardige bewoners / medeburgers te zijn van dat éne huis (deze planeet), waarop wij ons als de bliksem kunnen verplaatsen en ook met vuur spelen? Ons moreel besef /ons Godsgeloof zal in het komend tijdperk geijkt worden op dit eensgezind belijden. Zo begrijp ik Bas en mezelf.

Vriendelijke groet, Wim  

Ik haal hier dan maar het (misschien wel te vaak gebruikte) citaat van stal: "es kann gar nicht langsam genug gehen".  Toch maar even opgezocht op Google. Uit een "Wegweiser für Deutsche Lehrer" uit 1838:
![nicht langsam genug]({{ 'assets/images/nicht-langsam-genug.jpg' | relative_url }}){:.img-left.img-xxlarge}


Dit is een advies voor docenten om geduldig te zijn met hun pupillen in het proces van opbouw (verandering van die pupillen). Maar het kan heel goed zijn dat het niet terecht is om in ons geval te denken dat er een verschil gemaakt moet tussen docenten en pupillen.

Vandaar mijn oproep:
- het podium is open voor wie wil reageren, lang of kort, eigen tekst of een advies om iets moois te lezen.
- dit podium kan bereikt worden door je aan te melden op de Whatsapp groep (geef daarvoor je 06-nummer aan mij door), of via deze mails (stuur de reactie als antwoord op deze mail).


--
met vriendelijke groet,

Egbert Schroten, secretaris Vereniging Respondeo
