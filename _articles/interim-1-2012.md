---
title: "Interim 1 – najaar 2012"
thema: Introductie
category: interim
created: 2012-09
order: 1
---

### {{ page.thema }}

* Introductie – Wilmy Verhage
* Wat is de Christelijke Bijdrage aan Vrede? – Jean Jacques Suurmond.
* Wat mij heeft geïnspireerd in de bijdrage van J.J. Suurmond – Agnes Lieverse-Opdam
* Kan het christendom bijdragen aan de vrede? – Rudolf Kooiman
* Het kruis der werkelijkheid en de werkelijkheid van het kruis – Otto Kroesen
* 40 Jaar Rosenstock-Huessy Huis – Marlouk Alders
* Wereldjes in contrapunt – Jan Kroesen
* Voorjaarsbijeenkomst 2012: ‘Werk en leven’ – Feico Houweling
* Uit het bestuur – Feico Houweling
* Vereniging Respondeo
* Over Interim

[Interim 1-2012]({{ 'assets/downloads/interim_1_2012.pdf' | relative_url }})
