---
title: "Eugen Rosenstock-Huessy: De grote revoluties"
category: boek
order: 1
---
![De grote revluties]({{ 'assets/images/De-grote-revoluties.jpg' | relative_url }}){:.img-left.img-large}

Onze Europese waarden en normen zijn bevochten op bepaalde plaatsen door bepaalde mensen. Stuk voor stuk zijn ze verbonden aan een historische gebeurtenis, bijvoorbeeld ons recht om in vrede op te groeien, om gezond te zijn, om onze indivudele talenten te ontwikkelen, om een eigen beroep te kiezen en niet gebonden te zijn aan de gelofte van onze ouders, onze vrijheid om ons eigen onderwijs te kiezen en onze bewegingsvrijheid. Bij elk van deze vrijheden en waarden, hoort een jaartal. Hij zijn geboortemomenten als bijvoorbeeld de twee wereldoorlogen, de Russische revolutie en de Franse revolutie.

Dat onze westerse beschaving uit de revoluties van de afgelopen duizend jaar is voortgekomen is het verrassende vertrekpunt van De grote revoluties. Wie de ziel van Europa wil leren kennen moet deze Autobiografie van de westerse mens lezen.

**Het is een fascinerend, erudiet en uitermate leesbaar boek dat grote verbanden zichtbaar maakt.**

De Engelse editie “Out of Revolution – autobiography of western man” en Duitse editie worden al jarenlang herdrukt. Nu is de Nederlandse tekst van deze ‘klassieker’ in de voortreffelijke vertaling van Feico Houweling beschikbaar.

Te bestellen bij de uitgever
 [Skandalon](http://www.skandalon.nl/shop/theologie-cultuur/371-de-taal-van-de-ziel.html). De vereniging Respondeo heeft de uitgave financieel mogelijk gemaakt.
