---
title: Bijeenkomst 10 februari 2018
thema: "agenda Studiedag Respondeo/Girard"
category: bijeenkomst
created: 2018-12-01
order: 1
---
### {{ page.thema }}

Beste deelnemer,

Voor de studiedag Girard – Rosenstock-Huessy zullen we het onderstaande dagverloop volgen. Het is een vol programma, daarom beginnen we vroeg. Uiteraard is het programma een hulpmiddel en geen dwangbuis…\
(Vooral) voor mensen die van ver komen: schroom niet om aan te schuiven als we al begonnen zijn.\
Als je contact op wilt nemen (bijvoorbeeld bij problemen onderweg) kun je mij (Egbert) bellen: 06-36354837

Dominicuskerk, Spuistraat 12 te Amsterdam (5 minuten lopen vanaf het stationsplein Amsterdam CS).

| 10 uur | ontvangst
| 10.15  | opening door Nico Keijzer (Girardkring), voorstellen
| 10.30-11.00 | inleiding Geweld en de oorsprong van de cultuur (Hans Weigand, Girardkring)
| 11.00-11.15 | respons Juergen Mueller (Respondeo)
| 11.15-11.30 | pauze
| 11.30-12.00 | doorpraten (in groepen)
| 12.00-12.30 | algemene discussie
| 12.30-13.30 | lunch
| 13.30-14.00 | inleiding [Eindtijd en de globale samenleving]({{ 'assets/downloads/kroesen_otto_erh_De_Eindtijd.pdf' | relative_url }}) (Otto Kroesen Respondeo)
| 14.00-14.15 | respons ( Girardkring)
| 14.15.14.30 | pauze
| 14.30-15.00 | doorpraten (in groepen)
| 15.00-15.30 | algemene discussie
| 15.30-16.00 | afsluiting/borrel

Voor de zekerheid voeg ik nogmaals een leestekst ter voorbereiding bij.

Met vriendelijke groet,

Egbert schroten, Secretaris Respondeo

[Hans Weigand: De kracht van de tweedepersoonsperspectief](https://sophieonline.nl/de-kracht-van-het-tweedepersoonperspectief-over-de-verwantschap-tussen-rosenstock-huessy-en-girard/)
