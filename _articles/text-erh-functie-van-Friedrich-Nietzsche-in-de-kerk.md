---
title: "Rosenstock-Huessy: De functie van Friedrich Nietzsche in de kerk
en de crisis van theologie en filosofie"
category: online-text
published: 2024-03-18
org-publ: 1942
---
#### De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie

*door*\
*Eugen Rosenstock-Huessy*

I.	De eenheid van gezondheid en waanzin als nieuw historisch fenomeen\
II.	De verovering van de ruimte van 1100 tot 1900: Thomas, Cusanus, Paracelsus, Descartes.\
III.	Nietzsche's programma voor de verovering van de tijd.\
IV.	Nietzsches maskers als prijs voor zijn programma.\
(IVa.     Inzet 1944: Nietzsche's maskers.)\
V.	De vergoddelijking van de mens in de kerk. \
VI.	Godslastering en waanzin.\
VII.	Theologie en Filosofie hebben de degens verwisseld.

IVa.     Inzet 1944: Nietzsche's maskers.

*(Paper voorbereid voor de gezamenlijke bijeenkomst van de American Association of Church History en de American Historical Association in december 1942. Cf. brief van Stanley Pargellis 18 mei 1943)*

#### 1.	De eenheid van gezondheid en waanzin als een nieuw historisch fenomeen

Nietzsche is controversieel. Zijn betekenis heeft zoveel facetten dat Charles Andler vijf verschillende aspecten van zijn werk in vijf afzonderlijke delen behandelde. Een kort artikel over zo'n historische berg kan alleen waardevol zijn als het volgens een strenge discipline en methode verloopt. Daarom zal ik het niet hebben over Nietzsches plaats in de geschiedenis van het Duitse denken, noch over de crisis van het Humanisme in de engere zin van zijn verering van de Griekse en Romeinse oudheid, zelfs niet over zijn plaats in het verval van Europa of in de evolutie van de biografie.

Mijn onderwerp is Nietzsches functie in de Kerk en in de crisis van theologie en filosofie.

...

*de complete tekst:* [De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ 'assets/downloads/rosenstock_De_functie_van_Friedrich_Nietzsche_in_de_kerk.pdf' | relative_url }})

Dit is een vertaling van een [tekst geschreven in het Engels](https://www.rosenstock-huessy.com/text-erh-nietzsches-function-1942/).

[naar het begin van deze pagina](#top)
