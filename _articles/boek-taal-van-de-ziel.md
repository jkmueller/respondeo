---
title: "Eugen Rosenstock-Huessy: De taal van de ziel"
category: boek
order: 1
---
![De taal van de ziel]({{ 'assets/images/taal-van-de-ziel-1.jpg' | relative_url }}){:.img-left.img-small}
‘De taal van de ziel’ is de nieuwe Nederlandse titel van de vertaling van ‘Angewandte Seelenkunde’. In deze befaamde tekst, die voortkomt uit de discussie met Franz Rosenzweig, doet Rosenstock-Huessy voor het eerst zijn grammaticale methode uit de doeken.

De vertaling is van Henk van Olst, Hans van der Heiden en Otto Kroesen. De vereniging Respondeo heeft de uitgave financieel mogelijk gemaakt.

Bestellen bij de uitgever [Skandalon](http://www.skandalon.nl/shop/theologie-cultuur/371-de-taal-van-de-ziel.html).
