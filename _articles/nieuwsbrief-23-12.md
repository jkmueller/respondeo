---
title: Nieuwsbrief december 2023
category: nieuwsbrief
created: 2023-12
published: 2023-12-29
order: 1
---
Beste leden van Respondeo,

Bij deze de reeds aangekondigde nieuwsbrief. Deze is gewijd aan de onderwerpen die momenteel
binnen onze vereniging aan de orde zijn en die naar ons voornemen ook nog wel even aan de orde zullen blijven. En het is veel dat ons bezighoudt, niet in de laatste plaats ook door de recente verkiezingsuitslag met het riante resultaat voor de PVV. Meer dan eens zijn onze zorgen uitgesproken over de vraagstukken in Europa, de polarisering, de vraagstukken rond immigratie, inburgering aangelegenheden, identiteitskwesties en klimaatcrisis. Het lijstje is niet uitputtend. Het laat zich aanzien dat de geluiden daarover in de politiek over deze zelfde thema’s niet zullen verstommen, maar nog aan zullen zwellen. Wij proberen in de keuze van de thema’s daar rekening mee te houden en er op te anticiperen.

Allereerst de volgende bijeenkomst (in eigen kring) en de themakeuze. De datum ervan is vastgesteld (zie ook de website) op

**4 mei 2024**

### Nietzsche als christelijk denker

[nieuwsbrief 23-12]({{ 'assets/downloads/2023_Nieuwsbrief_van_de_Vereniging_Respondeo_18-12-23.pdf' | relative_url }})
