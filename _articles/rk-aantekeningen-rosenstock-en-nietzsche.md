---
title: "Rudolf Kooiman: Rosenstock en Nietzsche"
category: essay
essay-nr: 180
created: 2024-05-05
published: 2024-05-12
landingpage:
order-lp: 4
---
### Rosenstock en Nietzsche

#### Aantekeningen bij het gesprek n.a.v. de lezing “De functie van Nietzsche in de kerk en de crisis van theologie en filosofie” door Eugen Rosenstock-Huessy

Over Friedrich Nietzsche valt veel, héél veel te zeggen. Daarom legt Rosenstock zichzelf een beperking op en legt, in de lezing die hij hield in mei 1943, de nadruk op Nietzsche ’s betekenis voor theologie en filosofie.
<!--more-->
![Rudolf Kooiman]({{ 'assets/images/rudolf-kooiman.jpg' | relative_url }}){:.img-right.img-small}

Zie: [De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ '/text-erh-functie-van-friedrich-nietzsche-in-de-kerk' | relative_url }})

Vóór Nietzsche suggereerde zowel de filosofie als de theologie objectiviteit en algemene geldigheid. Filosofen als Thomas van Aquino en René Descartes construeerden een objectief wereldbeeld. De ontdekking van Nietzsche: er is geen onafhankelijke waarheid, de waarheid bestaat uit feiten, maar is ook normatief en relationeel. Daarom kan niemand de absolute waarheid claimen en moeten we luisteren naar elkaars invalshoek.

Met de uitspraak “God is dood” bedoelde Nietzsche: de god van de filosofen (en theologen) is dood, de geobjectiveerde god is dood. Met als gevolg dat wij onze oriëntatie kwijt zijn en zijn losgeraakt van eenduidige betekenissen van woorden bijvoorbeeld van het woord ‘god’, ‘mens’ en ‘deskundig’.  Nietzsche voorzag een overgangsperiode van 2 eeuwen waarin we de god van de filosofen achter ons laten. Voor Nietzsche bleef alleen het kwaad over en zijn grote vraag was: wat kunnen wij daar tegenover stellen?

Vóór Nietzsche was de algemene opvatting van theologie en filosofie dat achter onze zichtbare wereld een onzichtbare wereld schuilgaat: b.v. de ideeënwereld van Plato, het walhalla of de hemel

Nietzsche zegt: die onzichtbare wereld bestaat niet. Een geordende wereld bestaat niet. Onder de z.g. orde gaan allerlei instincten en driften schuil.

Rosenstock zag zichzelf als schatplichtig aan Nietzsche en sloeg tegelijk andere wegen in. De radicale opvatting van Nietzsche (die zich niet voor niets de filosoof met de hamer noemde) m.b.t. de tijd en de waarheid nam hij over; en hij moest ook niets hebben van een blauwdruk (zoekontwerpen, ballonnetjes, wereldbeelden, mensbeelden) waar we ons aan vasthouden. De Bijbel als verhaal van onze wording stond voor hem centraal met de christelijke feesten (of moet je zeggen: de liturgie)  als leidraad: Kerstfeest als feest van de dankbaarheid, feest van geboorte en gegeven leven. Goede Vrijdag en Pasen als beeld van een leven door conflicten heen (Jezus: “het oude moet sterven om het nieuwe mogelijk te maken”) en Pinksteren als feest van nieuwe inspiratie.

Openbaring had voor hem te maken met onze mogelijkheid om een bijdrage te leveren aan een nieuwe hemel en een nieuwe aarde.

Voor hem was cruciaal dat waarheid niet losstaat van onze biografie.
met kruis en opstanding als de vorm van ons leven.

Algemeenheden, abstracties hebben afgedaan zegt Rosenstock Nietzsche na.

>*Rudolf Kooiman*
