---
title: "Interim 12 – juni 2019"
thema: Gelijktijdig Ongelijktijdig
category: interim
created: 2019-06
order: 1
---

### {{ page.thema }}

INHOUDSOPGAVE
* Inleiding – Jan Kroesen
![Lise en Gerda van der Molen]({{ 'assets/images/Lise-Gerda-van-der-Molen.jpg' | relative_url }}){:.img-right.img-large}
* Interview met Lise en Gerda van der Molen – Jan Kroesen, Wilmy Verhage
  * Imperatieven en schermutselingen Internationalisering en Techniek
  * In den beginne
  * Taal en tijd
  * De breuk
  * Het lezen van Rosenstock-Huessy
  * Heil en onheil
  * Menselijk bestaan tussen heil en onheil
  * Rust en onrust – Vrede en Sterven
* Bericht van het bestuur – Otto Kroesen
* Achterpagina


[Interim 12-2019]({{ 'assets/downloads/interim_12_2019.pdf' | relative_url }})
