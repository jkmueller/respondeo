---
title: "Boeken voor de najaarsbijeenkomst 2021"
category: essay
essay-nr: 130
created: 2021-08-08
published: 2021-08-08
---
We vervolgen het thema identiteit en gaan in op hoe we als samenleving hiermee verder komen. Wat is er nodig om te voorkomen dat we ons in groepen opsluiten en elkaar de rug toe keren?

**In de aanloop van deze bijeenkomst (30 oktober in Twello) komen een aantal publicaties en reacties op onze website.
De basis komt uit een drietal actuele boeken:**
<!--more-->
### Polarisatie van Bart Brandsma

In het boek “Polarisatie” verkent Bart Brandsma de dynamiek van het wij-zij denken en pleit hij voor een nieuwe invalshoek. “Het zou al helpen”, zegt hij, “als we het fenomeen niet alleen als een probleem zien”. Maar het is voor hem nog belangrijker om ons niet als probleemeigenaren op te stellen. Dat is namelijk het verschil tussen een conflict en polarisatie, zegt hij. In polarisatie d.w.z. wij – zij denken is er altijd een keuze om je wel of niet als probleemeigenaar op te stellen. We kunnen er voor kiezen of we wel of niet meegaan in het zwart-wit denken.

### Mijn ontelbare identiteiten van Sinan Çankaya

Een mens wil meer betekenen dan zijn of haar eigen leven. Çankaya doet dat door zich in dienst te stellen van de multiculturaliteit. Hij laat overtuigend zien dat de zoektocht naar je eigen identiteit geen loutering met een glorieus einde in de goede groep is (eindelijk thuis). Het komt in het boek veel meer in de buurt van gedwongen worden tot stellingname: Hoor ik in deze situatie bij de migrant uit een achterstandsbuurt, of ben ik de rationele academicus? Ben ik moslim of laat ik geloofszaken over aan mijn moeder? Stiekem speelt ook de keus mee: Blijf ik aan de kant staan als observeerder of gaat het mij persoonlijk aan?

### Schlacht der Identitäten van Hamed Abdel-Samad

20 Thesen zum Rassismus – und wie wir ihm die Macht nehmen

Rassismus betrifft uns alle, er ist wie eine chronische Krankheit, die
sich in der DNA der Menschheit seit Jahrtausenden festgesetzt hat.
Die Entstehung und den Verlauf einer Krankheit zu verstehen, kann
uns zwar nicht immer helfen, diese auch zu heilen, aber es kann uns
Hinweise liefern, wie wir mit ihr leben können, ohne dass sie unser Leben
dominiert oder den gesellschaftlichen Frieden bedroht. In diesem
Buch will ich daher eine Art Weltreise meiner Rassismuserfahrungen
nachskizzieren, ohne daraus einen Betroffenheitsbericht zu machen.
Ich will weder klagen noch anklagen noch emotionale Apelle in die
Welt senden, mit denen sich Rassisten ohnehin nie wirklich erreichen
lassen. Ich will stattdessen versuchen, das Phänomen in seiner
Vielschichtigkeit zu dekonstruieren, um es zu verstehen. Das Buch
richtet sich an Opfer von Rassismus, aber auch an jene Menschen, die
sich für Antirassisten halten, ohne selbst frei von Rassismen zu sein. Es ist
insofern auch eine Einladung zur Reflexion und zur Überprüfung des
eigenen Handelns und Denkens.
