---
title: "Otto Kroesen: De botsing tussen Oost en West in Europa (2)"
category: reactie
essay-nr: 170
created: 2023-06-12
published: 2023-06-25
---

#### Regelen of met liefde doordringen

Het Westen met de katholieke kerk en Rusland met de orthodoxe kerk zijn uit elkaar gegroeid. In het Oost Romeinse Rijk bleef het keizerrijk bestaan. Politiek was de kerk daar altijd van afhankelijk. Het West Romeinse Rijk ging ten onder door de val van Rome in 476, en ook door de inval van veel stammen die zich vestigden binnen haar grenzen. In het Westen verliep de ontwikkeling daardoor veel chaotischer. Dat gaf de kerk aanleiding zich meer dan in het oosten met de politieke verhoudingen te bemoeien en de aardse verhoudingen te regelen.

#### Deze wereld anders

Karel de Grote heeft bij zijn veroveringen in de achtste eeuw wel met de paus samengewerkt, maar de paus werd door hem in een ondergeschikte positie geplaatst. Voor Karel de Grote was het belangrijk dat hij niet gezien werd als vertegenwoordiger van slechts een stam, de Franken. Als hij gezien werd als christelijke keizer met de paus naast zich, kon hij met veel meer overtuiging gezag claimen over de andere stammen ook [^1].

Na zijn dood begonnen de grootgrondbezitters ieder rond hun kasteel en met hun klooster hetzelfde te doen. Ze maakten de priesters en monniken ondergeschikt aan hun belangen [^2]. Met moeite hielden de opvolgers van Karel de Grote de boel een beetje bij elkaar. De Duitse keizer Hendrik IV probeerde in de 11e eeuw de eenheid te bevorderen door een geloofwaardige paus in Rome te installeren. De pausen zelf waren in die tijd verworden tot weinig meer dan lokale heersers van Rome. Die geloofwaardige paus werd uiteindelijk Gregorius VII en die begon nu net de afhankelijkheid van de kerk ter discussie te stellen.

In 1076 kwam het tot een uitbarsting: de zogenaamde investituurstrijd, de strijd om de benoeming van de bisschoppen. Dat was een strijd die in alle lagen van de maatschappij gevoerd werd. Ook op lokaal niveau wilde men een einde maken aan de machtswillekeur van de roofridders. Het was de kerk die daarvoor streed. Voor het eerst ging de kerk wereldveranderend ingrijpen [^3]!

#### Regels en geestelijken

De kerk probeerde vooral de politieke praktijk te uniformeren: wetten moesten opgeschreven worden en niet arbitrair zijn. Overal moesten zoveel mogelijk dezelfde wetten gelden, en de kerk begon daar zelf mee door zeven sacramenten in te stellen. Er werd voortdurend strijd gevoerd tussen de partij van paus en de partij van de keizer over wederzijdse bevoegdheden en betrekkingen.

In die machtsstrijd was er ruimte voor de steden en gilden om zichzelf van onderop te organiseren. Zij werden daarin door de paus en door de bedelorden gesteund. Gewone mensen en vaak weggelopen horigen namen daaraan deel. Geestelijken waren nu niet meer heiligen die afhankelijk waren van de inspiratie van het moment, maar zij werden een beroepsgroep die gestudeerd had, in het recht of in de theologie [^3].

Men moet ook begrijpen dat alle wereldlijke heersers, koningen en keizers, destijds van oorsprong alleen maar militaire leiders waren. Al het andere werd geregeld door de kerk: ziekenzorg, onderwijsinstellingen, zelfs technische innovaties in de landbouw en dergelijke.

#### Het verwijt van de orthodoxie

De oosters orthodoxe Kerk heeft altijd aan de westerse kerk verwereldlijking verweten. De kerk hield zich bezig met de regeling van de maatschappij en sterker nog: zowel in het geloof als in de maatschappij ging het om de regels, dogma’s en wetten. De kerk is er om Gods liefde voor erbarmelijke mensen te verkondigen. In de heiligen komt die liefde tot uiting. Zondaren worden door die liefde gered. Zo hoort het [^4].

Vanuit het westen kwam omgekeerd de kritiek dat de oosters orthodoxe Kerk wel eens wat kritischer naar de overheid mag zijn. De oosters orthodoxe kerk stelde zich altijd passief op tegenover de politieke macht. Veelal was daarbij het argument doorslaggevend dat elke zondaar de toegang tot God moest kunnen blijven vinden [^5].

Wat de kerk in het Westen destijds begonnen is heeft een voortzetting gevonden in het werk van seculiere organisaties: de samenleving organiseren door regels en wetten en redeneringen (filosofie, wetenschap) die een “dwingend” karakter hebben. Wij kennen dat ook: voor alles is een regeling. En als de regels uit de hand lopen, zoals in de toeslagen-affaire, dan komt er een andere regeling die de gaten moet vullen [^6]. Dan is armoedebestrijding meer een zaak van het hoofd dan van het hart. Dat is het verwijt vanuit het oosten: het Westen is te veel hoofd en te weinig hart.

#### De Russische maatschappij

Maar dan de Russische maatschappij: de kerk is passief, en de overheid had alleen militaire doelen. De tsaar Peter de Grote heeft ingezien hoe het Westen een voorsprong had genomen in maatschappelijke organisatie. Hij probeerde daar van alles van over te nemen om Rusland tot ontwikkeling te brengen. Maar het lukte steeds niet. De maatschappij bleef een zaak van grootgrondbezitters en arme boeren. Rusland had bijna geen steden. Petersburg was op het Westen gericht, en dat was vooral goed voor inkomsten door export. Hoe moest dat verder? In de 20e eeuw in de Russische revolutie van 1917 kwam dat tot een uitbarsting. Op de een of andere manier moest de Russische maatschappij toch ook het westerse organisatievermogen beërven. Maar hoe moeilijk is dat?

[^1]: Zie Rosenstock–Huessy, Die Furt der Franken, in E.Rosenstock-Huessy, E., Wittig, J., 1998. Das Alter der Kirche, in Agenda, Munster, pp.463 ff. (Orig. 1928).

[^2]: Zie Moore, R.I., 2000. The first European Revolution, 970-1215, Blackwell Publishing, Oxford.

[^3]: Zie Rosenstock–Huessy, E., 1989. Die Europäischen Revolutionen und der Charakter der Nationen, Moers, Brendow (Orig. 1931).

[^4]: Zie K.S. Aksakow, Ausgewählte Schriften, in Hans Ehrenberg, 1923. Östliches Christentum, Oskar Beck München 88, ff.

[^5]: Karsawin, in Der Geist des Russischen Christentums, wijst op de al-eenheid van God en mens, die ook de kwade mogelijkheid in de liefde opneemt, Bulgakow in Kosmodizee, verzet zich tegen het eschatologische karakter van de katholieke kerk in het Westen, die een dualistische neiging verraadt. Ook de mogelijkheid van het verkeerde en kwade wordt opgenomen in de zelfopofferende liefde van de Vader en de Zoon, zie Ehrenberg, Östliches Christentum, Band II, respectievelijk pp.314 ff., en pp.462 ff.
[^6]: Voor Duitse lezers: Gedurende meer dan 10 jaar heeft de belastingdienst in Nederland door middel van profilering middels algoritmen onterecht duizenden mensen van toeslagen voor kinderopvang en dergelijke beroofd waartoe deze mensen gerechtigd waren, waardoor mensen hun huis uitgezet werden, vele huwelijken opbraken en kinderen uit huis geplaatst werden, alles vanwege schulden en geldgebrek.

[De botsing tussen Oost en West in Europa (3)]({{ 'ok-botsing-oost-west-3' | relative_url }})
