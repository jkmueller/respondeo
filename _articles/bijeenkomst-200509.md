---
title: Bijeenkomst 9 mei 2020
thema: "De moderne mens van Charles Taylor"
category: bijeenkomst
created: 2020-05-09
order: 1
---
### {{ page.thema }}

**Studiedag rond A secular age (Charles Taylor), leven in voorstad en fabriek (Rosenstock-Huessy), en het kleine formaat van de moderne mens (Bas Leenman).**

De Vereniging Respondeo was van plan de studiedag in het voorjaar 2020 in het teken te zetten van de Moderne Mens. Vanwege de Corona-maatregelen is deze bijeenkomst niet doorgegaan. Hieronder toch de aanzet om dit onderwerp op je in te laten werken.

*De moderne mens leeft in fragmenten, hoort nergens bij*\
*(of overal een beetje),* \
*is agnostisch, atheïstisch, of areligieus, maar* \
*is ook post-seculier,* \
*gaat op pelgrimage, of naar de Mattheus-passion*

In zijn boek a secular age ziet Taylor onze Westerse, seculiere samenleving toch als voortzetting van de tijd daarvoor. Dat zou je een mooi Rosenstock-inzicht kunnen noemen. Maar Rosenstock trekt het ook breder: wij zijn deel van een onpersoonlijke moderne maatschappelijke machine en voelen daarom geen persoonlijke verantwoordelijkheid. Wij zeggen steeds minder vaak ‘ik’, maar als we een mening hebben doen we mee met een heersende trend. Kan er zo nog een voortgaande heilsgeschiedenis zijn? En wat is dan de rol van de kerk?

We lezen fragmenten van Taylor, Rosenstock-Huessy en Bas Leenman:
* Charles Taylor, Een Seculiere Tijd, 674-702.
* Eugen Rosenstock-Huessy, Het Christelijk levensgeheim, [32-43: over voorstad en fabriek]({{ 'assets/downloads/Christelijk-levensgeheim-34-43.pdf' | relative_url }}).
* Bas Leenman, Als God Stukloopt, [154-164: het kleine formaat van de moderne mens](({{ 'assets/downloads/Leenman-154-164.pdf' | relative_url }})).

[uitnodiging voorjaarsbijeenkomst]({{ 'assets/downloads/2020_uitnodiging_voorjaarsbijeenkomst.pdf' | relative_url }})
