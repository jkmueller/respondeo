---
title: "Interim 4 – oktober 2014"
thema: Hart en Ziel
category: interim
created: 2014-10
order: 1
---

### {{ page.thema }}

* Introductie – Wilmy Verhage
* Dienst van de Broeders van de Heilige Georg – Eugen Rosenstock-Huessy, vertaling Feico Houweling
* Terug naar de volheid van spreken – Feico Houweling
* De vier gestalten van de ziel – Jan Kroesen
* Eugen Rosenstock-Huessy THEN & NOW – Lise van der Molen
* Rosenstock-Huessy’s leven op een rijtje – Feico Houweling
* De Taal van de Ziel – Otto Kroesen
* Vereniging Respondeo  
* Over Interim


[Interim 4-2014]({{ 'assets/downloads/interim_4_2014.pdf' | relative_url }})
