---
title: Bijeenkomst 30 september 2023
thema: "Geestelijke heling van een verdeeld Europa"
category: bijeenkomst
created: 2023-06-25
published: 2023-09-03
order: 1
landingpage:
order-lp: 
---

![Dominicus Kerk Amsterdam]({{ 'assets/images/dominicus-kerk-amsterdam.jpg' | relative_url }}){:.img-right.img-small}
De vereniging Respondeo en de Girardkring nodigen je van harte uit voor een bijeenkomst op 30 september 2023 in de Dominicuskerk Spuistraat 12, 1012 TS Amsterdam. Het thema is:

### {{ page.thema }}
<!--more-->

Katya Tolstoj doet onderzoek naar post traumatische samenlevingen als professor aan de VU, is van Russische afkomst en spreekt over de ervaring van depersonalisatie in oorlogsomstandigheden en concentratiekampen. Telkens blijkt het ongelooflijke feit dat mensen zozeer ontdaan kunnen worden van de persoon die ze zijn, dat ze van hun menselijkheid zelve beroofd zijn. Wat betekent dat? Op de achtergrond van de lezing staat het conflict tussen Oost en West. Oekraïne is het slachtoffer geworden van tegengestelde opvattingen van menselijk samenleven en de ontsporing van de communicatie daartussen. Hoe komen wij verder? Dat vraagt erkenning van de verschillen maar ook van het zoeken naar een weg vooruit, naar heling. Door verbindende taal (Rosenstock), nederige spiritualiteit (Tostoy), geweldloze verzoening (Girard).

Het programma:

|  9.30 | Ontvangst en koffie
| 10.00 | Woord van welkom – dagvoorzitter(s)
| 10.15 | Conflict en de-humanisering – Katya Tolstoj
| 11.00 | Pauze
| 11.15 | Vragen en gesprek
| 11.45 | Einde ochtendprogramma
| 12.00 | Lunch
| 12.30 | In de kerk: orgelspel van Nico Brouwer
| 13.00 | Humanisering in Oost en West – Otto Kroesen (ethiek en ondernemerschap)
|| (inleiding en gesprek)
| 14.00 | Girard voor een gelovige trialoog – Wiel Eggen (antropoloog en theoloog)
|| (inleiding en gesprek)
| 15.00 | Borrel
| 16.00 | Sluiting

Dagvoorzitter is Jan Kroesen, \
voorzitter Respondeo

- Dominicuskerk, Spuistraat 12 te Amsterdam (5 minuten lopen vanaf het stationsplein Amsterdam CS).
- Voor koffie en thee wordt zorg gedragen. Lunch alsjeblieft zelf meenemen!!!!!!!!
- Graag vooraf opgeven bij Otto Kroesen, secretaris Respondeo. Otto Kroesen, Leeuweriklaan 3, 2623 RB, Delft, tel. 0653796968.
