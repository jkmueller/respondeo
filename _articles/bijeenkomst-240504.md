---
title: Bijeenkomst 4 mei 2024
thema: "De kerkelijke functie van Nietzsche"
category: bijeenkomst
created: 2023-12-02
published: 2024-03-18
order: 1
landingpage: 
order-lp: 1
---

![Ichtuskerk Colmschate]({{ 'assets/images/ichtuskerk_colmschate.jpeg' | relative_url }}){:.img-right.img-small}

De vereniging Respondeo nodigt je van harte uit voor een bijeenkomst op 4 mei 2024 de Ichthuskerk, Holterweg 106, 7429 AH Colmschate-Schalkhaar. Thema:

### {{ page.thema }}
<!--more-->
Hoewel door velen van zijn tijdgenoten onbegrepen wordt Nietzsche gezien als een scharnierpunt voor Europa. Je kunt bij hem spreken van een Christendom voor hem en een Christendom na hem. Als geen ander heeft hij het Christendom stuk gehamerd en gehekeld. Bekend is hij van de uitspraak ‘God is dood’. Wat hij stuk hamerde was niet alleen het Christendom, maar alle manieren van denken die ervan uitgingen dat er een wereld bestaat ‘achter onze wereld’. Of dat nu Plato, Hegel, het socialisme of de wetenschap betrof, de hemel, de hel of een onpersoonlijke dans van deeltjes (Fritjof Capra) achter de waarneembare wereld, geen van alle ontkwam of ontkomt aan zijn vaak felle kritiek.

Zo heeft hij bijgedragen aan het tijdperk van de seculariteit waarin we nu leven. Waar hij alle nadruk op legt is dit, dat de mens de aarde toebehoort en dat elk leefwijze die dat niet wil accepteren afgewezen moet worden.

Met het loskoppelen van deze wereld van zijn zon, lijkt het wel alsof deze vernietiger van alle waarden vooruit heeft gezien/gevoeld dat zich catastrofes zouden aandienen (in casu twee wereldoorlogen) en bijgedragen heeft aan het klimaat waarin een enorme diversiteit van opvattingen, tegenstellingen, actiegroepen en mensbeelden kunnen gedijen. En natuurlijk aan de secularisatie en de leegloop van de Kerken. Polarisatie, strijd is alom om ons heen, maar ook grote maatschappelijke betrokkenheid van personen en groepen.

Gerrit Komrij heeft eens gezegd dat wie vandaag de dag iets nieuws wil zeggen, zich eerst met Nietzsche moet uiteenzetten. Wanneer er iemand is, die nog voor Komrij al voldaan heeft aan deze eis dan is het wel Rosenstock-Huessy. Hij heeft grote waardering voor Nietzsche en zijn denken. In plaats van zich af te zetten tegen Nietzsche, zoals veel christenen hebben gedaan. Vanwaar die waardering? Wat is de betekenis die Nietzsche voor Rosenstock heeft.

Welke rol speelt Nietzsche in zijn denken? Nietzsche speelt misschien daarom zo’n prominente rol in het denken en schrijven van Rosenstock-Huessy, omdat hij ‘ontijdig’ was als Rosenstock-Huessy zelf. En tegelijk was Nietzsche de proclamator van het einde van het
Christendom en dat betekent nogal wat. Rosenstock met z’n neus voor wat echt crisis is, weet echter dat waar een deur dicht gaat er een andere opengaat. Het is dan overigens nog maar de vraag wat erdoor naar binnen komt.

Wanneer Eugen Rosenstock-Huessy ondanks alle tegenzin Nietzsche leest, leest hij tevens wat hij geleerd heeft van de crises van Europa. Bovendien leest hij dat een mens aan zichzelf overgelaten, opgesloten in zijn eigen tijd, ten ondergaat aan zijn eigen verscheurdheid. Het is de natuur van de mens dat hij geen natuur heeft. Van zichzelf heeft de mens niets om op terug te komen. Aan deze afgrondelijkheid is hij overgeleverd wanneer hem alles wordt
ontnomen. Bij beide is dat aan de orde. Bij de één in zijn mentale denkarbeid, bij de ander in het fysiek meemaken en ondergaan ervan.

Nietzsche heeft als geen ander de grote catastrofe voorvoeld van de twee wereldoorlogen. Eugen Rosenstock heeft ze in het vlees geleefd en moet zich afgevraagd hebben ‘hoe kan het wat worden met ons mensen’. Als het wat wordt met een mens heeft hij alleen wat hij geërfd heeft en alleen daarmee kan hij de toekomst tegemoet treden en de wereld tot een betere plaats maken dan hij is. Kierkegaard noemt dat nog voor Nietzsche ‘plaats maken opdat God kan komen’. Pas dan kun je iemand worden.

We komen dan ook alle bewegingen tegen die we van Eugen Rosenstock kennen. Het kruis der werkelijkheid, waaraan ook Nietzsche was overgeleverd, ons binnen met onze fantasieën, onze liefdes, onze grootheidswensen, onze angsten en verlangens, ons buiten zoals we gezien worden en gezien willen worden, onze gebondenheid aan de tijd waarin we leven en de plicht, de oproep om in meerdere tijden (polychroon) te leven. Het is fascinerend om dat
rond de persoon van Nietzsche te lezen en een Nietzsche tegen te komen die, net als wij, iets moest doen met de erfenis van het christendom, ertegen in het geweer moest komen en er lessen uit moest trekken met betrekking tot de toekomende tijd. Nietzsche maakt deel uit van onze autobiografie.

Het is niet doenlijk om het gehele vertaalde stuk in anderhalf à twee uur te behandelen. Daarvoor is de materie te complex, laat staan dat er dan ook nog zinvol over van gedachten kan worden gewisseld. Natuurlijk kan iedereen het hele stuk lezen (er staat ontzettend veel in), maar tijdens onze bijeenkomst lezen we in ieder geval de volgende pagina’s.
- Bl. 10 t/m 13
- Bl. 23 t/m 26

Met dit al wens ik ons met elkaar een goede en mooie bijeenkomst toe.
>*Jan Kroesen*

De tekst van Rosenstock-Huessy over Nietzsche is in het Nederlands vertaald en te vinden op de website: [De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ '/text-erh-functie-van-friedrich-nietzsche-in-de-kerk' | relative_url }})

In de middag is de ledenvergadering

Het programma:
- 10.30 Ontvangst en koffie
- 11.00 Opening, samen lezen onder leiding van Jan Kroesen
- 12.30 Lunch
- 14:30 Ledenvergadering
  1. Opening
  2. Jaarverslag (2024), zie nieuwsbrief)
  3. Financieel jaarverslag (idem)
  4. Vooruitblik naar de najaarsvergadering
  5. Rondvraag
  6. Sluiting
- 15.00 Koffie en thee
- 15.30 Sluiting

Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo.
Otto Kroesen, Leeuweriklaan 3, 2623 RB, Delft, [email](mailto:info@rosenstock-huessy.com)


De Ichthuskerk is met bus bereikbaar vanuit Deventer, maar een van de bestuursleden kan je ook altijd even bij het station in Deventer ophalen, als je contact opneemt.

zie ook: [Rosenstock en Nietzsche: Aantekeningen van Rudolf Kooiman]({{ '/rk-aantekeningen-rosenstock-en-nietzsche' | relative_url }})
