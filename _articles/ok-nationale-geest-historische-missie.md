---
title: "Otto Kroesen: Over nationale geest en historische missie"
category: essay
essay-nr:
created: 2024-01-20
published: 2024-01-20
landingpage: front
order-lp: 15
---
Naar aanleiding van Ter Apel als Nederlands nationale schande\
Delft, januari 2024

<!--more-->
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}
De hieronder [vertaalde tekst]({{ 'text-erh-amerika-van-europa-en-europa-van-amerika-1963' | relative_url }}) van Rosenstock-Huessy kan ons bevrijden van een eng Nationalistische Geest. Bij een natie denke men niet aan een gebied dat in het bezit is van een groep mensen, die daardoor een gemeenschappelijke geschiedenis heeft. Het is juist andersom: mensen die een gemeenschappelijke geschiedenis, want een gemeenschappelijke inspiratie hebben, hebben zich dit stuk land verworven om die inspiratie gestalte te geven. De geest van de natie is dus niet aan het land gebonden, maar het land aan de Geest.

Ons Nederland is daar een goed voorbeeld van. De Nederlandse revolutie, de opstand tegen Spanje en tegen Philips II is begonnen in Vlaanderen. Maar in Vlaanderen heeft deze nieuwe inspiratie zich niet kunnen doorzetten, want de opstand werd neergeslagen door Alva. In het onbeheersbare watergebied in Noord Nederland heeft de revolutie zich eindelijk ruimte kunnen verschaffen.

Mensen die de moed verloren hebben omdat ze kleine zielen zijn geworden draaien de volgorde om en roepen: dit is ons land! Wij Nederlanders dachten dat deze populistische en Nationalistische tendens misschien elders, maar niet bij ons thuis de overhand kon krijgen.

Velen zoeken het antwoord op de overwinning van Wilders in de sociale omstandigheden van de mensen en de maatschappelijke onvrede. Maar die legt alleen maar een dieper en ouder probleem bloot. Wie de Geest niet heeft komt geesteloos te leven en wie geesteloos leeft wordt benauwd en heeft zondebokken nodig.

Het gaat helemaal niet om nationalisme. Het is primitiever. Dat het niet om nationalisme gaat is bewezen door de spreidingswet. Het is alleen het bezonnen deel van de VVD geweest dat met de wet die de vluchtelingen over meerdere steden moet spreiden heeft ingestemd. Maar de zogenaamde “eigen land eerst” voorstanders hadden graag Ter Apel en dus Groningen opnieuw met de lasten willen laten zitten. Wie zou dat nationalisme durven noemen?

De Nederlandse natie is begonnen met de Unie van Utrecht in 1579. Daar spraken de provinciën met elkaar af dat zij niet afzonderlijk, niet zonder elkaar met Spanje vrede zouden sluiten. Die belofte heeft het gehouden. Voor die tijd had men niet iets met elkaar. Nu nam men wederzijdse verantwoordelijkheid, al bracht dat bijvoorbeeld in Haarlem en Leiden veel leed. Dat is solidariteit. Dat is Geest.

[Het Amerika van Europa en het Europa van Amerika (1963)]({{ 'text-erh-amerika-van-europa-en-europa-van-amerika-1963' | relative_url }})\
*vertaald door Otto Kroesen.*
