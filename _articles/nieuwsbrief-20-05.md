---
title: Nieuwsbrief mei 2020
category: nieuwsbrief
created: 2020-05
order: 1
---
Beste mensen,

Het is jammer dat wij op 9 mei niet gewoon bij mekaar konden komen om elkaar te ontmoeten. We zouden elkaar namelijk veel te vertellen hebben gehad, omdat de impact van het coronavirus op ieder van ons groot is. Op z’n minst op de gewone dingen van alledag. Daarbij komt dat die impact ook nog eens dicht aanligt tegen een belangrijk Rosenstock-Huessy thema. Het laat namelijk (weer) zien hoezeer wij wereldwijd met elkaar verbonden, zo niet opgescheept zitten. Dat is immers het thema van zijn Sociologie: de vraag hoe wij, die als het ware met elkaar in een te kleine ruimte zitten, als buren kunnen samenleven. Wij moeten het tenslotte met elkaar zien te rooien en dat lukt alleen maar als wij dragers zijn van de verworvenheden van de langdurige golven van de tijd. Ja, wij delen wereldwijd de gevolgen van hetzelfde virus, en juist die nabijheid trekt vaak kleine gemeenschappen uit elkaar. Gemeenschappen, die nu afstand moeten houden, zoals ook wij als vereniging Respondeo. Met dit rondschrijven willen we dat een beetje goedmaken.

De volgende onderwerpen komen aan de orde:
1. Terugblik op de najaarsbijeenkomst
2. Digitale communicatie, website, podcast
3. Discussie over Charles Taylor en Rosenstock-Huessy
We vragen de leden aan die discussie deel te nemen via de WhatsAppfaciliteit. Daarover later meer.

Op de valreep berichtte Agnes Lieverse dat Eckart Wilkens (bestuurslid van onze zustervereniging in Duitsland) zeer ernstig ziek is:
Eckart was al enige tijd ziek maar ligt sinds zondag 17-05- 2020 in het ziekenhuis.

*Zijn vrouw, Sigrid vraagt of  Harrie en ik  de informatie over Eckart  willen doorgeven.*\
*Zij  houdt ons* [Agnes en Harrie] *op de hoogte en Sigrid vraagt of men  niet naar hun thuis in Keulen belt maar naar Harrie en mij.*

[in verband met de privacy-wetgeving: bel of mail de secretaris van Respondeo als je Harrie of Agnes wilt spreken en hun contactgegevens niet hebt].

[nieuwsbrief 20-05]({{ 'assets/downloads/2020_mei_Digitale_brief_Respondeo.pdf' | relative_url }})

--
met vriendelijke groet,

Egbert Schroten, secretaris Vereniging Respondeo
