---
title: Bijeenkomst 7 mei 2022
thema: "De tweede Jesaja"
category: bijeenkomst
created: 2022-04-09
published: 2022-04-09
order: 1
landingpage:
order-lp:
---

![Irene Twello]({{ 'assets/images/irene-twello.jpg' | relative_url }}){:.img-right.img-small}

De vereniging Respondeo nodigt je van harte uit voor een bijeenkomst op 7 mei 2022 in gebouw “Irene”, Dorpsstraat 8, Twello, met als thema:

### {{ page.thema }}
<!--more-->
Hoe worden tijden aan elkaar verbonden? Niet door een rechte lijn! De rechte lijn doortrekken leidt vaak tot mislukking en ondergang. Pas in tijden van crisis wordt ook een nieuw begin mogelijk, opstanding uit de dood. Dat is de christelijke boodschap, maar die zit, volgens Bas Leenman al verpakt in het oude testament. Hij heeft een opstel geschreven over de Eerste en de Tweede Jesaja. Er zit 100 jaar tussen. Jesaja is waarschijnlijk om het leven gebracht, maar 100 jaar later staat een tweede Jesaja op, een profeet die tijdens de ballingschap spreekt over een nieuw begin. Deze profeet pakt de beloften en de stijl en de spreekwijze van de eerste Jesaja weer op.
Op 7 mei lezen en bespreken we als Respondeo met belangstellenden de tekst van Bas Leenman in zijn boek “Als God stukloopt” 228-238.

In de middag is de ledenvergadering.

Het programma:

| 10.30 | Ontvangst en koffie
| 11.00 | Opening, samen lezen onder leiding van Rudolf Kooiman
| 12.30 | Lunch
| 13:30 | Ledenvergadering
|| 1. Opening
|| 2. Jaarverslagen (2020 en 2021, bijgevoegd)
|| 3. Financiële jaarverslagen (idem, bijgevoegd)
|| 4. Bestuursverkiezing (Egbert en Rudolf zijn aftredend)
|| 5. Koers Respondeo
|| 6. Rondvraag
|| 7. Sluiting
| 15.00 | Koffie en thee
| 15.30 | Sluiting

De tekst is op te vragen bij Otto Kroesen. Maar beter bestel je het hele boek bij Skandalon (website of via de boekhandel). Ter vergadering zullen ook enkele exemplaren ter verkoop aanwezig zijn.

Voor de kosten van lunch en zaalhuur wordt een vrijwillige bijdrage gevraagd (richtbedrag 15 euro). Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo:\
Egbert Schroten, Veeningen 24, 7924 PJ Veeningen, tel. 0528-202041.
