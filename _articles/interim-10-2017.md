---
title: "Interim 10 – najaar 2017"
thema: "Als Hitler valt"
category: interim
created: 2017-10
order: 1
---

### {{ page.thema }}


* Inleiding – Jan Kroesen/Wilmy Verhage
* Hoe kwam ik ertoe een boek over een Duitse verzetsgroep te schrijven? – Marlouk Alders
* Nieuw boek over Duits verzet – Feico Houweling
* Het was een leuk feest – Jan Kroesen
* Bij de boekpresentatie van ‘Als Hitler valt’ – Jan Kroesen
* ‘Als Hitler valt’, een mooi, compact inhoudrijk boekje – Henk van Olst
* Een gedicht van Eugen Rosenstock Huessy – vertaling Wilmy Verhage
* Bericht uit het bestuur – Otto Kroesen
* Achterpagina

[Interim 10-2017]({{ 'assets/downloads/interim_10_2017.pdf' | relative_url }})
