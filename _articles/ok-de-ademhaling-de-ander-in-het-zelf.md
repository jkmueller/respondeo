---
title: "Otto Kroesen: De ademhaling: de ander in het zelf"
category: reactie
essay-nr: 160
created: 2022-11-05
published: 2022-11-05
---

Als Johan Keij spreekt over de ander die mij onderbreekt en interrumpeert en zo over het feit dat ik geen volledige controle uitoefen, schiet hem kennelijk het woord “spontaan” te binnen. Het woord “spontaan” is hier volgens mij vooral van Keij en niet zozeer van Levinas. Misschien bedoelt Keij met dat woord vooral te benadrukken dat wij ons antwoord (beter dan reactie) nooit volledig onder controle hebben. Je staat soms te kijken van je eigen woorden.

Op elk moment dat wij onder controle zijn van onszelf, zijn wij ook niet in controle en blootgesteld aan de ander. Elk initiatief is een antwoord en wordt geboren in verantwoordelijkheid. Juist daar waar ik ik ben, ben ik ook onder de beademing van de ander. De Ander inspireert en verlevendigd het Zelf. Vaak schrijft Levinas daarom die twee termen, Ander en Zelf met een hoofdletter. Het is inderdaad vergelijkbaar met de ademhaling: de lucht die ik inadem en de lucht die ik uitadem, ben ik dat zelf – is dat mijn adem?

Op elk moment is dat zo. Dat het “nu” vervliegt weerspiegelt de tijdsanalyse van Augustinus: het nu dat scherp is en dun als een scheerblad, want het is een punt tussen verleden en het volgende moment van de toekomst. Het is eigenlijk niets.

Rosenstock-Huessy heeft een andere opvatting van het heden. Niet dat hij deze analyse ontkent, maar hij wijst er op dat het “heden” in onze omgang daarmee een periode beslaat die een zekere eenheid vormt. Die eenheid komt tot stand door de taal die onderling verstaan creëert en daarmee een gemeenschappelijk heden. Mensen die elkaar verstaan zijn aan elkaar gelijktijdig – ze leven in hetzelfde heden. Dat is niet het messcherpe heden van Augustinus.

Levinas geeft er geen aandacht aan hoe verschillende mensen gelijktijdig worden. Hij benadrukt alleen een zekere “diachronie” op het moment zelf. Als ik terugkijk op de zin die ik zojuist uitgesproken heb, meen ik dat ik zelf aan het woord was. Op het moment zelf, voordat ik terugblik, ben ik mijzelf echter als het ware een stap voor: ik praat al voordat ik bedenk wat ik zeg. Pas als ik terugblik op wat ik zojuist zei, ben ik weer bij mijzelf, in de reflectie. Elk moment is niet alleen een kwestie van zelfbeschikking, maar is ook beademd door en geïnspireerd door de ander in mij.

Het is jammer dat Levinas zich nooit in Rosenstock-Huessy verdiept heeft, terwijl hij wel een goede lezer is van Rosenzweig. Rosenstock-Huessy figureert in de opstellen die Levinas over Rosenzweig geschreven heeft alleen als de vriend van Rosenzweig die christen werd. Vanaf dat moment vindt men hem niet meer interessant want men is juist geïnteresseerd in het Jodendom. Zo hebben verschillende artikelen over Rosenzweig hem neergezet en dat reproduceert Levinas eenvoudig.

Rosenstock-Huessy heeft wel van de Ster der Verlossing van Rosenzweig gezegd dat die weliswaar de bedoeling heeft het Jodendom toegankelijk te maken, maar dat het in zijn werking omgekeerd is: het maakt voor joden het christendom toegankelijk. Rosenzweig komt tot het Jodendom via het christendom. Zijn joods zijn is een bijdrage aan de door het christendom gestichte universaliteit en inclusiviteit. Zoiets zou je volgens mij ook van Levinas kunnen zeggen. Instaan voor de ander is voor Levinas kern van de humaniteit en tegelijkertijd kern van het Jodendom. Maar instaan voor de ander, plaatsbekleding, is ook de meest christelijke notie die men kan bedenken. Joodse wet en christelijk geloof zijn nu niet meer exclusief voor elkaar, bij alle verschillen in uitleg van hun wederzijdse geschriften. Zou deze meerstemmigheid in combinatie met gemeenschappelijke verantwoordelijkheid een voorbeeld kunnen zijn voor de interreligieuze dialoog?
