---
title: "Rudolf Kooiman: Naar aanleiding van het boek Polarisatie van Bart Brandsma"
category: essay
essay-nr: 140
landingpage:
order-lp:
created: 2021-09-19
published: 2021-09-19
---
![Rudolf Kooiman]({{ 'assets/images/rudolf-kooiman.jpg' | relative_url }}){:.img-right.img-small}
In het boek Polarisatie verkent Bart Brandsma de dynamiek van het wij-zij denken en pleit hij voor een nieuwe invalshoek. Het zou al helpen, zegt hij, als we het fenomeen niet alleen als een probleem zien.
<!--more-->
In onze maatschappij staan veel mensen tegenover elkaar. Vrouwen tegenover mannen, moslims versus niet-moslims, burgers tegenover de politiek, voor of tegen zwarte Piet, voor of tegen de avondklok, klimaatontkenner of juist klimaatfanaat, die van beneden de rivieren tegenover die van ‘erboven, de autochtoon versus de allochtoon, Belgen versus Hollanders. 010 tegenover 020, Koerden tegenover de Turken, het volk tegenover de elite. De lijst van polarisatie is oneindig. Maar de dynamiek is overal dezelfde. Die is universeel.
a. Op zichzelf neutrale tegenstellingen worden geladen met betekenis en dat zijn altijd gedachten-constructies (Belgen zijn dom, Moslims gevaarlijk, vrouwen zijn het zwakke geslacht)
b. Tegenstellingen blijven levend door het leveren van brandstof: voor elke negatieve uitspraak is een positieve tegenhanger te formuleren, passend bij hoe we mogelijk ook over de ander kunnen denken.
c. Bij toenemende polarisatie neemt de gespreksstof (discussie en debat) toe terwijl de redelijkheid afneemt. Gevoelens hebben de overhand op feiten. Zie: de complottheorieën.

Mensen vinden het in een spanningsveld moeilijk uithouden in het midden. Ze willen liever of het een of het ander. Vrijwel altijd is de kunst: allebei. En vrijwel altijd is het ook lastig om beide polen te laten meewegen. Zwart-wit denken is eenvoudiger. Activisten die doorschieten: natuurliefhebbers die Shell verketteren laten hetzelfde gedrag zien als een Shell dat de groene activisten verkettert.

Brandsma kies voor een depolariserende houding. Dat is niet hetzelfde als de grootste gemene deler. Het is niet een idealistisch etiket om problemen met verschil, conflict en polarisatie te camoufleren. Het is ook geen pleidooi voor de objectieve toeschouwer: de bruggenbouwer die boven de partijen staat en zelf, ongewild, brandstof blijft aanleveren voor de tegenstellingen.
Het gaat om een houding het uithouden in het midden, omgaan met niet-weten in plaats van oordelen. Het gaat om het juiste oog voor diversiteit

Dat is niet eenvoudig. Want dan moet je leren ruimte te maken en daarbij ook het onbekende toelaten. Je gaat in het onbekende staan en moet heel goed luisteren. Daar heb je uithoudingsvermogen, inspiratie en geduld voor nodig. Maar het is de moeite waard want de waardering van het verschillend zijn is een tegengif voor eenzijdigheid. En dat lijkt hard nodig in een wereld waar eenzijdigheid overal aanwezig is. In institutionele religies, in positivistische wetenschap en in absoluut gestelde overtuigingen.

De grammaticale methode van Rosenstock-Huessy kan niet los gezien worden van de situatie in het Duitsland van na de eerste wereldoorlog. De visie van Brandsma kan mijns inziens niet los gezien worden van de versplinterde, gepolariseerde wereld van de 21e eeuw, waar identiteitspolitiek zorgt voor scheiding en verdeeldheid.

Beiden bepleiten een houding van zelfstandigheid en verantwoordelijkheid.
Rosenstock gaf zijn droom vorm door de werkkampen die hij organiseerde.
Daardoor kwam met in contact met andere werelden en kon er geoefend worden. De vraag aan Brandsma en aan ons: Hoe oefenen wij een andere houding, een andere benadering. Wat zijn onze proefpolders? Hoe kunnen wij, in onze tijd, recht doen aan het probleem van het wij-zij denken (denk aan de debatten over identiteit) en bijdragen aan een ‘oplossing?’
