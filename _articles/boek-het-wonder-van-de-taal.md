---
title: "Eugen Rosenstock-Huessy: Het wonder van de taal"
category: boek
order: 1
---
![Het wonder van de taal]({{ 'assets/images/Het-wonder-van-de-taal.jpg' | relative_url }}){:.img-left.img-large}


Te bestellen bij de uitgever
 [Skandalon](http://www.skandalon.nl/shop/theologie-cultuur/371-de-taal-van-de-ziel.html). De vereniging Respondeo heeft de uitgave financieel mogelijk gemaakt.
