---
title: Bijeenkomst 6 mei 2017
thema: "Verzet, verzoening, samenwerking – presentatie: Als Hitler valt"
category: bijeenkomst
created: 2017-05-06
order: 1
---
### {{ page.thema }}

Beste vrienden,

Hierbij een bijgewerkte opzet voor een draaiboek voor 6 mei. Er zitten nog steeds wat vraagtekens in. De tijden zijn bij benadering. Meestal loopt het wat uit.

Vooraf:\
Inkopen voor zover nodig (Greet?)\
Bloemetje voor muzikanten en voor Lien (Wie?)

| 11.00 |    Algemene Ledenvergadering Respondeo o.l.v. bestuur
||Greet neemt de leiding over alles wat met eten en drinken voor 12:30 uur gebeurt
||Ze heeft hier hulp bij (van Aart, Egbert, meestal zijn er deelnemers die niet stil willen zitten)
||Egbert zal met haar afstemmen hoe de boodschappen op tijd zijn
||Egbert gaat ervan uit dat de koffiezetter en waterkoker in het RHH beschikbaar zijn
| 12.00 |     Lunch, organisatie: Respondeo
| 12.30 |    Zaal open (Greet, Egbert:  koffie, thee – die hebben niet veel tijd om te lunchen)
| 13.00 |    Intro: kort muziekstukje (Maria Möckel en Laurens Moreno)
|| Welkom (bestuur)
| 13.15 |    Het verzet van de Kreisauer Kreis, Rosenstock-Huessy etc. (Feico)
| 13.30 |    Vrijwilligerswerk nu: Aktion Sühnezeichen Friedensdienste (Dennis Fink)
| 13.40 |    Muzikaal intermezzo (Maria Möckel en Laurens Moreno)
| 13.50 |    De wording van het boek (Marlouk, kort)
| 14.00 |    Overhandiging van het eerste exemplaar aan Lien Leenman (Marlouk)
| 14.10 |    Muzikaal intermezzo (Maria Möckel en Laurens Moreno.)
| 14.20 |    Kort gesprek met de zaal (vragen) over het boek en nog e.e.a. over Kreisau vandaag (Feico)
||       Na afloop bloemen voor musici en Lien (bestuur?)
| 14.30 |     Ontmoeting en gesprek onder een hapje en een drankje (Kristien) – Maria en Laurens zijn ook dan bereid om iets te spelen, maar we moeten elkaar nog wel kunnen verstaan. Wellicht ook een cd-tje draaien?

Boekenverkoop: Feico (alleen contant, geen pin)

Richt Respondeo nog een boekentafel in? De voorraad staat pal achter de kapel. (Die verkoop los van Marlouks boek).

Graag verdere aanvullingen en opmerkingen.

Groet,

Feico

[uitnodiging 6 mei 2017]({{ 'assets/downloads/2017_Respondeo_6mei.pdf' | relative_url }})
