---
title: "Otto Kroesen: Identiteit, zeggenschap en zeggingskracht 1"
category: essay
essay-nr: 100
created: 2021-08-30
published: 2021-08-30
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}
### Deel 1: Strijd over identiteit

Zo’n 40 jaar geleden was het ongebruikelijk om trots te uiten over de identiteit als Nederlander. Nationaliteit, wat betekende dat nog? Wij moesten groter denken en verder kijken. Een Nederlands paspoort? – dat was alleen handig. Hoe heeft het debat over identiteit, nationaliteit, racisme en uitsluiting zo’n vlucht kunnen nemen?
<!--more-->
Voor een deel is het een reactie op het neoliberalisme en voor een deel en reactie van verschillende groepen op elkaar. Op het neoliberalisme kom ik nog, maar eerst het laatste: Als Nederlanders zich laten voorstaan op hun nationale identiteit, in ultrarechtse politieke partijen, dan is dat juist voor anderen aanleiding om op de schaduwkanten van dat Nederlanderschap te wijzen. De dag dat ik dit schrijf is Keti Koti, de herdenking van het slavernijverleden. Pas heeft de Vlaamse schrijver David van Reybrouck een dik boek gepubliceerd over het koloniale verleden van Nederland in Indonesië, onder de titel Revolusi. Eerder heeft hij een boek over Belgisch Congo geschreven. Je staat versteld van het geweld waarvan de Nederlandse koloniale geschiedenis vol is. Als jij denkt dat je dan trots kunt zijn op de Nederlandse identiteit, weten anderen je wel op je nummer te zetten.

Maar het debat over identiteit is ook deel van een grotere beweging: de boeren vragen om erkenning en de klimaatactivisten, de bankiers en het schoolpersoneel en de mensen die voor of tegen zwarte Piet zijn – ze hebben een boodschap te brengen, maar ze hebben ook behoefte aan erkenning. Zij willen delen in een gemeenschappelijk besef, een wij-gevoel. Daar kun je je aan optrekken.

Identiteit houdt in dat je je vereenzelvigt met een collectief, een groep. Wij hebben veel overlappende identiteiten. Dat is de boodschap van Sinan Çankaya in zijn boek Mijn Ontelbare Identiteiten. Hij is voluit Nederlander, en met een Marokkaanse achtergrond. Bart Brandsma, in zijn boek Polarisatie, wijst op het wij-zij denken en de polarisatie en vijandigheid die er het gevolg van zijn. In lijn daarmee kan er nog een Duitse auteur genoemd worden, Hamed Abdel-Samad, die in zijn boek Schlacht der Identitäten ongeveer dezelfde boodschap heeft als Brandsma. Wij moeten leren ons in elkaar te verplaatsen. Hoe heeft het gevoel van identiteit zo belangrijk kunnen worden? Een belangrijke reden is de ontworteling die het neo-liberalisme teweeg gebracht heeft. Wij zijn nummers geworden in reusachtige organisaties. De zeggenschap die mensen hebben over het werk dat ze doen en ook de zeggenschap in het maatschappelijk verkeer is daardoor verminderd. Als gevolg daarvan is ook onze zeggingskracht achteruit gehold. Wat is zeggingskracht? Het is het vermogen om een eigen oordeel onder woorden te brengen in een gezonde wisselwerking met het evenzeer onafhankelijke oordeel van anderen. In een volwassen dialoog corrigeer je elkaar en vul je elkaar aan. Maar wij zijn geestelijk verzwakt. We hebben er meer dan vroeger behoefte aan ergens bij te horen en met een groep mensen aan de goede kant te staan. Dat is een gevolg van onze ontworteling, het functioneren als een radertje in de machine.
