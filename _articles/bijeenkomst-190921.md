---
title: Bijeenkomst 21 september 2019
thema: "Over westerse waarden in radicale tijden"
category: bijeenkomst
created: 2019-09-21
order: 1
---
### {{ page.thema }}

Uitingen van radicalisering brengen morele verlegenheid aan het licht.

zaterdag 21 september 2019 in de Dominicuskerk in Amsterdam: Studiedag door De Girardkring en de vereniging Respondeo.

***Over westerse waarden in radicale tijden:***

*Uitingen van radicalisering brengen morele verlegenheid aan het licht.*

De dag wordt ingeleid door Hans Boutellier – wetenschappelijk directeur van het Verweij-Jonkerinstituut.

In onze turbulente tijd lijken veel vanzelfsprekende uitgangspunten van de democratische rechtsorde onder druk te staan. We zien opkomend nationalisme en groeiend verzet tegen migratie en tegen de elite, terwijl de islam een steeds grotere rol inneemt in het publieke domein. Hans Boutellier publiceerde onlangs een herziene versie van zijn boek Het Seculiere Experiment. Hierin analyseert hij de consequenties van de seculiere conditie. Na de ontzuiling ontwikkelde zich een door en door pragmatische samenleving, maar deze lijkt geen antwoord te kunnen bieden op de zorgen van mensen. Hij ziet zowel morele verlegenheid als toenemende radicalisering van verschillende zijden. Boutellier maakt in zijn inleiding de balans op van de seculiere conditie en gaat graag met u in gesprek over perspectieven voor de toekomst.

De bijeenkomst is een initiatief van twee werkgezelschappen, die respectievelijk het werk van René Girard en Rosenstock-Huessy onder de aandacht brengen. René Girard heeft gepubliceerd over het zondebokmechanisme. Rosenstock-Huessy heeft gepubliceerd over de noodzaak om de saamhorigheid van oude stammen te doen herleven maar niet hun onderlinge gewelddadigheid.

Programma:

| 10.00 | Inloop
| 10.30 | Opening en lezing Hans Boutellier
| 11.45 | discussie
| 12.30 | lunch
| 13.30 | Daan Savert geeft een reactie vanuit Girardkring met aansluitend discussie
| 14.30 | pauze
| 14.45 | Jan Kroesen geeft een reactie vanuit Respondeo met aansluitend discussie
| 15.45 | afsluiting, borrel

[als PDF]({{ 'assets/downloads/2019 najaarsbijeenkomst_respondeo_girard.pdf' | relative_url }})
