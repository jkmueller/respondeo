---
title: "Rudolf Kooiman: Waarom Nietzsche maskers moest dragen"
category: reactie
essay-nr: 180
created: 2024-11-16
published: 2024-12-08
landingpage: front
order-lp: 4
---
### Rosenstock en Nietzsche

#### Aantekeningen bij het het gesprek over [“Waarom Nietzsche maskers moest dragen en waarom het Kruis der werkelijkheid geen model is.”]({{ '/bijeenkomst-241116' | relative_url }})
<!--more-->
![Rudolf Kooiman]({{ 'assets/images/rudolf-kooiman.jpg' | relative_url }}){:.img-right.img-small}

1. Bij Nietzsche hoort gekte, wildheid en waanzin. Hij leed aan het leed van de wereld. Hij zag dat leven lijden met zich meebrengt.
2. Nietzsche zag problemen van onze tijd (de tijd na hem) scherper dan  anderen en eerder. Hij voorzag het eind van de Christelijke cultuur.
3. Nietzsche is christelijker dan de christen die in systemen gelooft,
dat herkent Rosenstock.
4. De drie maskers van Nietzsche die hij ontleende aan de rollen die hij heeft gespeeld in zijn leven:
      * De professor: de man die college geeft in Bazel
      * De ‘nazi-Nietzsche’: de Nietzsche die alles kapot hamert: het socialisme, feminisme, christendom. Overal heeft hij kritiek op: alles kan afgeschaft. Hij wijst het allemaal af en daarin was hij anti-platonisch, want Plato zei: alles is in ideale vorm elders aanwezig in transcendentale vorm. Datgene wat hier is op aarde is een zwakke representatie. Nietzsche weer de wereld achter en boven onze wereld af en in die zin is God dood (dat is de betekenis van de uitspraak: God is dood).
      * Zarathustra: de mens in zijn derde levensfase: de wetgever en regelgever. De kinderen wordt geleerd hoe in het leven te staan, hoe de dingen ter hand te nemen.
5. Nietzsche opponeert tegen het optimisme en het vooruitgangsgeloof van zijn tijd (die overigens begrijpelijk was met de uitvinding van de elektriciteit, het snellere vervoer: de trein enz.
6. Nietzsche herhaalt op zijn manier Kierkegaards uitspraak: mensen verliezen hun ziel door de gedachte dat er geen smalle weg is, maar alleen een brede weg die ons naar het Heil leidt.
7. Nietzsche was kritisch op het Christendom, want, zei hij - met Kierkegaard – wat stelt de binnenkant van het Christendom voor?
8. De taal van Nietzsche tussen twee tijden: de tijd die achter hem ligt en de tijd die komt. Hij staat tussen de tijden in en is daarmee ongelijktijdig. N. gaat terug tot voor onze tijdsrekening.
9. Nietzsche bekritiseert de natuurwetenschap met haar modellen.
10. Nietzsche fulmineert tegen Paulus, omdat hij vindt dat die de mensen heeft misleid. Paulus zou de lichamelijkheid en de driften ontkennen.
11. Wij missen een andere oriëntatie en daar was Nietzsche naar op zoek

Rosenstock-Huessy voelt zich verwant met Nietzsche, maakte óók korte metten met theologen én filosofen die in systemen geloven en boven de werkelijkheid ‘hangen’. Voor Rosenstock-Huessy was het ook belangrijk om teksten te interpreteren en verbinden met het geleefde leven.  Tegelijk heeft hij duidelijk een eigen positie en onderscheidt hij zich van Nietzsche.
Onder andere vanuit het Kruis der werkelijkheid, een levenswerkelijkheid waar vier aspecten een plek krijgen: verleden en toekomst, binnen en buiten.

**Samenvatting van het artikel** \
[De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ '/text-erh-functie-van-friedrich-nietzsche-in-de-kerk' | relative_url }})

#### Hoofdstuk 5. De vergoddelijking van de mens in de kerk

Nietzsche bekritiseert Protestantisme en Romanisme, omdat beiden:

1. Beiden zijn sterk beïnvloed door Aristoteles en Plato oftewel Grieks theïsme
(waarbij het Platonisme God heeft gedegradeerd tot een idee en de Aristotelisten tot een eerste oorzaak). Nietzsche benoemde de dood van die God. Hiermee stelde hij zich buiten de Kerk en buiten het Christendom, maar – zo zegt R.H - het is beter om de letters GOD te doden dan de levende God. De God wiens naam volgens Exodus 3 JWHW is en dat betekent niet “Ik ben die ik ben” (dat is een filosofische formule), maar “Ik ben met jullie, Ik ben aanwezig”.\
Met andere woorden met zijn kritiek stond N. buiten de kerk en buiten het Christendom, maar niet buiten de mensheid.

2. Zowel Protestantisme als Romanisme hadden/hebben een dubbelzinnige houding ten opzichte van de werkelijkheid van Gods wereld én de wereld van ruimte en wetenschap: ze lieten deze wereld over aan de wetenschap en trokken zich terug in een andere wereld (een bovenwereld of een binnenwereld)

3. Beide lieten het geloof in de incarnatie varen en die geloofsregel betekent:  deze wereld is de enige, het koninkrijk van God is in onze harten, de hemel is open en niet hierna

Verder bekritiseerde Nietzsche het feit dat:
1. Op scholen en hogescholen Plato en Paulus als gelijkwaardig werden gezien
2. Socrates en Jezus op één lijn werden gezet

Hier tegenover stelde hij Dionysos als de gekruisigde. Waarbij het dionysische masker het tragische karakter van het leven toont. De bede “Ave Socrates, Ora pro nobis” werd vervangen door: “Enthousiasmeer ons, vervul ons, o Verlosser”.





#### Hoofdstuk 6. Godslastering en waanzin of theologie en filosofie wisselen van stemming

Nietzsche kan niet uit de voeten met de dominante theologie en filosofie. Voor hem heeft de theologie God gewurgd, de nek om gedraaid en de filosofie is gezwicht voor de wereld.

Theologie is meestal de wetenschap van de God van iemand anders. Het is filosofie, geschiedenis en godsdienstpsychologie ineen.  Theologie is verworden tot de logica van een of ander ‘objectief’ concept van God, zoals die wordt gevonden in de kerk, de Bijbel of één van de andere religies. Wat betekent dat God afwezig is en zijn alomtegenwoordigheid de laatste 1000 jaar zijn enige eigenschap is. Zo is God gaan horen tot de wereld van dingen waarover gediscussieerd kan worden, maar dat lijkt me (zegt R-H) godslastering en waanzin. Want de naam van God is een macht in ons persoonlijk overleven. Theologen horen tegenwoordig tot de Filosofie met dien verstande dat hun (wereldse) materiaal bestaat uit de Bijbel, de Geloofsbelijdenis, de kerk enz. van de z.g. Christenen en Joden. Anders gezegd: het christendom is het onderwerp van de theologie.

De filosofie is de voorwaarde van haar bestaan ontgroeid. Geconfronteerd met Chaos bracht hij orde in de Chaos. De filosoof was belangrijk voor de wereld, want zijn geest was een speciaal geval: hij had – op een onnatuurlijke manier – last van Chaos. Als gevolg van de filosofie van het gezond verstand was de spanning weg. Pragmatisten vertellen dat de geest een gereedschap is, een instrument om de gewone man tevreden te stellen. Iedereen, zo zeggen ze, kan de wereld begrijpen. Niemand wordt gek van zijn abnormaliteit. De wereld wordt al bij voorbaat gerechtvaardigd als zou de goddelijk en ordelijk zijn.

De kern: alles veranderde toen theologen en filosofen begonnen te generaliseren en abstraheren.  “God, mijn God, waarom hebt Gij mij verlaten” stond niet meer centraal voor de theologen en ze begonnen te spreken over andermans Goden. “De wereld is uit zijn voegen” stond niet meer centraal bij de filosofen en ze begonnen te spreken over de wereld van iedereen. Gevolg: uitspraken als “Het is alsof een mindere God de wereld heeft gemaakt…” en “God is in zijn hemel – Alles is goed met de wereld!”


#### Hoofdstuk 7. Theologie en Filosofie hebben stuivertje gewisseld.

Er kan geen theologie zijn zonder het risico van godslastering. Wanneer God niet afwezig is in mijn spreken, doet het er toe wat ik zeg en doe ik er toe en staat er veel op het spel. Wanneer God als afwezig wordt behandeld is alle bezinning risicoloos.

Er kan geen filosofie zijn zonder het risico van waanzin. Want werkelijke filosofie – liefde voor de waarheid – speelt zich af op het scherp van de snede. Tenminste: wanneer men werkelijk beseft dat de wereld een labyrint is dat geen oriëntatie biedt. Je terugtrekken uit de wereld met haar concrete en klemmende vragen is risicoloos.

Nietzsche moest in meerdere tijdperken leven om te komen tot een doorlopende (continue) tijd, want:
1. Hij was het slachtoffer van de verovering van de ruimte
2. Hij voorzag dat anderen in opstand zouden komen
3. Hij is niet simpel de man die gek werd, maar benoemde en overleefde zijn eigen waanzin en die van de tijd waarin hij leefde.

Juist door Nietzsche’s ‘verscheurdheid’ wordt de aarde van de mens als gekruisigd openbaar. Want het kruis is niet alleen een historisch, maar ook een wereldlijk feit, omdat het de waarheid is over de menselijke natuur. Door Nietzsche is het kruis het centrum geworden van de nieuwe wetenschap van tijd en mens. De wereld van de wetenschap is ruimte, zij behandelt de tijd als bijzaak.

De mens kan ervoor kiezen om te lijden aan het kruis, om deel uit te maken van zijn eigen tijd. Niemand wordt daartoe gedwongen. Niemand is daartoe verplicht, maar iedereen kan vanaf nu weten dat dát de simpele waarheid is over de menselijke natuur.\
Daarom kan niemand nu nog spreken over:
1. Een menselijke geest die superieur is aan het menselijk lichaam
2. Een tijdloze rede
3. Onwankelbaar zelfvertrouwen in het menselijk karakter, onze rede

De menselijke natuur is namelijk onberekenbaar, instabiel, verscheurd, zwevend tussen verleden en toekomst. De mens kan hoogstens volledige desintegratie,  complete verwarring en een gespleten persoonlijkheid vermijden.


>*Rudolf Kooiman*
