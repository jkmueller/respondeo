---
title: Bijeenkomst 3 mei 2025
thema: "Nietzsche en de belichaming van de waarheid"
category: bijeenkomst
created: 2025-02-08
published: 2025-02-08
order: 1
landingpage: front
order-lp: 1
---

### {{ page.thema }}

<!--more-->
![Ichtuskerk Colmschate]({{ 'assets/images/ichtuskerk_colmschate.jpeg' | relative_url }}){:.img-right.img-small}

De vereniging Respondeo nodigt je van harte uit voor een bijeenkomst op 11 november 2024 de Ichthuskerk, Holterweg 106, 7429 AH Colmschate-Schalkhaar.




***Lees mee en overweeg mee… want dat is wat we gaan doen op 16 november.***

De tekst van Rosenstock-Huessy over Nietzsche is in het Nederlands vertaald en te vinden op de website: [De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ '/text-erh-functie-van-friedrich-nietzsche-in-de-kerk' | relative_url }})

In de middag is de ledenvergadering

Het programma:
- 10.30 Ontvangst en koffie
- 11.00 Opening, samen lezen onder leiding van Jan Kroesen
- 12.30 Lunch
- 14:30 Ledenvergadering
  1. Opening
  2. Jaarverslag (2024), zie nieuwsbrief)
  3. Financieel jaarverslag (idem)
  4. Bestuursverkiezing
  5. Rondvraag
  6. Sluiting
- 15.00 Koffie en thee
- 15.30 Sluiting

Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo.
Otto Kroesen, Leeuweriklaan 3, 2623 RB, Delft, [email](mailto:info@rosenstock-huessy.com)


De Ichthuskerk is met bus bereikbaar vanuit Deventer, maar een van de bestuursleden kan je ook altijd even bij het station in Deventer ophalen, als je contact opneemt.

zie ook: [Waarom Nietzsche maskers moest dragen: Aantekeningen van Rudolf Kooiman]({{ '/rk-aantekeningen-rosenstock-en-nietzsche2' | relative_url }})
