---
title: "Otto Kroesen: De botsing tussen Oost en West in Europa (4)"
category: reactie
essay-nr: 170
created: 2023-06-12
published: 2023-06-25
---

#### Civiliteit of liberalisering

Het Westen heeft een tamelijk verkort begrip van de fundamenten waar het zelf op gebouwd is. Het liberalisme gaat uit van mensen die in vrijheid een sociaal contract sluiten tot wederzijds voordeel. De mens is een “homo economicus”, calculerend eigenbelang brengt hem tot samenwerking. Dit beeld van de mens heeft grote schade aangericht tijdens de liberalisering van de economie in de Rusland van de 90-er jaren. De schade die het op de lange termijn in het Westen zelf aanricht is mogelijk nog groter.

#### Civiele samenleving

De samenleving is niet gebouwd op berekenend gedrag, maar op vertrouwen en betrouwbaarheid aan de ene kant en controlerende instituties aan de andere kant [^1]. Stap voor stap zijn die opgebouwd, want als je elkaar niet om te beginnen vertrouwt accepteer je ook de checks and balances van de instituties niet. Neem de gilden en steden in de middeleeuwen als voorbeeld: een eed op de wetten van de stad of van het gilde was vereist voor lidmaatschap. Gilden waren ook godsdienstige instellingen, want die betrouwbaarheid en trouw moest telkens expliciet gemaakt worden om ze in te oefenen[^2].

Op hun beurt hebben deze instellingen karakters gevormd. Een nieuw “zelf”, een nieuw menstype, is tot aanzijn gekomen. Hoe zou je anders de kas kunnen toevertrouwen aan iemand die niet van jouw familie was? Civiliteit betekent dit: je behandelt ook niet-familieleden alsof het familie is[^3]. Wederzijds vertrouwen is niet de enige verworvenheid. Daar komt bij pluriformiteit: je kunt het heel erg oneens zijn en toch waar mogelijk nog samen iets doen. Je kunt kritiek ontvangen en kritiek geven zonder dat de relatie breekt: we zijn allemaal zondaren, dat weten we. Dat besef heeft overigens ook de gevoeligheid voor status drastisch doen afnemen. Door deze verworvenheden worden allerlei problemen opgelost in onderling geven en nemen, zonder dat daar overheidsoptreden aan te pas komt. Dat is niet een natuurlijk gegeven, maar het is een leerproces in samenwerking op steeds grotere schaal die door de Europese Revoluties is gecreëerd. Naast nieuwe rechtsvormen (scheiding der machten, rechtsstaat, representatie) hebben die revoluties tot nieuwe menselijke eigenschappen geleid. Deze eigenschappen en waarden houden op hun beurt deze instituties overeind.

#### Rusland in de jaren 90

De jaren 90 van de vorige eeuw waren een experiment met liberalisering van de Russische economie zonder de aanwezigheid van een onderliggende civiele samenleving. Men dacht dat het wegnemen van belemmeringen in de vorm van staatsmonopolies genoeg was. De mensen hadden echter geen vertrouwen in elkaar en waren niet betrouwbaar jegens elkaar en alle andere eigenschappen die een civiele samenleving op die basis zou kunnen opbouwen waren evenmin voorhanden. Vroegere directeuren van staatsmonopolies, maffiabazen en KGB agenten wisten zich de vroegere staatsbedrijven toe te eigenen. Om een van de vele trucs te noemen: als een bedrijf geveild werd deed men dat vaak in afgelegen gebieden, zodat concurrenten daar of niet van wisten of niet konden komen [^4]. Waardeloze fabrieken werden voor miljoenen verkocht en andersom naargelang het uitkwam. Overboekingen voor verkochte staatsbedrijven werden gedirigeerd naar privérekeningen. Enzovoort. Volgens de redeneringen van de liberale “homo economicus” moest deze chaos van vrije concurrentie vanzelf leiden tot een aantal sterke bedrijven die de concurrentie aan konden, maar men vergat daarbij dat de instituties om eerlijk spel af te dwingen er ook niet waren. Met behulp van vrienden aan de top is het mogelijk concurrenten met betere producten toch uit te schakelen.

Marshall Goldmann zegt er in een artikel uit de jaren 90 dit van: “This has the potential for social upheaval that could easily undermine the existing regime and open the door to a form of proto-fascism claiming to have the only authority to redress past inequities” [^5]. Het zijn profetische woorden. Precies zo hebben de dingen zich ontwikkeld.

Is er dan vanuit Rusland geen verzet daartegen? De Russische oppositie heeft het geprobeerd en is gesmoord. De bevolking van Belarus heeft het geprobeerd en wordt met behulp van Poetin in bedwang gehouden. Er wordt wel gesteld dat eigenlijk Oekraïne een gemeenschappelijke geschiedenis heeft met de Rusland. Oekraïne is altijd grensgebied geweest tussen Oost en West, wat de naam ook betekent. Oekraïne kan ook, zo men wil, beschouwd worden als dat deel van Rusland dat vastbesloten is te breken met de Russische knoet zoals die zich ontwikkeld heeft na de jaren 90. Als Oekraïne daar succesvol in blijkt te zijn gaan meer landen die koers kiezen. Het is altijd weer indrukwekkend het verzet van Oekraïne tegen de Russische koers te zien: alles beter dan dit!

#### Russificatie volgens Ehrenberg

Het westerse liberalisme echter heeft geen sensitiviteit voor de onderlaag van civiliteit die het “vrije spel van maatschappelijke krachten” draaglijk maakt en bijstuurt. Hoe kon het dan ook oog hebben voor het volkomen ontbreken van een civiele maatschappij in de Sovjet-Unie? Daarom heeft ook het Westen een probleem, want door louter calculerend met mensen om te gaan vermindert ook in het Westen het publieke en anonieme vertrouwen in elkaar en in de instituties en verdampen langzaam alle menselijke kwaliteiten die daarmee verbonden zijn. Mensen worden functies in een megamachine en hun verantwoordelijkheid wordt geschonden door protocollen. Het Westen zelf ziet niet hoezeer haar eigen samenleving nog steeds functioneert dankzij wederzijdse verantwoordelijkheid, die helaas niet wordt gecultiveerd. Neoliberalisme en neo-stalinisme besturen beide de samenleving middels een totaalcalculatie van maatschappelijke krachten. Morele machten worden door beide samenlevingen zoveel mogelijk buitenspel gezet.

De oplossing die Ehrenberg voorstelt bestaat in wat hij noemt Russificatie (zie vorige bijdrage). De grammatica van de taal van de liefde, van opdracht en antwoord, van aangesproken zijn en draagvlak creëren, moet zowel in Oost als in West een nieuwe samenleving realiseren. Hij kan dat russificatie noemen omdat het een toepassing is midden in de maatschappij van de wederzijdse liefde van de broeders en moeders van de Russische kloosters en kerken. De grammatica van de liefde moet het veld van de harde maatschappij betreden [^6].

In het begin van de 20e eeuw hebben zowel Russische schrijvers en filosofen als westerse (joodse) geleerden het geprobeerd (zie vorige bijdrage). Er ligt hier nog een onbenut reservoir voor Oost en West. Beiden hebben het hard nodig, zowel om intern de vrede te oefenen alsook om in hun grensgebied, in Oekraïne, vrede te stichten.

[^1]: Fukuyama, F., 2011. The Origins of Political Order, Exmouth House, London.

[^2]: Rosser, G., 2015. The Art of Solidarity in the Middle Ages – Guilds in England 1250-1550, Oxford, United Kingdom.

[^3]: Rosser, The Art of Solidarity in the Middle Ages, 2015, 59. De woorden uit Mattheus 12: 46 – 50 worden vaak geciteerd in de constituties van de gilden en broederschappen: “Wie is mijn moeder en wie zijn mijn broers? Hij maakte een gebaar naar zijn leerlingen en zei: zij zijn mijn moeder en mijn broers. Want ieder die de wil van mijn Vader in de hemel doet is mijn broer en zuster en moeder.”

[^4]: Marshall Goldman, The Pitfalls of Russian Privatization, in Challenge , Vol. 40, No. 3 (May-June 1997), pp. 35-49 Taylor & Francis.

[^5]: Ibid.
[^6]: Kroesen J.O., 2015. Towards Planetary Society: the Institutionalization of Love in the work of Rosenstock-Huessy, Rosenzweig and Levinas, in Culture, Theory and Critique, Taylor and Francis 56:1, DOI: 10.1080/14735784.2014.995770, pp. 73-86.
