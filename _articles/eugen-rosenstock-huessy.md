---
title: Wie is Eugen Rosenstock-Huessy
category: lebensbild
order: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_4c.jpg' | relative_url }}){:.img-right.img-small}
Eugen Rosenstock-Huessy (Berlijn, 6 juli 1888 – Norwich, Vermont, 24 februari 1973) was een rechtshistoricus die zich ook bewoog op het terrein van de menswetenschappen.

Hij werd geboren in Duitsland in een joods gezin en sloot zich op jonge leeftijd aan bij de protestants-christelijke kerk. Hij studeerde rechten en specialiseerde zich in rechtsgeschiedenis.

In de Eerste Wereldoorlog diende hij als officier aan het front bij Verdun. In deze periode voerde hij een intensieve discussie met de joodse filosoof Franz Rosenzweig. Beide auteurs hebben de resultaten van deze joods-christelijke dialoog op vele plaatsen in hun oeuvre verwerkt.

Na de Eerste Wereldoorlog werkte Rosenstock, die na zijn huwelijk de familienaam van zijn echtgenote Margaretha Huessy als tweede naam aannam, onder meer bij de Daimler Benz-fabrieken. Hij verdiepte zich daar in het probleem van [levensvragen in het industriële tijdperk](https://www.boekwinkeltjes.nl/b/187275205/Werkstattaussiedlung-Untersuchung-ueber-den-Lebensraum/). Als uitvloeisel hiervan werd hij eind jaren twintig van de twintigste eeuw onder meer vicevoorzitter van de World Association of Adult Education.

Ook werkte hij in deze periode op publicitair gebied samen met filosofen als Martin Buber. In 1923 aanvaarde Rosenstock een hoogleraarschap in de rechtsgeschiedenis aan de Universiteit van Breslau in het toen nog Duitse Silezië (het tegenwoordige Wroclaw in de Poolse provincie Slask). Hier legde hij de grondslag voor vrijwilligerskampen voor boeren, arbeiders en studenten, die in geheel Duitsland een groot succes zouden worden. Hij deed dit onder meer in samenwerking met Helmuth James von Moltke, die tijdens het Hitler-bewind de drijvende kracht werd van de verzetsbeweging Kreisauer Kreis.

Na de machtsovername door Adolf Hitler in 1933 legde Rosenstock-Huessy zijn werk als hoogleraar aan de rechtenfaculteit in Breslau neer en emigreerde hij naar de Verenigde Staten. Daar doceerde hij geruime tijd aan de Harvard University en na enkele jaren aan Dartmouth College in Hanover (New Hampshire).

In de VS stichtte Rosenstock-Huessy onder meer een vrijwilligerskamp voor jonge studenten werklozen onder de naam [Camp William James](https://en.wikipedia.org/wiki/Camp_William_James). Deze naam verwees naar het essay “The moral equivalent of war” waarin de Amerikaanse filosoof William James pleitte voor een sociale dienstplicht als alternatief voor militaire dienst.

Rosenstock-Huessy bleef tot zijn dood in 1973 zeer productief als schrijver van boeken. Een centraal thema in zijn werk wordt gevormd door de werking van de tijd in de menselijke geschiedenis en de centrale rol daarin van het christelijk geloof.

Een [indrukwekkende biografie in het engels](http://www.erhfund.org/biography/) staat op de site van het Amerikaanse Eugen Rosenstock-Huessy Fund.
