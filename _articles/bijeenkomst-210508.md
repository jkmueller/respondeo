---
title: Virtuele voorjaarsbijeenkomst 8 mei 2021
thema: "“Mijn ontelbare identiteiten” van Sinan Çankaya"
category: bijeenkomst
created: 2021-05-08
order: 1
---
### {{ page.thema }}
![Zoom Meeting]({{ 'assets/images/zoom-meeting.jpg' | relative_url }}){:.img-right.img-small}

Agenda:

| 13:45 | bijeenkomst toegankelijk
| 14:00 | welkom, kort rondje: wie is er?
||. eerste deel
| 15:00 | pauze
| 15:15 | tweede deel
| 16:15 | afronding, tijd voor mededelingen
| 16:30 | einde

Geef je op door een mail te sturen.

Deze bijeenkomst is geen plaats om hele doorwrochte betogen of discussies te voeren. Lees het boek, of grasduin in onze nieuwsbrief, of vraag jezelf af: Aan welke stukken of uitspraken van Rosenstock doet je dit denken? In een bestuursvergadering werd genoemd de brief van Bas Leenman. Dit staat in “als God stukloopt”, uitgegeven bij Skandalon.

In dagblad Trouw van 6 juni 2020 heeft een interview gestaan met Sinan Çankaya. Aanleiding is de publicatie van het boek, het artikel is aangekondigd met het citaat: “Onwrikbare identiteiten kunnen dodelijk zijn”

![Mijn ontelbare identiteiten]({{ 'assets/images/cankaya-ontelbare.jpg' | relative_url }}){:.img-left.img-large}
*‘Wat willen jullie dat ik zeg?’ vraagt Sinan Çankaya als hij wordt uitgenodigd om te spreken op het veertigjarige jubileum van zijn oude middelbare school. De uitnodiging creëert een opening waar vergeten herinneringen uit zijn jeugd en middelbareschooltijd zich doorheen wurmen. Zo denkt Çankaya terug aan zijn voormalige geschiedenisleraar Nico Konst, ooit de tweede man van de extreemrechtse Centrumpartij.Zonder zichzelf te sparen beschrijft Çankaya in Mijn ontelbare identiteiten hoe moeilijk hij het vindt om een eigen verhaal te vertellen tegen de achtergrond van de debatten over integratie, cultuur en racisme. Steeds laveert hij tussen wortelen en willen vluchten, tussen ‘wij’ en ‘zij’, tussen straat en school, tussen een erbij en er niet bij horen, tussen ongebreideld individualisme en een soms beklemmend collectivisme. Op de dag van de jubileumviering, waar hij zijn oude geschiedenisleraar Nico Konst weer onder ogen moet komen, vertelt hij zijn verhaal.
Mijn ontelbare identiteiten is een bespiegeling op de veranderde omgang met de ‘de Ander’ in Nederland. Het is een indringend verhaal over opgroeien in Nederland als kind van migranten. Reflecterend op loyaliteit, ontheemding en vooral de zoektocht naar een thuis keert Çankaya zich tegen vastomlijnde identiteiten en weigert hij om een verhaal binnen de grenzen van zijn eigen lichaam te vertellen.* [Tekst bij dit boek van de uitgever [De Bezige Bij](https://www.debezigebij.nl/boek/mijn-ontelbare-identiteiten/)]
