---
title: "Interim 13 – juni 2020"
thema: "Secularisering van kerk naar samenleving"
category: interim
created: 2020-06
order: 1
---

### {{ page.thema }}

Het voorlopig laatste nummer van de Interim is uit, grotendeels gewijd aan de gezamenlijke studiedag van Respondeo en de Girardkring. Gehouden op 21 september 2019 met als thema: Secularisatie, Over westerse waarden in radicale tijden.

INHOUDSOPGAVE
* Inleiding – Jan Kroesen
* Voorwoord – Jan Kroesen
* Secularisering, over westerse waarden in radicale tijden – Hans Boutellier
* Westerse waarden in radicale tijden – Daan Savert
* Seculariteit in waarheid, werkelijkheid en toekomst – Jan Kroesen
* Impressie van de studiedag ‘Over westerse waarden in radicale tijden’ – Truus de Boer
* Nawoord – Jan Kroesen, Wilmy Verhage
* Uit het bestuur – Otto Kroesen
* Achterpagina


[Interim 13-2020]({{ 'assets/downloads/Interim_13_2020.pdf' | relative_url }})

**De inleiding** werd verzorgd door Hans Boutellier (o.a. Verwey-Jonker Instituut), een levendige uiteenzetting van zijn boek Het seculiere experiment : hoe we van God los gingen samenleven.

In zijn boek brengt hij veel van z’n eerdere publicaties bij elkaar. Dat betreft onderzoek naar criminaliteit, veiligheid, seksueel gedrag. Maar het is ook een manier van kijken en interpreteren waarin hij een duiding geeft van het weefsel van onze maatschappij. Centraal daarin is zijn aandacht voor de moraliteiten die daarin een rol spelen. Hij is daarin zeer getroffen door de talloze verschillen in opvattingen en de vraag wat ons (nog) bij elkaar houdt. Ondanks het feit dat de grote verhalen van Kerk en Staat niet meer de krachtlijnen zijn aan de hand waarvan groepen en mensen hun leven inrichten, blijkt vooral ook dat de samenleving met het afscheid daarvan niet ineen is gestort. Dit laatste is een steeds terugkerend refrein in dit nummer van de Interim. Maar ook is er de vraag: Blijft het zo?

Weinig konden we vermoeden dat de radicale tijden waarnaar in de subtitel verwezen wordt, met spoed verder zouden radicaliseren. De coronapandemie heeft de wereld veranderd. De al in gang zijnde ontwikkelingen, aldus Geert Mak, krijgen een sterker accent.

Hans Boutellier schetst de spanningsvelden: Criminaliteit, veranderingen in opvattingen omtrent seksualiteit, migratie, de plaats van de sociale wetenschappen passeren de revue. Het beeld dat oprijst is dat van maatschappelijke verwikkelingen en de inzet en de manier om ‘de boel bij elkaar te houden’ in een samenleving die als ‘hypermoreel’ is de karakteriseren.

**In de reactie van Daan Savert, vanuit de Girardkring**, wordt hierop heel puntig opgemerkt dat het verleidelijk is om een bepaalde omvattende analyse van deze tijd van iemand als René Girard te presenteren als een groot en sterk verhaal om opnieuw betekenis te vinden in de maatschappelijke kakafonie, maar tegelijkertijd besef je dat dit onmogelijk is, omdat je je daarmee precies in het spel begeeft waaraan je niet mee wilt spelen. Het spel van de rivaliteit. De ander(e groep) zien als obstakel voor wat jezelf ziet als ‘goed’, waarmee je juist de strijd aangaat die je het liefst zou willen vermijden: het verketteren van de anderen, die het kwaad vertegenwoordigen.

**In een antwoord dat Jan Kroesen tracht te geven wordt in de lijn van Eugen Rosenstock-Huessy**, gevraagd naar wat de verschillende taal- en tijdsritmen waarin mensen zijn opgenomen kan verbinden. Het seculiere tijdsritme waarin mensen in ultieme zin arbeidskrachten zijn die per uur kunnen produceren en het tijdsritme van de eigen ziel: De strijd van geduld en verdragen en van zijn leven ergens aan geven. Ons zieleritme lijkt veel te langzaam. Een evaluatief moment in het geheel is de enthousiaste reactie van Truus de Boer naar aanleiding van deze dag. Zelf zei ze al dat het geschreven is in de euforie van het moment. Maar toch geeft ze haarscherp weer waarin voor haar de waarde van deze bijeenkomst is gelegen. Haar trefwoorden zijn offer, vergeving en levensbeschouwelijke vorming. Een enkele opmerking van de vader van Hans Boutellier echoot een leven lang na in zijn zoon Hans Boutellier. Wat kan de vormingstijd van een jeugd dan niet nog meer achterlaten, zo kan men vragen. Of anders gezegd: Hoeveel toekomst ligt daar niet al in besloten?
