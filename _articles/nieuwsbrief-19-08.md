---
title: Uitnodiging studiedag 21 september 2019 Dominicuskerk Amsterdam
category: nieuwsbrief
created: 2019-08
order: 1
---
De bijlage is een printvriendelijke versie van onderstaand bericht.

De Girardkring en de vereniging Respondeo nodigen je van harte uit voor een studiedag op zaterdag 21 september in de Dominicuskerk in Amsterdam.

Over westerse waarden in radicale tijden:

Uitingen van radicalisering brengen morele verlegenheid aan het licht.

De dag wordt ingeleid door Hans Boutellier – wetenschappelijk directeur van het Verweij-Jonkerinstituut.

In onze turbulente tijd lijken veel vanzelfsprekende uitgangspunten van de democratische rechtsorde onder druk te staan. We zien opkomend nationalisme en groeiend verzet tegen migratie en tegen de elite, terwijl de islam een steeds grotere rol inneemt in het publieke domein. Hans Boutellier publiceerde onlangs een herziene versie van zijn boek Het Seculiere Experiment. Hierin analyseert hij de consequenties van de seculiere conditie. Na de ontzuiling ontwikkelde zich een door en door pragmatische samenleving, maar deze lijkt geen antwoord te kunnen bieden op de zorgen van mensen. Hij ziet zowel morele verlegenheid als toenemende radicalisering van verschillende zijden. Boutellier maakt in zijn inleiding de balans op van de seculiere conditie en gaat graag met u in gesprek over perspectieven voor de toekomst.

De bijeenkomst is een initiatief van twee werkgezelschappen, die respectievelijk het werk van René Girard en Rosenstock-Huessy onder de aandacht brengen. René Girard heeft gepubliceerd over het zondebokmechanisme. Rosenstock-Huessy heeft gepubliceerd over de noodzaak om de saamhorigheid van oude stammen te doen herleven maar niet hun onderlinge gewelddadigheid.

Het programma als geheel:

| 10.00 | Inloop
| 10.30 | Opening en lezing Hans Boutellier
| 11.45 | Discussie
| 12.30 | Lunch
| 13.30 | Daan Savert geeft een reactie vanuit Girardkring met aansluitend discussie
| 14.30 | Pauze
| 14.45 | Jan Kroesen geeft een reactie vanuit Respondeo met aansluitend discussie
| 15.45 | Afsluiting, borrel

Plaats: Dominicuskerk in Amsterdam

Graag vooraf opgeven in verband met de lunch.

Inlichtingen en opgave:\
Mail: vereniging.respondeo@gmail.com
Egbert Schroten (Secretariaat Respondeo), tel: 0528.202041\

Website [Girardkring](https://www.girard.nl)\
Website [Respondeo]({{ '/index' | relative_url }})

--
met vriendelijke groet,

Egbert Schroten, secretaris Vereniging Respondeo
