---
title: "Otto Kroesen: De botsing tussen Oost en West in Europa (1)"
category: essay
essay-nr: 170
created: 2023-06-12
published: 2023-06-25
landingpage:
order-lp: 2
---

### De koepel en het schip

In een oorlog, ook die in Oekraïne, gaat het om de macht. Maar het gaat nooit om de botte macht. Twee samenlevingen botsen, twee manieren van leven, oftewel, een verschillend verstaan van de geestelijke machten die ons bestaan in regie nemen.
![Ayah Sophia]({{ 'assets/images/Ayah_Sophia.jpg' | relative_url }}){:.img-left.img-large}

<!--more-->

Dat verschil in waardensysteem (seculier gezegd) of geestelijke machten (religieus gesproken) probeer ik in enkele bijdragen wat scherper te maken [^1]. Ik hoop daarmee dat wij de ander beter leren kennen en daardoor ook onszelf.

#### De architectuur van de oude kerk
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}

De architectuur verraadt vaak een verschil in geestelijke perceptie van de werkelijkheid. Het oude christendom kende vooral koepelkerken, zoals de Ayah Sophia in Constantinopel. Onder de open hemel worden de mensen in de kerk verzameld. Vaak zien we de verheerlijkte Christus vanaf het plafond naar beneden kijken. Hij heeft alle macht in hemel en op aarde. En hij trekt alles naar zich toe (Johannes 12:32). Je moet de ogen dus op de hemel gericht houden.

In de oude kerk kon je eigenlijk niet verwachten dat je de wereld ingrijpend kon veranderen. Maar je kon wel zelf, en als gemeente, anders leven, met grotere verantwoordelijkheid en zorg voor elkaar. En dat deed men dan ook. Dat was moeilijk genoeg in de harde werkelijkheid, maar dan werd het toch in ieder geval in de kloosters gedaan. En ook daar blijft het moeilijk. Je kon je ziel behouden en redden uit deze wereld [^2]. Het woord ziel is wat uit ons spraakgebruik verdwenen, maar vindt toch ook in onze tijd een equivalent in de uitdrukking “persoonlijke integriteit”.

Maar dat betekende ook dat men het publieke leven niet door direct ingrijpen probeerde te veranderen. Met name in het Oost Romeinse Rijk bleef de kerk aanleunen tegen en genoegen nemen met de macht van de keizer in Constantinopel. Na de verovering van Constantinopel in 1453 beërft de Russische kerk deze levenshouding. Voor persoonlijke integriteit moesten mensen het hebben van Gods genade, en voor aardse gerechtigheid moest men hopen dat de keizer ook onder de invloed van die genade geraakte [^3].

#### De architectuur van de moskee

Overigens had die levenshouding ook in het Oost Romeinse Rijk al voor veel maatschappelijke onverschilligheid gezorgd. Het Oost Romeinse Rijk na Constantijn de Grote was wel gekenmerkt door een veel menselijker en menswaardiger regime dan het oude Romeinse Rijk, maar toen met name in de vijfde en zesde eeuw grootgrondbezitters steeds meer macht kregen en veelal in Constantinopel waren om hun belangen te behartigen, terwijl in de dorpen de mensen krepeerden, kwam er vanuit de Arabische stammen een reactie: alle mensen, ook de grootgrondbezitters, moesten nu vijf keer per dag in het stof buigen, en men kon de erfenis niet meer ongedeeld doorgeven [^4].

Deze aanval van de islam op de ongelijke machtsverhoudingen vindt zijn uitdrukking opnieuw in de architectuur. De christelijke koepelkerken worden nu zo gebouwd dat als het ware het gebouw en heel de aarde hangt aan de pilaren, hangt aan de hemel, met minaretten als draden naar boven. Gods almacht slokt de aardse werkelijkheid op en brengt de stammen en de mensen tot eenheid en gelijkheid.

#### De architectuur van het Westen

In het Westen worstelde men met dezelfde problemen, maar er was al een wat andere oriëntatie op gang gekomen na de verovering van Rome door Alaric in 410. De westerse kerk kon niet meer op een keizer vertrouwen zoals de oosterse en Augustinus schreef zijn boek De Civitate Deï na de verovering van Rome. Daarin betoogt hij dat de verovering van Rome niet echt een ramp is, want het moet de kerk erom gaan de stad Gods op aarde te brengen, en dat kan ook zonder Rome.
![Nieuwe Kerk Delft]({{ 'assets/images/Nieuwe_Kerk_Delft.jpg' | relative_url }}){:.img-right.img-small}

In de donkere eeuwen na Karel de Grote hebben de grootgrondbezitters (“roofridders”) in het Westen nog erger huisgehouden dan in het oosten. In 1076 verklaarde paus Gregorius VII dat hij boven de keizer staat en dat leidt tot een enorme machtsstrijd tussen aanhangers van de paus en aanhangers van de keizer. Maar dat is niet louter een politiek conflict, het is een maatschappelijk conflict dat heel de samenleving raakt. Veel kloosters en grootgrondbezitters speelden onder één hoedje en veel oprechte monniken en gewone mensen zetten zich daar met alle macht tegen af [^5]. In dit conflict ontstond er ruimte voor de gilden en broederschappen, organisaties van onderaf. Daarin kon men min of meer vrijelijk zijn gang gaan, omdat de pauselijke partij er steun aan gaf, zodat de keizer het niet goed kon tegenhouden [^6].

Daarmee komt in het Westen ook een nieuw soort kerkbouw op: wie bijvoorbeeld in de Nieuwe Kerk in Delft (vergeet niet dat het oorspronkelijk een katholieke kerk is!) binnen gaat, ziet niet een koepel boven zich, maar een weg voor zich. Die weg is bovendien gericht op het oosten, op Jeruzalem. Die weg bestaat uit stadia. Men gaat steeds verder naar binnen in het schip van de kerk. Men gaat ook steeds verder met zijn blik omhoog. Daar zijn voorlopige halteplaatsen, een krans, een dwarsbalk, maar dan wordt de blik weer verder getrokken naar boven of naar voren. De westerse kerk, ook in zijn kerkbouw, is niet meer louter gericht op de hemel, maar ook op een stapsgewijze verandering van de aarde.
![Orthodoxe Kerk]({{ 'assets/images/orthodoxe_kerkbouw.jpg' | relative_url }}){:.img-left.img-small}


De oosters orthodoxe Kerk heeft dat steeds als een ketterij gezien. Daarom is de oosters orthodoxe kerk ook “orthodox”. De oosters orthodoxe Kerk blijft erbij dat je als kerk de aarde niet kunt verlossen, de zeggenschap over de aarde blijft aan de wereldse machten voorbehouden. [3] Anders dan de moskee symboliseert de oosters orthodoxe kerkbouw een tweedeling: de koepel of beter de koepels (er zijn meerdere patriarchaten) verheft zich boven de aarde, en is duidelijk gescheiden van de aardse onderlaag. De oosters orthodoxe Kerk maakt de westerse vernieuwing niet mee. Zij heeft daar grote bezwaren tegen. Het is duidelijk dat reeds 1000 jaar geleden de richting ingeslagen is naar een heel andere samenleving.

[^1]: Ze zijn hier tegelijkertijd gepubliceerd, eerder op de website van een Kerkelijke Gemeente in Delft. Ze zijn ook enigszins bewerkt en van voetnoten voorzien.

[^2]: Noble, D.F., 1997. The Religion of Technology. New York: Penguin Books p. 11.

[^3]: In de bundel van Ehrenberg uit 1923 wijst Aksakow op de rolverdeling tussen keizer en volk. Het volk accepteert de almacht van de staat, maar leeft in het wijde land zijn eigen leven los daarvan. De staat is niet meer dan uiterlijke bescherming, K.S. Aksakow, Ausgewählte Schriften, in Hans Ehrenberg, 1923. Östliches Christentum, Oskar Beck München 88, ff.

[^4]: Armstrong wijst op deze betekenis van het islamitische gebed vijf keer per dag, Armstrong K., 1995. Een geschiedenis van God – 4000 jaar Jodendom, christendom en islam, Ambo, Amsterdam (Orig. 1993). Kuran wijst erop dat volgens het islamitische erfrecht niet meer dan een derde van de grond in de familie bij elkaar mag blijven, de rest wordt verdeeld. Het is een maatregel tegen het grootgrondbezit in het Oost Romeinse Rijk, zie Kuran, T., 2011. The Long divergence: how Islamic Law held back the Middle East. Princeton University Press.

[^5]: Zie Moore, R.I., 2000. The first European Revolution, 970-1215, Blackwell Publishing, Oxford.

[^6]: Zie Rosenstock–Huessy, E., 1989. Die Europäischen Revolutionen und der Charakter der Nationen, Moers, Brendow (Orig. 1931).

[De botsing tussen Oost en West in Europa (2)]({{ 'ok-botsing-oost-west-2' | relative_url }})
