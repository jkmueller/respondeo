---
title: Bijeenkomst 8 oktober 2016
thema: "Hoe word je een Nederlands burger? Over inwoners en binnenkomers."
category: bijeenkomst
created: 2016-10-08
order: 1
---
### {{ page.thema }}

De vereniging Respondeo houdt op zaterdag 8 Oktober 2016 haar najaarsbijeenkomst bij de Synagoge van Deventer:

### Hoe word je een Nederlands burger?
#### Over inwoners en binnenkomers.

Iedereen is welkom.\
Wij gaan een aantal verhalen horen, recente voorbeelden van al dan niet geslaagde inburgering. Daarna kijken we vanuit historisch perspectief met de visie van Rosenstock-Huessy op de Westerse geschiedenis.

Met een groeiend aantal nieuwe Nederlanders komt volgens sommigen de identiteit van Nederland in gevaar. Anderen vinden de nieuwe binnenkomers een verrijking. Maar gevaar voor of verrijking van wat? Of serieuzer: hoe leven we in vrede samen?

En: wat moet je doen om in te burgeren? Bij inburgeringscursussen gaat het ofwel om kennis van typisch Nederlandse kenmerken (wanneer is Sinterklaas jarig?), of om kennis van procedures (hoe vraag je rijbewijs aan?). Maar word je daarmee als binnenkomer een Nederlands burger?

Rosenstock-Huessy wond zich op over een professor die burgerschap definieerde als “het hebben van een betaalde baan”. De werkelijke burger is diegene die de stad opbouwt als die in puin ligt. Daarin zit de gedachte aan het doen van een extra stap, vrijwilligheid, opoffering. Niet alleen een zakelijke transactie. Ook geen geboorterecht.

Er is ook zoiets als een burgerlijke samenleving, een publieke ruimte, civiliteit, met een aantal specifieke waarden en codes, en daar heeft Nederland een eigen versie van. Maar moeten de Nederlandse inwoners daarin niet even hard in inburgeren als de binnenkomers?

Het programma:

| 10:30 | Ontvangst en koffie
| 11:00 |Verhalen van inburgering – Anneke Kroesen-Bos, lerares Nederlands als Tweede Taal bij Capabeltaal in Den Haag
| 11:30 | Nederland door de ogen van een vluchteling – Samir Maglajlic, onderwijsassistent in Deventer, gevlucht uit voormalig Joegoslavië. Hij heeft ook een boek geschreven  met dezelfde titel.
| 12:00 | Nederlands/Europese burgerschapswaarden – Otto Kroesen, docent interculturele communicatie aan de Technische Universiteit Delft
| 12:30 | Lunch
| 13:30 | Gesprek
| 15:00 |Koffie
| 15:30 | Afronding


Kosten voor deelname (inclusief lunch) bedragen € 15,- vooraf over te maken op NL 57TRIO 0198420757, of ter plekke te voldoen. Je kunt je opgeven bij Egbert Schroten, graag vóór 5 oktober.

Secretariaat Respondeo:
Egbert Schroten\
e-mail:  schroten.hardenberg@tele2.nl\
adres:   Ondermaat 38, 7772 JD Hardenberg

[uitnodiging 8 oktober 2016]({{ 'assets/downloads/2016_Respondeo_uitnodiging_8_okt_burgerschap.pdf' | relative_url }})
