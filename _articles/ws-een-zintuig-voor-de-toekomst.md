---
title: "Wim van der Schee: Een zintuig voor de toekomst"
category: essay
created: 2018-07-09
published: 2018-07-09
---
Lesstof voor de eerste week van mei voor bovenbouw Havo-VWO

Respondeo biedt gastlessen aan en korting bij meerdere exemplaren van het boek Als Hitler valt ?

Het [boekje ‘Als Hitler valt’]({{ '/boek-als-hitler-valt' | relative_url }}) gaat over het verzet tegen Hitler in Duitsland zelf. Een groep vooraanstaande mensen, waaronder Helmuth James von Moltke, werkt jarenlang in het geheim aan het plan hoe Duitsland, als Hitler heeft verloren, opnieuw ingericht kan worden, als een vredelievende rechtstaat, partner in een na-oorlogs verenigd Europa. Na de mislukte aanslag op Hitler op 20 juli 1944 worden duizenden mensen opgepakt onder wie een aantal leden van wat de Kreisauer Kreis werd genoemd. Een van hen was Von Moltke. In januari 1945 wordt hij ter dood gebracht. In zijn afscheidsbrief aan zijn zonen schrijft hij:

*‘Ik heb mijn hele leven lang, al op school, gestreden tegen een geest van bekrompenheid en macht, van de zelfverheffing, de intolerantie en van de absolute meedogenloze drijverij, die de Duitsers met zich meedragen en die zijn uitdrukking heeft gevonden in de nationaal-socialistische staat. Ik heb me er ook voor ingezet dat deze geest met zijn vreselijke consequenties zoals het buitensporige nationalisme, rassenvervolging, ongeloof en materialisme zou worden overwonnen. Wat dat betreft hebben de nationaalsocialisten, van hun standpunt uit, gelijk dat ze mij ombrengen.’*
>*Helmuth James Von Moltke, afscheidsbrief aan zijn zonen, winter 1944/1945*

De groep werd door de voorzitter van het Volksgerechthof vernoemd naar het landgoed van de Von Moltkes in Kreisau, nu Krzyzowa in Polen, waar de voornaamste bijeenkomsten plaats vonden. Enkele overlevenden kregen na 1945 belangrijke overheidsfuncties in de Bondsrepubliek Duitsland, maar het duurde tot het midden van de 80er jaren voordat het belang van deze groep breed erkend werd.

Het Krzyzowa van vandaag is een [internationale ontmoetingsplaats](https://www.krzyzowa.org.pl/en/) waar men zich bezig houdt met het bewerkstelligen van Europese toenadering en begrip. Het hele jaar door is er een uiteenlopend aanbod van workshops/trainingen, bezinningsweken, colleges, conferenties en tentoonstellingen, waaraan mensen – van alle leeftijden – met uiteenlopende sociale, nationale en etnische achtergronden deelnemen. Daarbij wordt gestreefd naar een Europese democratische samenleving met een oriëntering op de universele rechten van de mens, onder andere door de geschiedenis van verzet en de oppositie in de 20e eeuw en tot nu toe te bespreken, als gemeenschappelijke oriëntatie op internationale vrede.

De vereniging Respondeo heeft via haar (oud-)leden een veeljarige band met het vormingswerk in Krzyzowa. Niet alleen omdat [Freya](http://www.fvms.de/en/welcome.html), de weduwe van Von Moltke later in de V.S. samenleefde met Eugen Rosenstock-Huessy, maar vooral omdat we eensgezind zijn in de hoop dat er steeds weer mensen opstaan die zich verzetten tegen onrecht en zich inzetten voor een eerlijke wereldwijde samenleving, waarin alle mensen de kans hebben zich te ontwikkelen en een zelfstandig bestaan op te bouwen.

***De zin van het geschiedenisonderwijs en van maatschappijleer ligt in de oriëntatie op het verleden, de huidige samenleving en de toekomst, om te ontdekken dat de inspanningen om tot vrede en samenwerking te komen noodzakelijk is en van alle tijden. Laat het jonge mensen inspireren, zodat ze het werk kunnen voortzetten, want het goede komt nooit vanzelf.***
