---
title: "Otto Kroesen: Respondeo-podcasts: Een mozaiek"
category: essay
essay-nr:
created: 2020-11-13
published: 2020-11-13
---

**Podcasts hieronder laten de echo van het werk van Rosenstock-Huessy in de levensverhalen van de leden van Respondeo zien. De podcasts zijn gemaakt door Kurt Kooiman en Freek Schröder.** [Podcasts](https://podcasthuis.com)

Respondeo is de naam van de vereniging die het werk van Rosenstock-Huessy onder de aandacht wil brengen. Respondeo – dat is dan wel een toepasselijke naam. Want centraal in het werk van Rosenstock-Huessy staat de taal. Nu is taalfilosofie tegenwoordig redelijk populair. Maar meestal gaat het dan om de betekenis en logische structuur of anders gaat het om het discours. Rosenstock-Huessy ging het er om wat levende taal met mensen doet. Een levend woord is een woord dat je raakt. Dat woorden binnenkomen en ons veranderen en de baan van het leven verleggen – dat is wat de taal levend maakt. Daarover is op deze website het nodige te vinden.

Maar de leden van Respondeo hebben ook zelf die levende betrokkenheid. Voor de meeste leden zijn woorden van Rosenstock-Huessy geen bijkomstigheid geweest, waar je kennis van kunt nemen en naar kunt kijken. Vaak is het zo dat zegswijzen en opmerkingen van Rosenstock-Huessy je iets doen en raken nog voordat je de betekenis goed begrepen hebt.


### Hoe kan ik dit uitleggen ?

Leden van Respondeo over de betekenis van Rosenstock-Huessy: verschillende leden van Respondeo leggen in hun woorden en met hun verhaal uit waar het volgens hen om gaat bij Rosenstock-Huessy.
<audio controls src="{{ 'assets/podcasts/Rosenstock-podcastfinal.mp3' | relative_url }}"></audio>


### Over Eugen Rosenstock-Huessy
![Lise van der Molen]({{ 'assets/images/lise-van-der-molen.jpg' | relative_url }}){:.img-left.img-small}
Lise van der Molen over Eugen Rosenstock-Huessy en zijn levensverhaal. Lise van der Molen is werkzaam geweest als predikant en heeft de bibliografie van Rosenstock-Huessy op zijn naam staan. Deze bibliografie ligt ten grondslag aan het verzamelde werk, dat in de vorm van boeken en artikelen te vinden is op de Amerikaanse website van het Rosenstock-Huessy Fund.
<audio controls src="{{ 'assets/podcasts/Lange-adem-deel-Lise-vd-Molen.mp3' | relative_url }}"></audio>


### Korte en langere tijdbogen
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-left.img-small}
Otto Kroesen over de visie van Rosenstock-Huessy op de verschillende tijdsspannen die de menselijke biografie gestalte geven, korte en langere tijdbogen. Otto Kroesen is als studentenpastor en universitair docent filosofie werkzaam geweest aan de Technische Universiteit Delft en werkt nu bij het Centrum voor Entrepreneurship op het gebied van entrepreneurship voor ontwikkelingslanden.
<audio controls src="{{ 'assets/podcasts/Lange-adem-deel-Otto-Kroesen.mp3' | relative_url }}"></audio>


### Betrokkenheid

Wim van der Schee over de betrokkenheid die in het werk van een psycholoog doorslaggevend is, en ook doorslaggevend in het werk van Rosenstock-Huessy. Wim van der Schee is als orthopedagoog en psycholoog werkzaam geweest in de jeugdzorg, het onderwijs, de psychiatrie en forensische psychiatrie en de gehandicaptenzorg.
<audio controls src="{{ 'assets/podcasts/Lange-adem-deel-Wim-vd-Schee.mp3' | relative_url }}"></audio>
