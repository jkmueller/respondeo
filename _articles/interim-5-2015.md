---
title: "Interim 5 – februari 2015"
thema: "De mens is onbetaalbaar – Oecumene en economie"
category: interim
created: 2015-02
order: 1
---

### {{ page.thema }}
inhoudsopgave
* Inleiding – Wilmy Verhage
* Noodzaak en Aanspraak – Otto Kroesen
* Ervaringen uit de werkpraktijk bij Shell – Wouke Lam
* Een Amerikaanse multinational als Wijngaardenier – Egbert Schroten
* Oogsten – Jan Kroesen
* Het thema: De mens is een geheim – Eugen Rosenstock-Huessy, vertaling: Wilmy Verhage
* De oogst van de bijbel – Bas Leenman
* Noodzaak van de industrie om de kerk te ontmoeten – Bas Leenman
* Lang leve de vriendschap – Henk van Olst
* De herontdekking van de ziel door Rosenstock-Huessy – Bart Voorsluis
* In memoriam Prof. Dr. Ger van Roon – Feico Houweling
* Uit het bestuur
* Achterpagina – Feico Houweling


[Interim 5-2015]({{ 'assets/downloads/interim_5_2015.pdf' | relative_url }})
