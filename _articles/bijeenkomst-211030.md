---
title: Bijeenkomst 30 oktober 2021
thema: "Identiteit in de wij-het-samenleving"
category: bijeenkomst
created: 2021-10-30
order: 1
---
### {{ page.thema }}
![Irene Twello]({{ 'assets/images/irene-twello.jpg' | relative_url }}){:.img-right.img-small}
Plaats: gebouw “Irene”, Dorpsstraat 8, Twello,

Programma:

| 10.30 | Ontvangst en koffie
| 11.00 | Opening, toelichting van het thema vanuit verschillende hoeken
| 12.30 | Lunch
| 13:30 | Open gesprek onder leiding van Otto Kroesen
| 15.00 | Koffie en thee
| 15.30 | Sluiting

In verband met lunch graag vooraf aanmelden met een mailtje \
aan vereniging.respondeo@gmail.com.
