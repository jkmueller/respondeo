---
title: Over Respondeo
category:
order: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_4c.jpg' | relative_url }}){:.img-right.img-small}
### Respondeo etsi mutabor

>*Ik antwoord, al verander ik daardoor*

*In onze tijd waarin de oude religieuze stelsels voor velen hun betekenis hebben verloren, scheppen de ideeën van Rosenstock-Huessy een ongekende ruimte en verrassende inzichten, ook in de diepste waarheid van het christendom.*

Soms leven er mensen die hun tijd ver vooruit zijn en antwoorden vinden op problemen die in het verschiet liggen. Zo iemand was Rosenstock-Huessy. Hij werd geboren in 1888 in Duitsland, in een Joods gezin, en emigreerde in 1933 naar Amerika. Zijn omvangrijke werk zou sociologisch of theologisch genoemd kunnen worden, maar hij onttrok zich aan de grenzen die de wetenschap stelt. Hij wilde niet beschouwend te werk gaan, maar vanuit het leven zelf de gebeurtenissen doen spreken in menselijke taal.

De ***vereniging Respondeo*** stelt zich ten doel het bestuderen van het werk van Eugen Rosenstock-Huessy. De vereniging tracht dit doel te bereiken door het organiseren van bijeenkomsten en het bevorderen van de beschikbaarheid van zijn werk door middel van vertaling, uitgave en beheer.

De bestuursleden zijn:

| Jan Kroesen | *voorziter*
| Otto Kroesen | *secretaris*
| Jürgen Müller | *penningmeester*

Over de [geschiedenis van Respondeo]({{ '/geschiedenis-respondeo' | relative_url }})

Andere initiatieven:

Rosenstock-Huessy [Gesellschaft](http://www.rosenstock-huessy.com/) (Duitsland)\
Eugen Rosenstock-Huessy [Fund](http://erhfund.org) (Verenigde Staten)\
Otto Kroesen ([Temporae-vita](https://temporavitae.nl): hoe houden mensen de maatschappij in stand)\
Het [Rosenstock-Huessy Huis](https://www.erhg.net/rosenstock-huessy-huis-nl/) in Haarlem
