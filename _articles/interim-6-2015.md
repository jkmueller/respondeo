---
title: "Interim 6 – juli 2015"
thema: "De taal is wijzer dan haar spreker"
category: interim
created: 2015-07
order: 1
---

### {{ page.thema }}

#### Lees- en leerervaringen met de werken van Rosenstock-Huessy

* Inleiding – Wilmy Verhage
* In Memoriam Wim Leenman (6 mei 1928 – 21 juni 2015) – Feico Houweling
* Bij het afscheid van mijn vader – Peter Leenman
* Ervaringen uit het Rosenstock-Huessy Huis – Lien Leenman-De Pijper
* Hoe lees je Rosenstock? \
  Een impressie van de voorjaarsbijeenkomst 2015 – Egbert Schroten
* We kunnen/ hoeven niet alles te lezen van Rosenstock-Huessy – Feico Houweling
* Je kunt het beste in een groep lezen – Elly Hartman
* Er moet ook een herkenning met het dagelijks leven zijn – Agnes Lieverse-Opdam
* Hoe lees ik Rosenstock? – Egbert Schroten
* Rosenstock-Huessy lezen is barrières nemen – Jan Kroesen
* Inlezen via de introducties – Wilmy Verhage
* Verslag van het bestuur over het jaar 2014 – Feico Houweling, voorzitter tot en met 7 mei 2015
* Uit het bestuur – Otto Kroesen, voorzitter vanaf 7 mei 2015
* Achterpagina

[Interim 6-2015]({{ 'assets/downloads/interim_6_2015.pdf' | relative_url }})
