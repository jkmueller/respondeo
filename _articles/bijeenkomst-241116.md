---
title: Bijeenkomst 16 november 2024
thema: "Waarom Nietzsche maskers moest dragen en waarom het Kruis der Werkelijkheid geen model is…"
category: bijeenkomst
created: 2024-09-01
published: 2024-09-01
order: 1
landingpage: 
order-lp: 3
---

![Ichtuskerk Colmschate]({{ 'assets/images/ichtuskerk_colmschate.jpeg' | relative_url }}){:.img-right.img-small}

De vereniging Respondeo nodigt je van harte uit voor een bijeenkomst op 11 november 2024 de Ichthuskerk, Holterweg 106, 7429 AH Colmschate-Schalkhaar. Thema:

### {{ page.thema }}
<!--more-->
De westerse wetenschap maakt altijd modellen. Die staan niet gelijk met de werkelijkheid, maar zijn bedoeld als afspiegeling ervan. Zo is het in de theologie en ook in de natuurwetenschappen. In de theologie is daar wel het woord “zoekmodel” voor uitgevonden. Dat heeft op zich al een nihilistische trek in zich. Het model dat jij bouwt komt in de plaats van de werkelijkheid. Een mens houdt zich graag ergens aan vast, maar het ballonnetje waar jij je aan vasthoudt geeft geen houvast, want jij zelf houdt het vast.

Daarom wil Nietzsche het anders doen. Naar zijn voorbeeld wil Rosenstock-Huessy het ook anders doen. Het gaat er om je ervaringen te beluisteren en te verwoorden wat je hoort. Maar dat wordt altijd verkeerd begrepen, bij beiden.

Het Kruis der Werkelijkheid van Rosenstock-Huessy is een ervaren werkelijkheid maar de oppervlakkige lezer maakt er snel een model van. Je kunt als westers filosoof en theoloog haast niet nalaten hem zo te lezen. Zozeer zijn wij modellenbouwers.

Wij lezen de komende bijeenkomst uit een tekst van Rosenstock-Huessy over de maskers van Nietzsche. Hij kon niet direct tot de mensen spreken maar moest dat via bijvoorbeeld Zaratoestra doen omdat anders zijn kritische wijsheid begrepen zou worden als weer een nieuw model. Zo wordt eveneens het kruis der werkelijkheid met zijn afwisseling van imperatief, conjunctief, participatief en indicatief misverstaan als een nieuw model. De filosofie moet leren luisteren!

Voordat je redeneert is er al iets met je gebeurd en dat moet je horen en verwoorden. Zo wil Nietzsche filosofie bedrijven. Stiekem neemt hij dan de traditionele taak van de theologie over. Want die moest toch eerst luisteren?! Maar ook de theologie heeft er een gewoonte van gemaakt een intellectueel beeld te maken van de Levende God.

***Lees mee en overweeg mee… want dat is wat we gaan doen op 16 november.***

De tekst van Rosenstock-Huessy over Nietzsche is in het Nederlands vertaald en te vinden op de website: [De functie van Friedrich Nietzsche in de kerk en de crisis van theologie en filosofie]({{ '/text-erh-functie-van-friedrich-nietzsche-in-de-kerk' | relative_url }})

Het programma:
- 10.30 Ontvangst en koffie
- 11.00 Opening, samen lezen onder leiding van Jan Kroesen
- 12.30 Lunch
- 13:30 Samen verder lezen onder leiding van Jan Kroesen
- 15.00 Koffie en thee
- 15.30 Sluiting

Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo.
Otto Kroesen, Leeuweriklaan 3, 2623 RB, Delft, [email](mailto:info@rosenstock-huessy.com)


De Ichthuskerk is met bus bereikbaar vanuit Deventer, maar een van de bestuursleden kan je ook altijd even bij het station in Deventer ophalen, als je contact opneemt.

zie ook: [Waarom Nietzsche maskers moest dragen: Aantekeningen van Rudolf Kooiman]({{ '/rk-aantekeningen-rosenstock-en-nietzsche2' | relative_url }})
