---
title: "Bas Leenman: Als God stukloopt"
category: boek
order: 1
---
![Als God stukloopt]({{ 'assets/images/bas-leenman-als-god-stukloopt.jpg' | relative_url }}){:.img-left.img-small}
### TEKSTEN VAN BAS LEENMAN (1920-2006)

We ervaren dat de tijd van allround-mensen voorbij is. Er komen steeds meer stuk-mensen. Ze ervaren zich ook als stuk. En toch ligt in de imperfectie ons heil. De imperfectie dwingt ons tot het aanvaarden van de naaste als het ontbrekende deel van ons leven. Wie zichzelf niet als stuk heeft ervaren wordt nooit een mens die op zijn meervoud wacht. Jezus heeft als stuk-mens geleefd en omdat hij de aanvullende delen van zijn leven zelfs nog bij de allerverste geslachten heeft gezocht, is hij het leven.

Het boek bevat verrassend actuele teksten van Bas Leenman († 2006), leerling van K.H. Miskotte en Eugen Rosenstock-Huessy. Aangevuld met beschouwingen van Otto Kroesen, Wilmy Verhage en Rinse Reeling Brouwer.

Te bestellen bij de uitgever
 [Skandalon](http://www.skandalon.nl/shop/theologie-cultuur/371-de-taal-van-de-ziel.html). De vereniging Respondeo heeft de uitgave financieel mogelijk gemaakt.

Waarom is dit boek belangrijk? Lees [De metamorfose van het woord van God in de taal van de mens]({{ 'assets/downloads/2016_5maartvoordrachtOtto.pdf' | relative_url }}), van Otto Kroesen bij de presentatie van dit boek, op 5 maart 2016.
