---
title: "Interim 2 – zomer 2013"
thema: De Derde Levensfase en de Oudere Werknemer
category: interim
created: 2013-07
order: 1
---

### {{ page.thema }}

* Introductie – Wilmy Verhage
* Kleine overdenking als bijdrage aan de vraag: Hoe zou de 3e levensfase in verschillende beroepen tot uiting kunnen komen? – Marlouk Alders
* Mijmeringen over de Ziel en haar Geest. – Jan Kroesen
* ‘Je draagt je droom uit en dan gebeurt het’ Wouke Lam, ex-manager Shell – Feico Houweling, Wouke Lam
* ‘Bedrijven hebben ouderen nodig’ Otto Kroesen over levensfasen en carrière – Feico Houweling, Otto Kroesen
* Toespraak van Greet Oosterbroek bij de begrafenisdienst van Teuntje Leenman – Greet Oosterbroek
* Ichthus – leven, leer, werken – Otto Kroesen
* Vanuit het bestuur van de Vereniging – Egbert Schroten
* Vereniging Respondeo  
* Over Interim

[Interim 2-2013]({{ 'assets/downloads/interim_2_2013.pdf' | relative_url }})
