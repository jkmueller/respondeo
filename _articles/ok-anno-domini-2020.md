---
title: "Otto Kroesen: Anno Domini 2020"
category: essay
created: 2020-02-03
published: 2020-02-03
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}
### Klimaatoplossingen vanuit een onverwachte hoek

Iedereen die vanuit het punt waar we nu staan probeert vooruit te blikken, loopt op tegen de kwestie van het klimaat. Hebben juist wereldwijd zo ongeveer alle nationale staten de westerse erfenis van techniek en welvaart overgenomen (of daar net mee bezig), moeten zij met de westerse wereld deze erfenis overwinnen en opnieuw door de bocht. Dat is een enorme verandering. En …

**… wie verandering zegt,\
 zegt Rosenstock-Huessy …**

van wie het adagium luidt “***Respondeo, etsi mutabor***”: *Ik antwoord ondanks dat ik daardoor een verandering onderga*.

Telkens opnieuw moet er een antwoord komen op een nieuwe imperatief, die zich vanuit de toekomst opdringt, inderdaad, in de gebiedende wijs. We moeten of we willen of niet. Welke religieuze traditie had ook weer de toekomst in het centrum staan? Dat was de stem van Israël, het Jodendom. Hetgeen betekent, dat alleen diegene de stem van Israël serieus neemt, die antwoordt op de uitdaging van het klimaat. De ware God daagt ons altijd uit vanuit een onverwachte hoek.

Nog een Rosenstock inzicht is hier relevant: wij zijn vergroeid met de natuur die wij beheersen. Wij gebruiken de producten van de natuur maar worden daardoor zelf natuurproducten, die zich aanpassen aan het mechanische ritme van de productie, ter wille van overvloedige consumptie: welvaart. Met andere woorden:

**Het dagelijkse ritme van productie en consumptie** *(plus nieuws en evenementen)* **beheerst ons innerlijke leven. Rosenstock-Huessy noemt dat proletarisering, want het hogere doel ontbreekt eraan.**

Zijn voorstel voor een oplossing: wij moeten groter worden dan wie wij zijn. Dat geschiedt aan ons als wij niet alleen met de stroom mee gaan, maar waar het nodig is ook tegen de stroom ingaan en staan voor het in ernst gesproken woord. Het geschiedt ook als wij onze plaats innemen in de keten van het heil door de verworvenheden van heel het menselijk geslacht door de geschiedenis heen in ere te houden en te belichamen. Dat gaat natuurlijk nog wel wat verder dan een Boeddhakop kopen en in de tuin zetten. Wie luistert naar de stemmen van het voorgeslacht, daar waar mensen telkens een gewaagde stap gezet hebben, breidt het eigen handelingsrepertoire uit. En wie geestelijk meer heeft kan materieel met minder toe.
