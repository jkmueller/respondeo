---
title: Nieuwsbrief oktober 2021
category: nieuwsbrief
created: 2021-10
order: 1
---
Veel nieuws heeft deze Nieuwsbrief misschien niet. Want iedereen weet al dat de volgende ledenvergadering op 30 oktober zal zijn. Ook het thema is al bekend. Het ligt in het verlengde van eerdere besprekingen en sluit aan bij de actualiteit. Het gaat over identiteit en uitsluiting en wil dat thema bespreekbaar maken tegen de achtergrond van de inbreng van Rosenstock- Huessy. Om dat thema een naam te geven gebruiken we de volgende titel voor de bijeenkomst op

**30 oktober 2021:**

### Identiteit in de wij-het-samenleving

Plaats: gebouw “Irene”, Dorpsstraat 8, Twello,

[nieuwsbrief 21-10]({{ 'assets/downloads/2021_oktober_digitale_nieuwsbrief_Respondeo.pdf' | relative_url }})
