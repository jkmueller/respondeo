---
title: "Otto Kroesen: Het Amerika van Trump in historisch perspectief"
category: essay
created: 2020-01-03
published: 2020-01-03
landingpage:
order-lp: 4
---

Recent was in het nieuws dat Trump wel graag Groenland wil kopen van Denemarken. Kopen… Dat is het waarschijnlijk niet laatste uitvloeisel van de ‘Amerika first’ politiek van deze Republikeinse president. Het lijkt lachwekkend,
<!--more-->
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}

maar wie naar de geschiedenis van Amerika kijkt ziet meer van dat soort aankopen. Bekend is de Louisiana Purchase van 1803 toen Amerika het land ten westen van de Mississippi kocht van Frankrijk. Later is ook nog Alaska gekocht van Rusland, in 1867. Dus waarom niet ook Groenland? Die paar mensen die op dat ijs wonen…

Toch is er een groot verschil in betekenis. Bij de Louisiana Purchase was Amerika nog in opbouw, nog lang geen wereldmacht. De aankoop van Alaska was meer om Rusland te ontlasten. Amerika had er ook zonder gekund. Het idee om ook Groenland te kopen, daarentegen, past in de tegenwoordige politiek die gericht is op grondstoffen, banen, en het handhaven van de levenstandaard van de kiezers. Dat is de manier tegenwoordig voor bijna elke politieke leider om in het zadel te blijven. Imperialisme? Bij imperialisme denken we aan de wil om te overheersen. Het is een scheldwoord geworden. Maar Rosenstock-Huessy meent dat we beter kunnen denken aan een ander verschijnsel, namelijk Empire Building. Oftewel een Rijkscultuur opbouwen zoals ook in het Egypte van de farao’s geschiedde evenals in andere Rijken zoals dat van de Assyriërs, de Babyloniërs, de Perzen, China, Rome. Kenmerk van die Rijken is het afbakenen van het gebied en het onderwerpen van de andere Rijken en Stammen aan de behoefte om de eigen bevolking te voeden en tevreden te houden. Imperialistisch dus, maar voorop stond altijd de interne vrede. Nu herleven dit soort Rijksculturen in het Amerika van Trump, maar niet alleen daar. De prioriteiten van het huidige China, Rusland, Brazilië, of de Europese Unie zijn niet heel verschillend. Het gaat om een wereldwijde ontwikkeling. Opvallend is het wel dat de 20e eeuw juist een einde heeft gemaakt aan alle keizerrijken die er tot dan toe nog waren: van Ethiopië tot Japan, van Duitsland tot China. En keert dat nu weer terug?

**De Rijkscultuur moet terugkeren!**

**Maar het moet,** ***zoals alles wat terugkeert,*** **gezuiverd terugkeren.**

Aan de geschiedenis van Amerika laat Rosenstock-Huessy in zijn lessen uit 1954 (onder de titel Universal History te vinden op de Amerikaanse website https://www.erhfund.org/) zien dat ook Amerika vanaf het tijdperk van de kerk terug leeft naar het tijdperk van Israël, van Griekenland en dat in de 21e eeuw (maar dat begon reeds in de 20e eeuw) Egypte aan de beurt is. De presidenten van Amerika van voor de Eerste Wereldoorlog zouden zich nooit bekommerd hebben om de broodvraag en om de economie. Daar ging de politiek niet over.

**De geschiedenis van Amerika begint met de puriteinse gemeenten die zich juist willen onttrekken aan het Europese imperialisme.** Zij wilden een zuiver leven leiden als christelijke gemeenten die alles met elkaar delen. Dat is een terugkeer van de kerk van de eerste eeuw na Christus. Maar voor deze gemeenten was ook het volk Israël het ideaal: een leven van gerechtigheid als volk dat geen overheid boven zich (nodig) heeft, maar waarin God de leiding heeft, de God van het verbond met het volk dat hem volgt. Dat ideaal is Amerika blijven begeleiden min of meer tot aan de burgeroorlog in de 19e eeuw. Daarna, maar ook al daarnaast was er een ander ideaal, waarvan de architectuur van het Capitool en zoveel andere regeringsgebouwen getuigt: de Griekse democratie te doen herleven. Net als de Grieken voerden de Amerikanen handel over heel de wereld, maar net als bij de Grieken hield de politiek zich daarvan afzijdig. De politici van de Griekse stadstaten gingen over oorlog en vrede en over de rechten van slaven en lijfeigenen, maar niet over de broodvraag.

**Met vier fasen in zijn geschiedenis heeft Amerika vooruit geleefd de toekomst in door tegelijkertijd de blik te richten op een steeds dieper verleden als inspiratiebron.**

Europa heeft iets soortgelijks gedaan. Drie fasen hebben we al genoemd: de christelijke gemeente, het oudtestamentische volk, de Griekse democratie. Wie nog verder terug gaat komt uit bij de tijd van de Rijken en Stammen. In een rijkscultuur staat het voeden van de massa’s centraal. In een stamcultuur staat de beleving van de eigen identiteit centraal. Wij maken een terugkeer mee van die beide grootheden.

Is die vierde fase onvermijdelijk? Ja volgens Rosenstock-Huessy wel – alle verworvenheden van het verleden keren terug.

**Maar opstanding is niet hetzelfde als reïncarnatie. Wat terugkeert is niet een kopie.**

Dat moet juist vermeden worden. Eigenlijk is de wereldeconomie al één geworden. Het ideaal dat het Egyptische rijk oorspronkelijk dacht te kunnen realiseren was precies dat: de hele wereld omvatten en in een gebied te verenigen. Zij konden ook niet weten hoe groot de wereld was en een 1500 km lange Nijl was al heel wat. En om niet aan uitbuiting en onderdrukking te doen moeten de huidige wereldleiders (geen godenzonen!) hun imperia beschouwen als deel van die nu wereldomvattende eenheid. Slechts als een aandeel. Voorgaande Rijksculturen streefden ernaar wereldomvattend te worden. Nu moeten ze. Maar zonder dat een enkele politieke macht zich daarvan het middelpunt kan maken. Dat gaat niet meer. Ook Trump kan zijn ultieme troefkaart, de bom, niet gooien en hij weet dat. De anderen ook.

Wereldleiders die zichzelf toch het middelpunt willen maken komen in de verleiding de eigen identiteit te vereeuwigen, de eigen stam te cultiveren. Stammen kunnen niet zonder vijandbeelden. En omdat het inmiddels onmogelijk is dat er nog stammen buiten de muren van het rijk leven *(dat kon nog bij de Romeinen en bij China)* richten verschillende groepen in de ene wereldomvattende samenleving zich tegen elkaar.

**Terrorisme heet dat. Of iets onschuldiger discriminatie en nationalisme.**

Ooit heeft de christelijke gemeente instaan-voor-elkaar (plaatsbekleding) in plaats van eigen identiteit gesteld en stamhoofden en godenzonen afgeschaft door de gekruisigde Christus in het centrum te zetten, die voor-de-ander leefde/stierf. Door te sterven voor de ander of daartoe bereid te zijn werd een deur geopend naar onderlinge zorg en samenwerking tussen volken en stammen en rijken en gewone mensen, een deur die tot dan toe gesloten bleef. Vanuit dat feit heeft het herleven van al die andere fasen ook zijn zin. Ze moeten herleven, maar gedecentreerd om zo te zeggen. Ze hebben hun centrum niet meer in zichzelf. Het Rijk is wereldomvattend geworden, maar wordt niet meer bestuurd vanuit een centrum. Althans, dat moet voorkomen worden. En stammen, dat wil zeggen in zich gesloten groepen van grote saamhorigheid – ook die moeten terugkeren. Maar zoals het Rijk niet alle ruimte mag innemen, zo mag de Stam niet meer alle tijd opeisen. Dat wil zeggen: hechte wij-groepen kunnen zich alleen maar een tijdelijk bestaan veroorloven. Hun groepidentiteit mag er zijn, maar alleen tijdelijk, alleen als bijdrage aan het geheel.

Dat moet zo, want anders wordt de plaatsbekleding van de een voor-de-ander ongedaan gemaakt. Dan zou de deur naar de A/ander die door Christus geopend is weer gesloten worden. Dan leven we wereldwijd opnieuw in compartimenten. Dat zou het einde betekenen van het christelijke tijdperk.

Maar juist het christelijke tijdperk heeft zoveel deuren tussen mensen onderling geopend dat een terugkeer naar het voor-christelijke tijdperk van gesloten compartimenten niet meer mogelijk is. De techniek, vrucht van het christelijk tijdperk, maakt dat wij in een wereldwijde ruimte leven, als buren van elkaar. Zouden we erin slagen terug te gaan naar voorchristelijke tijden, dan zou het geweld dat wij elkaar aandoen en kunnen aandoen vele malen groter zijn dan het destijds ooit was.

**Het einde van het christelijke tijdperk kan daarom alleen maar de vorm aannemen van** ***wederzijdse vernietiging***. **Dus, we moeten wel verder. Of we willen of niet. We kunnen niet terug.**
