---
title: Bijeenkomst 11 november 2011
thema: "Moslims in de westerse samenleving"
category: bijeenkomst
created: 2011-11-05
order: 1
---
### {{ page.thema }}

Zaterdag 5 november 2011 van 11 tot 4 uur

In de Johanneskerk, Westsingel 30, Amersfoort

een open bijeenkomst\
Iedereen is welkom.
