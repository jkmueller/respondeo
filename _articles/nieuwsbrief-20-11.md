---
title: Respondeo podcasts nu te beluisteren
category: nieuwsbrief
created: 2020-11
order: 1
---
Beste leden en belangstellenden,

met trots presenteren wij een viertal podcasts waarin een aantal van onze leden te horen zijn over Rosenstock:
Ze laten de echo van het werk van Rosenstock-Huessy in de levensverhalen van de leden van Respondeo zien. De podcasts zijn gemaakt door Kurt Kooiman en Freek Schröder in opdracht van Vereniging Respondeo.

Ze zijn te beluisteren op onze [website]({{ '/ok-respondeo-podcasts' | relative_url }}).


--
met vriendelijke groet,

Egbert Schroten, secretaris Vereniging Respondeo
