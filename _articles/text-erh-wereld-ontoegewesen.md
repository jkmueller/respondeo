---
title: Ontoegewezen Wereld
category: online-text
---
![Uncentered World]({{ 'assets/images/bipolar_world.jpg' | relative_url }}){:.img-right.img-xlarge}
![Uncentered World]({{ 'assets/images/bipolar_world2.jpg' | relative_url }}){:.img-left.img-xlarge}

Achterschutblad Kaart, De Wereld Aan Niemand Toegewezen: Geen Natie of Continent ligt in het centrum. De kaart wordt twee keer gegeven. Een bipolaire, transversale, elliptische, gelijke-oppervlakte kaart. Net als een Mercator-projectie vervormt deze kaart vormen; in tegenstelling tot een Mercator-kaart geeft zij gebieden exact weer en toont zij alle mogelijke verbindingen over de polen heen. De eerste kaart van dit type werd, wellicht met een weinig indrukwekkende techniek, getekend door Sir C.F. Close in Great Britain's Ord-nance Survey, Professional Papers, New Series, volume II, 1927. Dit lijkt te zijn weggelaten in de overigens uitputtende studie van C. H. Deetz en Oscar S. Adams, Elements of Map Projection, U. S. Department of Commerce, Coast and Geodetic Survey, Spe-cial Publication No. 68 (Vierde herziene editie, 2 april 1934). Zie ook C. H. Deetz, Cartography (Special Publication No. 205), blz. 50 e.v., Washington, 1936, en dit boek, blz. 465, 604 e.v., 630, 715, 717, 729.

Eugen Rosenstock-Huessy: Out of Revolution, 1938
