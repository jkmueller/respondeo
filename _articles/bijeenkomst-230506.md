---
title: Bijeenkomst 6 mei 2023
thema: "In de ban van het schisma (jaar 1054)"
category: bijeenkomst
created: 2023-01-21
published: 2023-01-21
order: 1
---

![Het Kantoor]({{ 'assets/images/het_kantoor_deventer.jpg' | relative_url }}){:.img-right.img-small}

De vereniging Respondeo nodigt je van harte uit voor een bijeenkomst op 6 mei 2023 in gebouw “Het Kantoor” Brink 91, 7411 BZ Deventer, met als thema:

### {{ page.thema }}
<!--more-->
We lezen en bespreken het hoofdstuk: "Zegenrijke schuld of terugblik op de kerk" van [Toekomst - Het Christelijk Levensgeheim](https://www.erhfund.org/wp-content/uploads/606.pdf), bladzijde 163-194.


Het programma:

| 10.30 | Ontvangst en koffie
| 11.00 | Opening, samen lezen onder leiding van Jan Kroesen
| 12.30 | Lunch
| 14:30 | Ledenvergadering
|| 1. Opening
|| 2. Verslag vorige jaarvergadering
|| 3. Jaarverslag (2022)
|| 4. Financiële jaarverslagen (2021 en 2022)
|| 6. Rondvraag
|| 7. Sluiting
| 15.30 | Sluiting


Graag vooraf opgeven in verband met de lunch bij Secretariaat Respondeo:\
Otto Kroesen, Leeuweriklaan 3, 2623 RB Delft.
