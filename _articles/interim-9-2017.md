---
title: "Interim 9 – maart 2017"
thema: "Inburgeren"
category: interim
created: 2017-03
order: 1
---

### {{ page.thema }}


* Inleiding – Jan Kroesen
* Een verhaal van een vluchteling – Samir Maglajic
* Verhalen van inburgering – Anneke Kroesen-Bos
* Burgerschap, Integratie in de Nederlandse samenleving, bewoners en bijkomers – Otto Kroesen
* Uit het bestuur – Otto Kroesen
* Achterpagina

[Interim 9-2017]({{ 'assets/downloads/interim_9_2017.pdf' | relative_url }})
