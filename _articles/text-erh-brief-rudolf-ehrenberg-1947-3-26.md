---
title: "Rosenstock-Huessy: Brief aan Rudolf Ehrenberg (26-3-1947)"
category: online-text
published: 2022-10-23
org-publ: 1947
---

Beste Rudi,

Het was fijn om weer een innerlijke ruimte van gesprek te betreden dankzij jouw brieven. Hopelijk blijven onze pakjes aankomen. Het sneeuwt vandaag - het is hier winter tot half mei - en we bevriezen bij de gedachte aan Europa. Maar misschien is het bij jullie al lente.

Maar om het vliegtuigporto te concentreren op het centrale:
Natuurlijk heb ik met plezier je lofzang op Christian Future 1 gelezen[^1]. Toch is het meer het witte kanten kraagje van mijn verleden, plichtmatig aangetrokken voor een Amerikaans publiek. Het ontstond niet direct op het punt van mijn eigen groei.

Daarom wordt veel meer gezegd en genoemd dan bewezen, en worden ervaringen van strijd als zodanig geboekt, in plaats van inzichten van liefde en geloof. Het boek wordt beschouwd als "belangrijk" en wordt door niemand gelezen - behalve door mijn beste vrienden. Objectief gezien wil ik een misverstand uit de weg ruimen. Het is heel belangrijk voor mij! Het kruis der werkelijkheid is geen schema. Het is een ontdekking, gedaan in 1924, en naar Beatenberg opgeschreven in mijn beste boek, "De krachten der Gemeenschap" („Die Kräfte der Gemeinschaft”) (dat in Duitsland heruitgegeven zou moeten worden)[^2]. A. Meyer paste het vervolgens toe op de biologie, zoals ik het in het laatste hoofdstuk op Kant heb toegepast. Lees dit alsjeblieft. Het is gebaseerd op de gedenkwaardige ontdekking dat tijd en ruimte door ons in omgekeerde richting worden ervaren en dat de populaire parallel "tijd en ruimte" onhoudbaar is.

Want ruimte wordt als universum, tijd als moment empirisch aangeraakt, en we moeten innerlijke ruimtes toevoegen aan het universum, tijdruimtes aan het moment, voordat deze eerste stap - hier in de tijd, daar in de ruimte - ervaring kan worden. Elk binnen is kleiner, elke periode is groter dan het herinneren zou voordoen! Huizen en tijden zijn de menselijke handelingen die het paniekerige moment en de paniekerige wereld tot ervaarbare feiten maken! "De vooruitgang van de wetenschap" is een geschapen tijdruimte, "de aarde" een geschapen onderverdeling van de ruimte.

Daarom komt het nu, ten tweede, dat ruimte en tijd alleen gegeven zijn in de verdubbeling van binnen en buiten, verleden en toekomst. Wij ontstaan tussen hen in. Dit is geen schema, maar geldt voor elke historische groep.

Ten derde verandert deze dubbele polariteit van het kruis de begrippen van binnen en buiten, verleden en toekomst van naar de groep toestralende naar van de groep uitstralende. Verleden wordt achteruit, toekomst wordt vooruit, binnen wordt naar binnen, buiten wordt naar buiten. Dit is revolutionair. Zie het laatste hoofdstuk van Sociologie hierover! Omdat er nu geen romantiek of utopie ontstaat!

Ten vierde wordt nu een diagnose van zieke en gezonde groepen mogelijk. Ik heb zojuist een Brit in Bonn hierover geschreven. Als je geïnteresseerd bent, stuur ik je het transcript. Elke groep vertoont kruismismaaktheden! En ze genezen elkaar.

Ten vijfde dient de taal als reden voor historische tijd- en ruimtecoördinaten. Namen zijn gewoon dit. Het gedrag van namen en voornaamwoord, van hoge taal en dialect, toont de voortdurende spanning. Ik heb zojuist een aantal prachtige ontdekkingen gedaan over het feit dat de stammen de naamruimte hebben gevormd, terwijl de Egyptenaren de voornaamwoordelijke huisruimte hebben gevormd.

Dit alles komt tot uiting in de eerste wetenschappelijke grammatica die nu mogelijk is. Eindelijk kan de grammatica worden gebaseerd op empirisch waarneembare zaken. "Jij", "ik", "wij", "het" zijn de vier richtingen naar voren, naar binnen, naar achteren, naar buiten (preject, subject, traject, object). Dit is geen schema, maar de wet van het spreken, zoals mijn "Woord van de mensheid" („Wort des Menschengeschlechts”) uitlegt. - Hierbij geef ik je overigens graag toe dat Israël en de Kerk machtig zijn in het spreken, Laozi en Boeddha zijn machteloos in het spreken. Maar het kruis is geen schema; zoals elke werkhypothese ontdekt het dit: historische groepen en hun toestand. Daarom verontrust onze ontrouw aan Patmos mij. Ik heb zojuist de correctie gelezen van mijn essay over het schepsel (Chicago 1947)[^3]. De conprocessio van de geest in verschillende talen (Goethe - Schiller, Nietzsche - Dostojevski) is het probleem van de tijd. Het hoofdstuk over Laozi e.d. is daar - gebrekkig - voor bedoeld.

Je oude Eugen.

[^1]: Eugen Rosenstock-Huessy: The Christian Future or Modern Mind Outrun. New York 1946 (deutsche Ausgabe nunmehr: Moers 1985)
[^2]: Eugen Rosenstock: Soziologie I. Die Kräfte der Gemeinschaft. Berlin und Leipzig 1925
[^3]: Zie nu E. Rosenstock-Huessy: Ja und Nein. Heidelberg 1968, S 107 ff.

Dit is een vertaling van een [brief geschreven in het Duits](https://www.rosenstock-huessy.com/text-erh-brief-rudolf-ehrenberg-1947-3-26/).

[naar het begin van deze pagina](#top)
