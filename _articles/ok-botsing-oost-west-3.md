---
title: "Otto Kroesen: De botsing tussen Oost en West in Europa (3)"
category: reactie
essay-nr: 170
created: 2023-06-12
published: 2023-06-25
---

#### Verwestersing van Rusland en russificatie van het Westen

In Rusland heeft zich niet een civiele maatschappij ontwikkeld met gilden en steden en organisaties van onderop. Vanaf tsaar Peter de Grote is geprobeerd Rusland te moderniseren. Gedurende heel de 19e eeuw was Frankrijk met zijn burgerlijke revolutie en rationalisme en democratisch bestuur als nationale staat het model. In deze derde bijdrage vestigen we de aandacht op het geprivilegieerde moment van de wending van de 19e naar de 20e eeuw, met als hoogtepunt de Russische Revolutie van 1917 die in Rusland het wetenschappelijk socialisme invoert met totaalplanning van staatswege.

#### De Russische Revolutie en het wetenschappelijk socialisme

In 1905 vindt in Rusland een burgerlijke revolutie plaats. Maar die is eigenlijk slechts de voorloper van de uitbarsting van 1917, toen midden in de eerste wereldoorlog de radicale communisten, de bolsjewieken, onder leiding van Lenin de macht grepen. Lenin en de zijnen waren trots dat hun land, de toekomstige Sovjet-Unie, als eerste de socialistische samenleving ging realiseren. Zonder de radicalisering van de soldaten/boerenzonen aan het front in de strijd van de eerste wereldoorlog zou dit overigens niet gelukt zijn.

De communistische partij in Rusland vestigde een straf regime met staats-eigendom van de productiemiddelen en totaalplanning van de productie, waarbij radicale maatregelen als verplaatsing van bevolkingen, vervolging van de burgerlijke middenklasse, strafkampen voor dissidenten niet geschuwd werden. Alles draaide om rationele vervulling van de maatschappelijke behoeften en dat onder staatscontrole.

#### Taalfilosofie in Rusland

Niet zo bekend zijn in het Westen de namen van een aantal Russische filosofen die niet vanuit het communisme maar vanuit de Russische orthodoxie geïnspireerd waren. Daarbij komen namen naar voren als Solovjev, Bulgakov, Berdjajew, Florenski [^1]. Ook zij waren ervan overtuigd dat Rusland westerse instituties en waarden moest overnemen, maar daar moest naar hun overtuiging de Russisch orthodoxe inspiratie de leiding bij hebben. De namen van de auteurs Dostojevski en Tolstoy zijn in het Westen veel bekender geworden. Ook zij wilden de westerse erfenis overnemen en tegelijkertijd overtroeven, want zij konden niets aanvangen met de heersende westerse filosofie van het utilisme en rationalisme.

Opmerkelijk is dat deze Russische denkers de schatten van de Russische orthodoxie hebben willen blootleggen door een nieuwe bezinning op de taal. Mensen mogen gebrekkig zijn, bang, soms kwaadaardig, maar als zij elkaar aanspreken en voor elkaar opengaan, gebeurt er iets. Dan komen ze elkaar te hulp en raken als vrienden met elkaar verweven. Zo verweven als de broeders in de Russische kloosters, die alles met elkaar delen en broederschap en vriendschap beoefenen [^2]. Dat is de oplossing van de maatschappelijke problematiek die zij voor ogen hebben.

Door de taalfilosofie van appel en antwoord stroomt het liefdesoverschot van de Russische ziel de maatschappij in. De taal zelf heeft aldus Florenski een trinitarische structuur[^3]. God is eigenlijk de enige die Ik kan zeggen. Het Jij, het tegenover van God is de Zoon, die in Christus openbaar wordt, maar die ons allen insluit als aangesprokenen. Doordat wij in de Vader die aan ons appelleert (Ik zegt) en de Zoon (aangesproken als Jij) zijn ingesloten vormt de Geest ons om tot een Wij, een gemeenschap van mensen die voor elkaar instaan en samenwerken om het Rijk te realiseren. Zo ontstaat het heil en niet door utilistische of rationalistische redeneringen. Die zijn eigenlijk alleen maar de seculiere voortzetting van de regelgeving vanaf de middeleeuwen door de katholieke kerk.

#### De crisis van de nationale staat/staten in de eerste wereldoorlog

Een groep vooral joodse geleerden in Duitsland ziet rond de eerste wereldoorlog eveneens de noodzaak om nationalisme en rationalisme te overwinnen. Duitsland heeft de oorlog verloren en het volk dreigt zonder oriëntatie door te draaien en te verwilderen. Hier helpen redeneringen niet. In appel en antwoord, spreken en luisteren, moeten ook hier mensen voor elkaar opengaan. Dat is de enige weg. Aan deze benadering zijn namen verbonden als Ehrenberg, Rosenzweig, Buber, Rosenstock-Huessy[^4]. Deze taalfilosofie zou met name bruggen moeten slaan tussen werkgevers en arbeiders, in een gezamenlijke verantwoordelijkheid, ondanks al hun tegenstellingen.

Ehrenberg noemt deze herontdekking van de taal de russificatie van het Westen. In de oosterse orthodoxie zijn de schatten van de liefde van Christus en van de toewijding aan elkaar bewaard gebleven in de liturgie en de kloosters. Nu stroomt liefde en respect de maatschappij in en geeft nieuwe inspiratie, waar mensen met hun plannen en redeneringen vastlopen.

#### Waar is dit gebleven – in het Oosten en in het Westen?

Zowel in Rusland als in het Westen is deze nieuwe taal een randverschijnsel gebleven. Het heeft wel enige navolging gekregen maar het heeft de samenleving niet op een ander spoor kunnen zetten. Rusland bleef op het spoor van staatskapitalisme met een toenemende rol voor de geheime diensten. Het Westen bleef in het spoor van het liberalisme, met toenemend consumentisme, zodat veel burgerlijke samenwerking die het erfgoed was van de katholieke middeleeuwen en van de reformatorische nieuwe tijd, is weggesleten. De ergste uitwassen van het ieder voor zich liberalisme werden weliswaar bestreden, maar dan vooral in het Westen zelf. Het Westen heeft er een lange traditie van de verworvenheden die het cultureel kapitaal van de eigen samenleving uitmaken te ontzeggen aan andere samenlevingen [^5].

[^1]: In twee banden, al eerder geciteerd, met de titel Östliches Christentum, verschenen in 1923 en 1924, laat Hans Ehrenberg Russische auteurs, filosofen en theologen aan het woord. Zowel de eerste als de tweede bundel is voorzien van een nawoord van Hans Ehrenberg. Het eerste gaat over de europeïsering van Rusland en het tweede gaat over de russificatie van Europa. Ehrenberg heeft samen gestudeerd met uit Rusland uitgeweken theologen en filosofen, en is op deze manier deelgenoot geworden van hun discussies en wil met deze twee banden onder woorden brengen wat deze twee samenlevingen, van Rusland en West-Europa, elkaar te zeggen hebben (zie ook Hans Ehrenbergs Auseinandersetzung mit dem “Östlichen Christentum” Gedanken eines ökumenischen Visionärs, Nikolaus Thon, in Franz Rosenzweig und Hans Ehrenberg, Bericht einer Beziehung, Haag & Herchen, 1986, pp. 150 – 194). Een dergelijk experiment is voor zover ik weet niet herhaald, afgezien van de dialoog tussen Amerikanen en Russen die geïnitieerd is door Clinton Gardner tijdens de koude oorlog. Maar daarover is weinig opgetekend, zie Clinton C. Gardner, Beyond Belief, White River Press, 2008.

[^2]: Florenski omschrijft die vriendschap en broederschap als zeer open en innig. Hij doet daarbij een pleidooi voor de onrechtvaardige rentmeester uit Lucas 16. Als de onrechtvaardige rentmeester vanwege mismanagement ter verantwoording geroepen wordt door zijn baas, maakt hij vrienden onder de mensen die onder hem staan door hun schulden te verminderen. Men kan dat zien als strategisch gedrag. Maar aldus Florenski, zo moeten mensen zich gedragen in het sociale leven: Bewust van onze eigen zonden, praten wij de schulden van de anderen goed en dekken ze toe, zie An den Wasserscheiden des Denkens, pp. 274 ff. in Östliches Christentum.

[^3]: Florenski, Der Pfeiler und die Grundfeste der Wahrheit, in An den Wasserscheiden des Denkens, editionKONTEXT, 1994, pp.75 ff.

[^4]: In zijn nawoord bij Band II van Östliches Christentum wijst Ehrenberg erop dat de oosterse orthodoxie Johanneïsch van karakter is, want geestelijk op de samenleving inwerkt; dit in tegenstelling tot het katholicisme dat via regels en het protestantisme dat via rationaliteit op de samenleving inwerkt. Het juiste handelen wordt niet juridisch geregeld zoals in het canonieke recht en niet logisch afgeleid zoals in het negentiende-eeuwse protestantisme (denk aan de filosofie van Kant), maar het juiste handelen vindt zijn oriëntatie dankzij een geestelijk appel, een geestelijke macht. Zoals in het evangelie van Johannes de Geest van Christus de richting wijst, zo is de levende taal van de grammaticale methode de macht die het vrije spel van maatschappelijke krachten richting wijst. Zie voor deze interpretatie van Ehrenberg’s Östliches Christentum ook Rudolf Hermeier, Hans Ehrenberg und der Osten, in Jenseits all unsres Wissens wohnt Gott, Brendow Verlag 1986, pp.21 – 44.

[^5]: Winkler, H.A., 2011. Greatness and Limits of the West – The History of an Unfinished Project, LEQS Paper No. 13/2011, https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1762586, (26-4-2021).

[De botsing tussen Oost en West in Europa (4)]({{ 'ok-botsing-oost-west-4' | relative_url }})
