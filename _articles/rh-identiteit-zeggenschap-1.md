---
title: "Rein Hoekstra: Reactie op: Identiteit, zeggenschap en zeggingskracht 1"
category: reactie
essay-nr: 100
created: 2021-09-10
published: 2021-09-10
---
Ontelbaar geduld

In ‘Mijn ontelbare identiteiten’ van Sinan Çankaya komt het misverstaan van een mens schrijnend dichtbij. Sinan Çankaya wordt gevraagd om te spreken op het veertigjarige jubileum van zijn vroegere middelbare school. Dat is natuurlijk bijzonder, maar het roept ook allerlei vergeten herinneringen uit die tijd wakker. Geboren uit Turkse ouders moest hij al jong laveren tussen wortelen en willen vluchten. Waar hoort hij bij, is hij Nederlands, is hij Turks? Maar bij het Turkse dorp van zijn ouders, tijdens de familievakanties, voelt hij geen enkele aansluiting. Afkomstig uit een Nijmeegse krachtwijk zoals dat heet, een sociaal zwakke buurt, is hij als gepromoveerd academicus niet meer één van hen. Op een gegeven moment doet hij onderzoek naar discriminatie bij de politie, maar tijdens een workshop zegt een leidinggevende: ‘Toen ik jou de eerste keer zag lopen in de gangen dacht ik: wat een onguur type!’ De man verwoordt, naar eigen zeggen, zijn onderbuikgevoel.

Het boek van Çankaya gaat over etiketten plakken, iemand wordt zwart of wit gestempeld, geschoold, ongeschoold, guur of onguur. Maar al die oordelen staan ook voor evenzovele misverstanden. Onderbuikgevoelens die ongegeneerd bij de ander worden neergelegd, hebben als uitwerking dat iemand apart komt te staan met alle kwalijke gevolgen van dien.

Het stempelen van mensen, zoals Sinan Çankaya overkwam, doet de gemeenschap ineenschrompelen. Vraagt dit moreel beraad? Of vraagt dit misschien vooral een bredere kijk op dat wat gemeenschap inhoudt? Daarbij moest ik denken aan dat wat Rosenstock-Huessy over Franciscus van Assisi schreef in De grote revoluties: ‘Zijn armoede en zijn bedelaarsprincipe maakte het zijn volgelingen mogelijk een nieuw leven te leiden.’ Oftewel: er is een nieuwe orde nodig. Nieuwheid in de persoonlijke, maar ook in de maatschappelijke verhoudingen. Voor Franciscus van Assisi betekende menszijn, meebewegen in een steeds uitbreidende gemeenschap. De gemeenschap die hij met God ervaart wil hij uitbreiden naar nieuwe, meer omvattende terreinen.

Laat ik een poging doen om dit concreet te maken. Dat wil ik vervolgens toespitsen op de zorg. Hoewel meerdere maatschappelijke terreinen genoemd kunnen worden, is het met de coronacrisis in het achterhoofd duidelijk hoezeer de zorg onder druk staat. Vanwege mijn ernstig gehandicapte zoon heb ik afgelopen maanden opnieuw ontdekt hoe precair daar het evenwicht is. Maar ik begin met een verhaal van Kikker en Pad van Arnold Lobel. Het specifieke verhaal heet ‘De tuin’.

In het verhaal komt Pad bij Kikker en hij ziet daar een prachtige tuin. En Pad wil ook een tuin. Gelukkig krijgt hij wat zaadjes van Kikker. En vol ongeduld gaat hij naar huis. En meteen maken we kennis met het karakter van Pad.

Pad bracht zijn kop heel dicht bij de grond en schreeuwde:\
“Nou zaadjes, ga maar groeien!”\
Kikker kwam het paadje ophollen.\
“Waarom maak je zo’n lawaai?” vroeg hij.\
“Mijn zaad wil niet groeien,” zei Pad.\
“Je schreeuwt ook te veel,” zei Kikker.\
“Die arme zaadjes durven niet eens te groeien.”

Dat lawaai is een interessante. Kortweg gezegd: als een leidinggevende over een ‘onguur type’ spreekt, dan maakt hij veel lawaai. Zoals ten tijde van de coronacrisis veel managers bij instellingen vooral lawaai hebben gemaakt. Dan moet er ten tijde van de crisis opeens met alle geweld protocollen worden geschreven, rapportages worden gemaakt, enzovoort. Een gesprek met betrokkenen staat vaak niet of nauwelijks op het netvlies.

Zorg bestaat voor een belangrijk deel uit het o zo belangrijke evenwicht tussen ouders – verzorgenden – en bewoner. Met het oog op Kikker en Pad, zijn drie punten van belang zijn tussen zorgverleners, verzorgenden, ouders en bewoners:
* Zorgverleners, managers, betrokkenen en bewoners hebben elkaar nodig. Anders vervalt eén van de betrokkenen, voordat je het weet, tot geschreeuw.
* Zorg vraagt (getuige de zaadjes van Kikker die tijd nodig hebben) om geduld. Geduld is meer dan efficiëntie en het beheersbaar maken van een probleem.
* Zorg vraagt niet in de laatste plaats om gepaste zorg. En wat gepast is, wijst niet naar wiskundige formules. Dat wat gepast is wordt kenbaar vanuit het gezamenlijke gesprek. (Denk aan dat gesprekje van Kikker en Pad.)

In het verhaal viel Pad uiteindelijk in slaap. Maar toen Kikker hem wakker maakte, je raadt het al, toen eindelijk waren de zaadjes niet meer bang, ze stonden op een prachtige manier boven de aarde. Otto Kroesen stelt vanuit Rosenstock dat verantwoordelijkheid en visie weer teruggebracht moeten worden op de werkvloer. Met Kikker en Pad in mijn achterhoofd denk aan de concrete taak om de betrokkenen binnen een maatschappelijk veld te benoemen. Wie praten ermee? Vervolgens lijkt het mij van groot gewicht om zoiets als ‘geduld’ handen en voeten te geven. Waar bestaat geduld uit? In Dienen op de planeet zegt Rosenstock het zo: ‘Nu het gaat om een samenwerkingsverband van alle werkende krachten, zal opnieuw een schare, een groep mensen opgekweekt moeten worden, mensen die bepalend zijn voor de kunst geduldig, langzaam, onopvallend, en zonder er beter van te worden, bezig te zijn.’

Rein Hoekstra, Dordrecht
