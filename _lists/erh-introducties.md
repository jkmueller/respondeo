---
title: Introducties
menu: Introducties
column: 1
row: 2
used-categories: intro
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-right.img-large}

### In het Nederlands

#### De persoon Eugen Rosenstock-Huessy

[Korte overzicht](https://www.erhg.net/eugen-rosenstock-huessy-nl/)

[Auto-biografische notitie](https://www.erhg.net/erh-autobiografische-notitie/)

<br/>
#### Thema's van Rosenstock-Huessy

[Otto Kroesen: Eugen Rosenstock-Huessy – Gebieden van zijn denken](https://www.erhg.net/ok-gebieden/)

[Otto Kroesen: De taalleer van Rosenstock Huessy](https://www.karlbarth.nl/kroesen-taalleer-rosenstock/)

[Wilmy Verhage over «Weitersager Mensch»](https://www.rosenstock-huessy.com/wv-weitersager-voorwoord/)

[Wilmy Verhage: Tijdbesef – Een inleiding tot het project «Weitersager Mensch»](https://www.rosenstock-huessy.com/wv-weitersager-tijdbesef/)

[Hans Weigand: De kracht van het tweedepersoonperspectief](https://sophieonline.nl/de-kracht-van-het-tweedepersoonperspectief-over-de-verwantschap-tussen-rosenstock-huessy-en-girard/)

<br/>
#### Introductie van zijn werken

[Wilmy Verhage: Dochters, Die Tochter – Das Buch Rut](https://www.rosenstock-huessy.com/weitersager-mensch/)

[Wim van der Schee over De onbetaalbare mens](https://www.rosenstock-huessy.com/ws-weitersager-onbetaalbaar/)

[Otto Kroesen naar aanleiding van Judaism despite Christianity](https://www.rosenstock-huessy.com/ok-weitersager-naar-despite/)

[Otto Kroesen: Toekomst – het Christelijk levensgeheim](https://www.rosenstock-huessy.com/ok-weitersager-toekomst/)

<br/>
### Anderstalige introducties
* [In het Engels](https://www.erhg.net/life-portraits/)
* [In het Duits](https://www.erhg.net/lebensbilder/)
