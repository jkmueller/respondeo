---
title: Statuten van de vereniging Respondeo
menu: Statuten
column: 2
row: 4
used-categories: statuten
---
### Preambule
Soms leven er mensen die hun tijd ver vooruit zijn en antwoorden vinden op problemen die in het verschiet liggen. Zo iemand was Rosenstock-Huessy. Hij werd geboren in 1888 in Duitsland, in een Joods gezin, en emigreerde in 1933 naar Amerika. Zijn omvangrijke werk zou sociologisch of theologisch genoemd kunnen worden, maar hij onttrok zich aan de grenzen die de wetenschap stelt. Hij wilde niet beschouwend te werk gaan, maar vanuit het leven zelf de gebeurtenissen doen spreken in menselijke taal.

In onze tijd waarin de oude religieuze stelsels voor velen hun betekenis hebben verloren, scheppen de ideeën van Rosenstock-Huessy een ongekende ruimte en verrassende inzichten, ook in de diepste waarheid van het christendom.

De naam van de vereniging, Respondeo, is afgeleid van een uitspraak van Rosenstock-Huessy: "Respondeo etsi mutabor": "Ik antwoord, al verander ik daardoor"

### Naan en Zetel <br/> Artikel 1
1. De vereniging draagt de naam: Vereniging Respondeo
2. De vereniging is gevestigd in Driebergen-Rijsenburg.

### Duur <br/> Artikel 2
De vereniging is aangegaan voor onbepaalde tijd.

### Doel <br/> Artikel 3
1. De vereniging stelt zich ten doel het bestuderen van het werk van Eugen Rosenstock-Huessy.
2. De vereniging tracht dit doel te bereiken door:
   1. het organiseren van bijeenkomsten voor het gezamenlijk bestuderen en het bespreken van het werk van Eugen Rosenstock-Huessy;
   2. het bevorderen van de beschikbaarheid van zijn werk door middel van vertaling, (her)uitgave en beheer;
   3. andere wettige middelendie voor het doel van de vereniging bevoordelijk kunnen zijn.


[naar boven](#top)
